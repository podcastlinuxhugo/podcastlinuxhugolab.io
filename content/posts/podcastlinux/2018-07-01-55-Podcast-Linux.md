---
title: "#55 Especial 2º Aniversario"
date: 2018-07-01
author: Juan Febles
categories: [podcastlinux]
img: PL/PL55.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL55
  image: https://podcastlinux.gitlab.io/images/PL/PL55.png
tags: [Especial, Aniversario, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL55.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL55.mp3" type="audio/mpeg">
</audio>


 Muy buenas, amante de GNU/Linux. Te doy la bienvenida a un nuevo episodio de Podcast Linux.  
 Mi nombre es Juan Febles y desde hace dos años nos encontramos en este espacio, un lugar abierto al que venir y disfrutar si te gusta el sistema operativo del ñu y el pingüino.

 **SORTEO: **Para participar en el sorteo de Slimbook, Vant y Neodigit debes seguir las indicaciones que comento en este audio.  
 <https://twitter.com/podcastlinux/status/1013275368877371392>

 **Audios por orden de aparición:  
 **Marta  
 Eduardo Collado (Neodigit)  
 Alejandro López (Slimbook)  
 Juan Carlos Navarrete (Vant)  
 Pedro Sáncher y Guiller (Bala Extra)  
 Margot Martín (El Recuento)  
 Sergio Solís (Bitácora de Ciberseguridad)  
 Raúl Fernández (Bitácora de Ciberseguridad)  
 Roberto RuiSánchez (Reto Friki)  
 Yoyo Fernández (Salmorejo Geek)  
 Paco Estrada (Compilando Podcast)  
 David 8 Bits (ochobitshacenunbyte)  
 Ricardo Espinosa (A golpes de clic)  
 Elav (System Inside)  
 NeoRanger (NeoSitLinux)  
 Helena e Iván (Wikimedia España)  
 David (Oyente)  
 Cosmos (Oyente)  
 Dj Mao Mix (Estación Radio Linuxera)  
 La Buardilla Geek  
 Baltolkian (KDE España Podcast)  
 Hefistion (El blog de Lázaro)  
 Mosquetero Web  
 Marcos Costales (Ubuntu y otras Hierbas)  
 Papá Friki  
 No Legal Tech  
 Iñaki Pinto (Maratón Linuxero)  
 José GDF (Rebasando el Horizonte)  
 Lorenzo Carbonell (Atareao)

  

 Las **imágenes utilizadas** se han liberado desde Pixabay con licencia Creative Commons 0:  
 <https://pixabay.com/photo-1904647/>  
 <https://pixabay.com/photo-1015697/>  
 <https://pixabay.com/photo-1015689/>  
 <https://pixabay.com/photo-1015703/>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other Side  
 YUTI – Language

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
