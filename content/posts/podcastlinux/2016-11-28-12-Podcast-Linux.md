---
title: "#12 Linux Connexion con Alejandro López"
date: 2016-11-28
author: Juan Febles
categories: [podcastlinux]
img: PL/PL12.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL12
  image: https://podcastlinux.gitlab.io/images/PL/PL12.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL12.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL12.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxeros!!! Bienvenidos a otra entrega de Podcast Linux. Un saludo muy fuerte de quien les habla, Juan Febles. Hoy vamos a tener de nuevo otra entrega de Linux Connexion.

 Al otro lado tenemos a Alejandro López. Alejandro es director comercial y cofundador de [Slimbook](https://slimbook.es/), empresa 100% española que nos ofrece ultraportátiles a medida ensamblados en España con una alta configuración y componentes de primera calidad 100% con GNU/Linux.

 Hablamos del nuevo dispositivo de la marca, [Slimbook One](https://slimbook.es/one-minipc-potente), y de su portátil buque insignia [Slimbook Katana](https://slimbook.es/ultrabook-katana).

 **![pegatina_03]({{< ABSresource url="/images/PL/pegatina_03-300x218.jpg" >}})**
 
 **ATENCIÓN:**

 **Los primeros 10 oyentes que comenten en la página del episodio qué es lo que les gusta más de Slimbook tendrán un detalle de la marca, concretamente unas pegatinas.**  
 ** Comparte tu comentario en <http://avpodcast.net/podcastlinux/slimbook/>**

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 LukHash – The Other Side  
 The Madpix Proyect – Liquid Blue (Instrumental)  
 The Madpix Proyect – Show me the way (Instrumental)  
 The Madpix Proyect – Wish you were here (Instrumental)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
