---
title: "#16 Antergos"
date: 2017-01-18
author: Juan Febles
categories: [podcastlinux]
img: PL/PL16.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL16
  image: https://podcastlinux.gitlab.io/images/PL/PL16.png
tags: [Distros, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL16.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL16.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero. Bienvenido a un nuevo episodio de Podcast Linux.  
 Mi nombre es Juan Febles y quincenalmente compartimos este espacio sonoro para hablar de nuestro sistema de escritorio favorito: GNU/Linux.

 En el Núcleo Kernel hablaremos de una distro que está sorprendiendo a expertos y recién llegados a Linux. Antergos.  
 <https://antergos.com/>

 ![]({{< ABSresource url="/images/PL/Captura-de-pantalla-de-2017-01-11-13-27-54-300x169.png" >}})  
 ![]({{< ABSresource url="/images/PL/Captura-de-pantalla-de-2017-01-11-13-28-44-300x169.png" >}})  
 ![]({{< ABSresource url="/images/PL/Captura-de-pantalla-de-2017-01-11-13-29-40-300x169.png" >}})  

 En el Gestor de Paquetes conoceremos todas las posibilidades que da Telegram en nuestras computadoras.  
 <https://telegram.org/>

 ![Telegram Desktop en GNU/Linux]({{< ABSresource url="/images/PL/Screenshot-Mon-Jan-16-2017-191118-GMT0000-WET-1440x787-300x164.png" >}})  

 <https://t.me/antergosesp>  
 <https://t.me/salmorejogeek>  
 <https://t.me/nuestrolinwinx>  
 <https://t.me/panicoenelkernel>

 El invitado a Comunidad Linux no podría ser otro que Alexandre Filgueira, creador y coordinador de Antergos.  
 <http://www.alexfilgueira.com/>

 Por último, en Área de Notificaciones, le daré un repaso a los mensajes que he recibido en este último mes.


 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Slinte – She Moved Through the Fair  
 Slinte – Lark in the Morning The Atholl Highlanders  
 Stereofloat – Chased  
 Stereofloat – Intergalactic Method  
 Stereofloat – Snowflakes on Mars

 La imagen de fondo de la carátula del episodio está diseñada por Blossomstar – Freepik.com

 Documentación para realizar este programa:

 <https://antergos.com/>  
 <https://es.wikipedia.org/wiki/Antergos>  
 <https://www.genbeta.com/a-fondo/asi-es-antergos-la-joya-de-la-corona-de-las-distros-linux-espanolas>  
 <http://www.linuxadictos.com/antergos-la-hija-mayor-de-archlinux.html>  
 <http://www.muylinux.com/2016/07/06/un-mes-antergos>  
 <http://www.muylinux.com/2016/06/17/antergos-dejara-de-distribuir-imagenes-iso-para-32-bits>  
 <http://www.muylinux.com/2016/12/31/ubuntu-kde-plasma-2016>  
 <https://lignux.com/aur-pacman-y-yauort-que-son-y-para-que-se-usan/>  
 <https://telegram.org/blog/desktop-1-0>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
