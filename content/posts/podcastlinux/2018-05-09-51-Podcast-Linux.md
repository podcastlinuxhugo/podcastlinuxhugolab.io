---
title: "#51 Chromebook"
date: 2018-05-09
author: Juan Febles
categories: [podcastlinux]
img: PL/PL51.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL51
  image: https://podcastlinux.gitlab.io/images/PL/PL51.png
tags: [Chromebook, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL51.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL51.mp3" type="audio/mpeg">
</audio> 

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy voy a estar contigo para hablar de un sistema operativo y dispositivos linux muy cercanos, o no, a GNU/Linux.  
 Aparca los problemas y vente conmigo, porque Chromebook, el episodio el número 51, arranca ya.

 En el **Núcleo Kernel** hablaré del dispositivo de moda en educación, Chromebook. Un concepto con raíces GNU/Linux que llama la atención por su versatilidad, eficiencia y posibilidades. Eso sí, totalmente privativo.

 En el** Gestor de Paquetes** te mostraré [Moodle](https://moodle.org/), una herramienta de gestión educativa libre para crear comunidades de aprendizaje online.

 En **Comunidad Linux** conoceremos a **MosqueteroWeb**, podcaster, profesor y usuario de GNU/Linux y ChromeOS.  
 Twitter: <https://twitter.com/mosqueteroweb>  
 Podcast: [https://www.ivoox.com/mosqueteroweb-tecnologia-linux-chromebooks\_sq\_f1248962\_1.html](https://www.ivoox.com/mosqueteroweb-tecnologia-linux-chromebooks_sq_f1248962_1.html)  
 Youtube: <https://www.youtube.com/channel/UCFPq7DDctQoMOHXSkDpkzqg>  
 Blog: <https://eschromebook.blogspot.com.es/>

 Por último, en **Área de Notificaciones**, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 La **imagen del chromebook utilizada** se ha liberado desde Flickr con licencia Creative Commons Reconocimiento por TechnologyGuide TestLab:  
 <https://flic.kr/p/eRPEzm>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other SideLukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 AlexZavesa – Calm Corporate Inspiration  
 AlexZavesa – Upbeat Inspiration Corporate  
 AlexZavesa – Inspiration Corporate  
 AlexZavesa – Happy Corporate  
 AlexZavesa – Tropical House Main Version

 **Bibliografía para realizar el episodio:**

 [https://store.google.com/us/product/google\_pixelbook\_specs](https://store.google.com/us/product/google_pixelbook_specs)  
 <https://es.wikipedia.org/wiki/Chromebook>  
 [https://es.wikipedia.org/wiki/Chromium\_OS](https://es.wikipedia.org/wiki/Chromium_OS)  
 [https://es.wikipedia.org/wiki/Chrome\_OS](https://es.wikipedia.org/wiki/Chrome_OS)  
 <https://computerhoy.com/noticias/hardware/que-son-chromebook-todo-que-necesitas-saber-11435>  
 <https://hipertextual.com/2015/07/instalar-chrome-os>  
 <https://www.neverware.com/#getcloudready>  
 <https://arnoldthebat.co.uk/wordpress/>  
 <https://www.chromium.org/>  
 <https://edu.google.es/intl/es/k-12-solutions/chromebooks/>  
 <https://andro4all.com/2015/02/razones-por-las-que-usar-chrome-os>  
 <http://maslinux.es/como-ver-la-version-actual-de-chrome-os-y-forzar-la-actualizacion-en-el-chromebook/>  
 <https://www.enriquedans.com/2011/05/hablando-sobre-chromebook-en-cinco-dias.html>  
 <https://support.google.com/chromebook/answer/3438631?hl=es>  
 <https://www.xatakandroid.com/tablets-android/acer-chromebook-tab-10-la-primera-tablet-con-chromeos-llega-con-puntero-wacom-y-enfocada-al-sector-educativo>  
 <https://www.google.com/chromebook/find-yours/>  
 <https://es.ctl.net/products/chromebook-ctl-j4>  
 <https://es.wikipedia.org/wiki/Moodle>  
 <https://moodle.org/>  
 <https://www.tropicalserver.com/ayuda/que-es-moodle/>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
