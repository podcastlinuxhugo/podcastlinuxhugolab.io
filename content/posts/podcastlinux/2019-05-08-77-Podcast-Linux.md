---
title: "#77 Especial FLISoL Tenerife"
date: 2019-05-08
author: Juan Febles
categories: [podcastlinux]
img: PL/PL77.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL77
  image: https://podcastlinux.gitlab.io/images/PL/PL77.png
tags: [Especial, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL77.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL77.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y tenemos un especial muy especial entre manos. Hoy te quiero acercar todo lo que pasó en el FLISoL Tenerife 2019 a través de sus ponentes.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 77, FLISol Tenerife.

 **Enlaces de interés:  
 **FLISol: <https://flisol.info>  
 FliSOL Tenerife: <https://flisol.info/FLISOL2019/Espana/Tenerife>  
 Telegram FLISOL Tenerife: <https://t.me/flisoltenerife19>  
 Gitlab FLISOL Tenerife:[ https://gitlab.com/osl-ull/flisol-tenerife/2019/](https://gitlab.com/osl-ull/flisol-tenerife/2019/)  
 Oficina de Software Libre de La Universidad de La Laguna:[ https://osl.ull.es/](https://osl.ull.es/)  
 Gulic: <http://gulic.org/>  
 Manz: <https://www.emezeta.com/manz/>

  

 **La imagen utilizada en la portada es propiedad de [Manz](https://www.emezeta.com/manz/) y ha sido liberada con licencia [Creative Commons Atribución – Compartir Igual](https://creativecommons.org/licenses/by-sa/4.0/deed.es): **  
 El logo de FLISOL es propiedad del propio proyecto internacional.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons **y respeta la** Cultura Libre**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>

 Recuerda que puedes **contactar** conmigo de la siguiente manera:

 
 * Twitter: [@podcastlinux](https://twitter.com/podcastlinux)
 * Mastodon: [@podcastlinux](https://mastodon.social/@podcastlinux)
 * Correo: [podcastlinux@avpodcast.net](mailto:podcastlinux@avpodcast.net)
 * Web: [avpodcast.net/podcastlinux](https://avpodcast.net/podcastlinux/)
 * Blog: <https://podcastlinux.com>
 
 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
