---
title: "#64 Pásate a GNU/Linux"
date: 2018-11-07
author: Juan Febles
categories: [podcastlinux]
img: PL/PL64.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL64
  image: https://podcastlinux.gitlab.io/images/PL/PL64.png
tags: [Instalación, Distros, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL64.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL64.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy vamos a hacer algo nuevo. Lo que no cambia es, que como cada quince días, vamos a disfrutar en torno a media hora de un episodio.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 64 con una premisa bien clara, Pásate a GNU/Linux.  
 ¿Te animas?

 En el Núcleo Kernel explicaré qué pasos son necesarios para instalar GNU/Linux sin miedo y disfrutar de este fantástico sistema operativo.  
 En el gestor de paquetes hablaremos de [Firefox](https://www.mozilla.org/es-ES/firefox/), el navegador libre multiplataforma.  
 En Comunidad Linux te presentaré a [Riky Linux](https://twitter.com/Rikylinux), amante del software libre y dedicado a la difusión de nuestro sistema operativo del ñu y el pingüino dando clases, participando en todos los eventos posibles y fomentando su uso en el proyecto que desde hace 2 años impulsa: [El Club de Software Libre](https://clubdesoftwarelibre.wordpress.com/).  
 Finalizaremos este episodio escuchando comentarios de oyentes en la última sección, Área de Notificaciones.**  
 **

 **Pasos a tener en cuenta:**  
 Ordenador a instalar GNU/Linux  
 Copia de seguridad  
 Elegir distro a nuestras necesidades y posibilidades  
 Descargar ISO live  
 Quemar ISO live en pendrive  
 Arrancar la ISO live en la computadora  
 Instalar distro GNU/Linux: elegir arranque dual o sólo instalar GNU/Linux.  
 Algunas recomendaciones post instalación.

 Las **imágenes utilizadas en la portada, **se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/photo-1816348/>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 Arhur Yul – Cinema Intro: <https://www.jamendo.com/track/1503848/>  
 GarnaVutka – Corporate: <https://www.jamendo.com/track/1354699>  
 Zoran Sebic – Innovative Logo: <https://www.jamendo.com/track/1487845>  
 GarnaVutka – Crystal Wellcome Logo: <https://www.jamendo.com/track/1508030>  
 GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): <https://www.jamendo.com/track/1464611>  
 GarnaVutka – Inspiring Motivational Full version:<http://jamen.do/t/1389160>  
 GarnaVutka – Inspirational Corporate Full version: <http://jamen.do/t/1471639>  
 GarnaVutka – Inspiring Full version: <http://jamen.do/t/1352946>  
 GarnaVutka – Motivational Inspiring Corporate: <http://jamen.do/t/1493905>  
 GarnaVutka – Tech Full version: <http://jamen.do/t/1558215>

 **Bibliografía utilizada para realizar este episodio:**

 <https://clonezilla.org>  
 <https://rsync.samba.org/>  
 <https://etcher.io/>  
 <https://forums.linuxmint.com/viewtopic.php?t=231688>  
 <https://www.atareao.es/tutorial/linux-mint-cinnamon/>  
 <https://es.wikipedia.org/wiki/Mozilla\_Firefox>  
 <https://www.gnu.org/software/gnuzilla/>  
 <https://www.mozilla.org/es-ES/firefox/>  
 <https://clubdesoftwarelibre.wordpress.com>  
 <https://t.me/CSLibre>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
