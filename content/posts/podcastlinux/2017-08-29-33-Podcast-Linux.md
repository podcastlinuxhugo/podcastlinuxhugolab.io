---
title: "#33 1º Directo Podcast Linux"
date: 2017-08-29
author: Juan Febles
categories: [podcastlinux]
img: PL/PL33.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL33
  image: https://podcastlinux.gitlab.io/images/PL/PL33.png
tags: [Directo, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL33.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL33.mp3" type="audio/mpeg">
</audio>


 **NOTA: Este directo es un experimento más que cierra mis pruebas de verano. Es un poco personal sin una temática definida donde cuento mis experiencias en los meses de julio y agosto. **

 Muy buenas linuxeros y bienvenidos al primer directo de Podcast Linux. Después de solicitar a los oyentes qué plataforma desean para la emisión, Youtube fue la elegida y mediante OBS Studio, aplicación que ya hemos hablado, he intentado coger más experiencia en el streaming.

 Comenté lo sucedido en este verano, con sus 5 episodios publicados y próximos temas a tratar.

 También compartí la experiencia de ser parte del evento [Maratón Linuxero](https://maratonlinuxero.github.io/), que tendrá lugar el 3 de septiembre.  
 [maratonlinuxero.github.io.](https://maratonlinuxero.github.io/)

 Compartí comentarios de algunos oyentes y respondí algunas preguntas del chat del directo.

 Me ha encantado la experiencia como otra herramienta más para acercarme a la audiencia.

 La emisión de Youtube:

  Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 TwoTriangles – House

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
