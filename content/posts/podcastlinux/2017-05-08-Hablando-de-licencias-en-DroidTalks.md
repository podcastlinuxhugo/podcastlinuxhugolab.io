---
title: Hablando de licencias en DroidTalks
author: Juan Febles
date: 2017-05-05
img: 2017/5Droidtalks.jpg
---
Cada primer o segundo domingo de mes nos solemos reunir los compañeros de [DroidTalks](https://avpodcast.net/droidtalks/) para grabar el episodio mensual. Tengo la suerte de compartir con Gabriel, Roberto y Pedro este podcast tecnológico, en el que aporto mi granito de arena.

<audio controls>
  <source src="http://avpodcast.net/wp-content/uploads/2017/05/DT5-Licencias.mp3" type="audio/mpeg">
</audio>

Este mes tocó un tema muy interesante del que tengo que conocer más: las licencias de software. Otra asignatura pendiente. Creo que ha quedado muy bien la charla y aunque son 2 horas, merece la pena echarle una escucha.
