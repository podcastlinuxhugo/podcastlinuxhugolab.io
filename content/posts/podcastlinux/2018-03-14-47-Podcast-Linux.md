---
title: "#47 Especial YEPO 737A"
date: 2018-03-14
author: Juan Febles
categories: [podcastlinux]
img: PL/PL47.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL47
  image: https://podcastlinux.gitlab.io/images/PL/PL47.png
tags: [Hardware, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL47.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL47.mp3" type="audio/mpeg">
</audio>

 Muy buenas amante del Software Libre y GNU/Linux, mi nombre es Juan Febles y hoy te voy a contar qué tal me ha ido con el **YEPO 737A**, un netbook ultrabarato de 13 pulgadas.  
 Quédate conmigo unos cerca de 20 minutos para conocer mi experiencia con los nuevos portátiles chinos que están de moda en tiendas de comercio electrónico del continente asiático. ¿Estás preparado?

 **Enlaces:**

 Tienda Gearbest: [https://www.gearbest.com/laptops/pp\_1050951.html](https://www.gearbest.com/laptops/pp_1050951.html)

 **Foros:**

 [https://techtablets.com/forum/topic/how-to-install-manjaro-linux-on-jumper-ezbook-3-pro-v4/  
 ](https://techtablets.com/forum/topic/how-to-install-manjaro-linux-on-jumper-ezbook-3-pro-v4/)<https://bbs.archlinux.org/viewtopic.php?id=231540>

 **IsoRespin:**

 <https://linuxiumcomau.blogspot.com.es/2017/06/customizing-ubuntu-isos-documentation.html>

 **Configuración Linux Mint Xfce:**

 Iconos Papirus: <https://github.com/PapirusDevelopmentTeam/papirus-icon-theme>  
 Tema Adapta Nokto: <https://github.com/adapta-project/adapta-gtk-theme> <https://www.omgubuntu.co.uk/2016/10/install-adapta-gtk-theme-on-ubuntu>  
 Iconos blancos en la bandeja del sistema: <https://github.com/bil-elmoussaoui/Hardcode-Tray>  
 Tema clima Wsky Light: <https://github.com/kevlar1818/xfce4-weather-mono-icons>

 **Vídeo:**

  Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other SideLukHash – The Other Side  
 Alexiaction – Motivational Corporate

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
