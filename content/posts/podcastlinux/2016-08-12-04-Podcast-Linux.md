---
title: "#04 Amor de Distro Madre"
date: 2016-08-12
author: Juan Febles
categories: [podcastlinux]
img: PL/PL04.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL04
  image: https://podcastlinux.gitlab.io/images/PL/PL04.png
tags: [Distros, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL04.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL04.mp3" type="audio/mpeg">
</audio>

Muy buenas Linuxeros. Bienvenido a un episodio más de Podcast Linux.  
Mi nombre es Juan Febles y este es mi proyecto más personal con el que quiero acercar el mundo GNU/Linux a los usuarios domésticos de sistemas operativos de escritorio.

 Gestor de Arranque: Resumen del episodio

 En el Núcleo Kernel te hablaré de las distros madres, fundamental para entender el universo GNU/Linux.

 En el Gestor de Paquetes daremos paso a OBS Studio, un programa multiplataforma de software libre para la grabación de vídeo y streaming en vivo.  
 <https://obsproject.com/>  
 

 El invitado de Comunidad Linux es Yoyo Fernández, blogero, podcaster y youtuber linuxero.  
 @yoyo308  
 <https://salmorejogeek.com>  


 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 Artistas:  
 LukHash  
 Cleric  
 Matti Paalanen  
 The Polish Ambassador  
 Arstex

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1285850_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
