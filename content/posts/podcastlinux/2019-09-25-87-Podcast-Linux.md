---
title: "#87 Linux Connexion con Adrián Perales"
date: 2019-09-25
author: Juan Febles
categories: [podcastlinux]
img: PL/PL87.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL87
  image: https://podcastlinux.gitlab.io/images/PL/PL87.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL87.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL87.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega, la número 87, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy tenemos con nosotros a [Adrián Perales](https://adrianperales.com/), bloguero, podcaster y profesor de Lengua Castellana y Literatura en Secundaria y Bachillerato. Es parte del blog comunícate libremente cuyo objetivo es informar y difundir tecnologías web libres, abiertas y descentralizadas.

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y agradecer a [Neodigit](https://www.neodigit.net/), nuestro proveedor de alojamiento y servicios de confianza de habla hispana para todo Avpodcast.

**Enlaces de interés:**

Blog Adrián Perales: [https://adrianperales.com](https://adrianperales.com)  
Comunícate Libremente: [https://comunicatelibremente.wordpress.com](https://comunicatelibremente.wordpress.com)

Mastodon: [https://joinmastodon.org/](https://joinmastodon.org/)  
Nodo principal de Mastodon: [https://mastodon.social](https://mastodon.social)  
Aplicaciones de Mastodon para diferentes plataformas: [https://joinmastodon.org/apps](https://joinmastodon.org/apps)  
Pinafore.social, interfaz web alternativa para Mastodon: [https://pinafore.social](https://pinafore.social)

Otras redes basadas en ActivityPub  
PixelFed: [https://pixelfed.org/](https://pixelfed.org/)  
FunkWhale: [https://funkwhale.audio/](https://funkwhale.audio/)  
Peertube: [https://joinpeertube.org/es/](https://joinpeertube.org/es/)

Clientes para XMPP que recomienda Adrián Perales:  
Android: Conversations: [https://conversations.im/](https://conversations.im/) Gratuito en F-droid  
iOS: ChatSecure: [https://chatsecure.org/](https://chatsecure.org/)

Charla TED de Marta Peirano (en Peertube): [https://peertube.servebeer.com/videos/watch/367f616a-efb7-448a-84b7-8a5d0ab560f2](https://peertube.servebeer.com/videos/watch/367f616a-efb7-448a-84b7-8a5d0ab560f2)

Terms of service; didn’t read: [https://opencollective.com/tosdr](https://opencollective.com/tosdr)

Las **imagen utilizada en la portada** ha sido cedida y es propiedad del propio Adrián Perales.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Free Music – Gentle Corporate: [https://www.free-stock-music.com/fm-freemusic-gentle-corporate.html](https://www.free-stock-music.com/fm-freemusic-gentle-corporate.html)  
  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1287870_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
