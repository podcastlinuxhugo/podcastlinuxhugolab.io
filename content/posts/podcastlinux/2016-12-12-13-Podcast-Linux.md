---
title: "#13 Ciberseguridad Básica en GNU/Linux"
date: 2016-12-12
author: Juan Febles
categories: [podcastlinux]
img: PL/PL13.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL13
  image: https://podcastlinux.gitlab.io/images/PL/PL13.png
tags: [Ciberseguridad, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL13.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL13.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxeros. Bienvenidos a una nueva entrega de Podcast Linux.  
 Mi nombre es Juan Febles y cada 2 semanas enciendo el micrófono para hablar de lo que más nos gusta: GNU/Linux.

 En el Núcleo Kernel abordaré unos sencillos pasos a seguir para tener una seguridad básica en nuestros ordenadores domésticos.  
 Proteger físicamente el ordenador  
 Proteger la BIOS con contraseña  
 Proteger el GRUB con contraseña  
 Comprobar las ISO de las distros y otros archivos con su firma MD5  
 Proteger la sesiones y las gestiones administrativas con una contraseña robusta  
 Proteger el Disco Duro con cifrado  
 Proteger la sesión con el bloqueador de pantalla  
 Proteger el sistema con actualizaciones  
 Contrastar las aplicaciones y repositorios que instalamos  
 Desinstalar aplicaciones que no usemos  
 Gestionar tus contraseñas  
 Instalar un cortafuegos para dar mayor seguridad al sistema  
 Realizar copias de seguridad  
 Hábitos que nos aportan mayor seguridad en Internet

 En el Gestor de Paquetes quiero que conozcas KeePassX, un gestor de contraseñas libre multiplataforma.  
 <http://keepass.info/>

 ![]({{< ABSresource url="/images/PL/Screenshot-Sat-Dec-10-2016-093633-GMT0000-WET-1920x1080-300x169.png" >}})

  

 ![]({{< ABSresource url="/images/PL/Screenshot-Sat-Dec-10-2016-092856-GMT0000-WET-578x342-300x178.png" >}})  
 Los invitados a Comunidad Linux son Raúl Fernández y Sergio Rodríguez Solís, el binomio encargado del podcast Bitácora de Ciberseguridad.  
 <http://avpodcast.net/bitacora-ciberseguridad/>  
 <https://twitter.com/BitaCiber>


 Por último, en Área de Notificaciones, le daré un repaso a los mensajes que he recibido en este último mes.

 El podcast promocionado es [Psicomanagement](http://avpodcast.net/psicomanagement-podcast/).


 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Soundshim – Sunny Days  
 Soundshim – Ambient Background  
 Soundshim – Ambient Pop  
 Soundshim – Beatiful Corporate  
 Soundshim – Corporate Background

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
