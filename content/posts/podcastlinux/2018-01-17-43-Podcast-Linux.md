---
title: "#43 Especial UltraMoove de Vant"
date: 2018-01-17
author: Juan Febles
categories: [podcastlinux]
img: PL/PL43.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL43
  image: https://podcastlinux.gitlab.io/images/PL/PL43.png
tags: [Hardware, Vant, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL43.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL43.mp3" type="audio/mpeg">
</audio>

 Muy buenas linuxero, bienvenido a otro episodio más de Podcast Linux. Mi nombre es Juan Febles y hoy vamos a tener un programa especial, exactamente el número 43, para conocer un portátil diseñado para GNU/Linux, el UltraMoove de la marca Vant.

 **FE DE ERRATAS: **

 
 * El peso del dispositivo es 1,439 kg batería incluida, no cargador incluido. Fallo mío.
 * La conexión no es Micro SD, sino micro SIM. Ésta carece de funcionamiento porque no tiene un modem interno conectado a ella. Sí funciona la ranura SD/MMC. Otra vez, fallo mío.
 * El USB tipo C no tiene salida de vídeo. De nuevo, fallo mío por no probarlo y malentender que sí tenía esta posibilidad.
 * Si bien comenté conseguir 6 horas y media de batería, la propia empresa me comenta que en sus pruebas y las de otros usuarios llegan valores en torno a las 4 horas. Hay que tener en cuenta todas estas cifras.
 
 Y antes de empezar comentarte un **sorteo para los oyentes** que retuiteen el tuit de este episodio y sigan a Vant y Podcast Linux. 5 serán los agraciados de un lote de la marca que consta de: teclado+ratón linuxero, Pendrive con distribución Linux y pegatina TUX. En una semana daré a conocer los agraciados. En las notas del programa te dejo el tuit que debes utilizar.  
 <https://twitter.com/podcastlinux/status/953506421555056641>

 **Vant:**

 Web: <http://www.vantpc.es/>  
 UltraMoove: <http://www.vantpc.es/producto/ultramoove>  
 Twitter: <https://twitter.com/vantpc>

 Te dejo el screencast que realicé a las semanas de llegarme:

  Aquí tienes algunas imágenes del dispositivo:

 ![]({{< ABSresource url="/images/PL/photo_2017-12-31_18-32-38-300x169.jpg" >}})   
 ![]({{< ABSresource url="/images/PL/photo_2017-12-31_18-32-46-300x169.jpg" >}})  
 ![]({{< ABSresource url="/images/PL/photo_2017-12-31_18-32-30-300x169.jpg" >}})   
 ![]({{< ABSresource url="/images/PL/photo_2017-12-31_18-32-16-300x169.jpg" >}})  
 ![]({{< ABSresource url="/images/PL/photo_2017-12-31_18-32-10-300x169.jpg" >}})  
 ![]({{< ABSresource url="/images/PL/photo_2017-12-31_18-32-04-300x169.jpg" >}})  
 ![]({{< ABSresource url="/images/PL/photo_2017-12-31_18-31-58-300x169.jpg" >}})  
 ![]({{< ABSresource url="/images/PL/photo_2017-12-31_18-31-48-300x169.jpg" >}})  

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Seastock – Lift Up

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
