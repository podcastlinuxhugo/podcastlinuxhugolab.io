---
title: "#61 Linux Connexion con RikyLinux"
date: 2018-09-26
author: Juan Febles
categories: [podcastlinux]
img: PL/PL61.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL61
  image: https://podcastlinux.gitlab.io/images/PL/PL61.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL61.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL61.mp3" type="audio/mpeg">
</audio>

 Muy buenas amante de GNU/Linux y el Software Libre.  
 Bienvenido a un nuevo episodio, el número 61, de Podcast Linux. Un saludo muy fuerte y cariñoso de quien te habla, Juan Febles.  
 Hoy vamos a disfrutar de la compañía de [Riky Linux](https://twitter.com/Rikylinux), lignuxero dedicado plenamente a la difusión de nuestro sistema operativo del ñu y el pingüino dando clases, participando en todos los eventos posibles y con el proyecto que desde hace 2 años impulsa: [El Club de Software Libre.](https://clubdesoftwarelibre.wordpress.com)

 **Métodos de contacto:**

 Twitter: <https://twitter.com/Rikylinux>  
 Diaspora: csl@salas.diaspora.com.ar  
 Club del Software Libre: <https://clubdesoftwarelibre.wordpress.com>  
 Telegram: <https://t.me/CSLibre>

 Las **imágenes utilizadas en la portada **son cedidas por el propio entrevistado.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 MegaEnx – Magic Rainbow: <https://soundcloud.com/megaenx/bubbles>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
