---
title: "#44 Linux Connexion con Vant"
date: 2018-01-31
author: Juan Febles
categories: [podcastlinux]
img: PL/PL44.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL44
  image: https://podcastlinux.gitlab.io/images/PL/PL44.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL44.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL44.mp3" type="audio/mpeg">
</audio>

 Muy buenas amante del ñu y el pingüino, mi nombre es Juan Febles y estás escuchando Podcast Linux, el programa para usuarios domésticos de GNU/Linux.

 Hoy tenemos un Linux Connexion, las entrevistas que se intercalan con los episodios formales para ahondar más sobre los temas tratados. En esa ocasión, el número 44, vamos a conversar con Juan Carlos Navarrete, Director Técnico de [Vant](http://www.vantpc.es/), empresa que ofrece ordenadores personales con Distros Linux, totalmente compatibles con nuestro sistema operativo preferido.

 Lo primero, vamos a leer la fe de erratas del episodio anterior sobre el dispositivo UltraMoove, errores que cometí yo personalmente y que quiero enmendar para que no haya problema alguno.

 **FE DE ERRATAS:**

 
 * El peso del dispositivo es 1,439 kg batería incluida, no cargador incluido. Fallo mío.
 * La conexión no es Micro SD, sino micro SIM. Ésta carece de funcionamiento porque no tiene un modem interno conectado a ella. Sí funciona la ranura SD/MMC. Otra vez, fallo mío.
 * El USB tipo C no tiene salida de vídeo. De nuevo, fallo mío por no probarlo y malentender que sí tenía esta posibilidad.
 * Si bien comenté conseguir 6 horas y media de batería, la propia empresa me comenta que en sus pruebas y las de otros usuarios llegan valores en torno a las 4 horas. Hay que tener en cuenta todas estas cifras.
 
  

 Hemos realizado el **sorteo para los oyentes** que retuitearon el tuit del episodio anterior, sigan a Vant y Podcast Linux. 5 son los agraciados de un lote de la marca que consta de: teclado+ratón linuxero, pendrive con distribución Linux y pegatina TUX.

 Los agraciados son: <https://twren.ch/check#953506421555056641>  
 @alberto\_losada  
 @tuxitagnu  
 @imacsamu  
 @juldelagbeta  
 @platter5\_

 Felicidades a todos ellos.

 **Vant:**

 Web: <http://www.vantpc.es/>  
 Correo: [info@vantpc.es](mailto:info@vantpc.es)  
 Twitter: <https://twitter.com/vantpc>  
 Teléfono: 960600995

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side

 Rugi Estalagmita MidiMan – Padix´s Theme Alimoche for Ever

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
