---
title: "#60 Software Libre"
date: 2018-09-12
author: Juan Febles
categories: [podcastlinux]
img: PL/PL60.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL60
  image: https://podcastlinux.gitlab.io/images/PL/PL60.png
tags: [Software Libre, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL60.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL60.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy iniciamos la tercera temporada. Como cada quince días, vamos a disfrutar en torno a media hora de un nuevo episodio.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 60, Software Libre, un nuevo capítulo que empieza ya.

 En el Núcleo Kernel explicaré qué es el [Software Libre](https://www.gnu.org/philosophy/free-sw.es.html) y las repercusiones que genera su uso.  
 En el gestor de paquetes hablaremos de [ShotCut](https://www.shotcut.org/), un editor de vídeo Software Libre multiplataforma con licencia GNU GPL v3.0.  
 En Comunidad Linux te presentaré a [Rubén Rodríguez](http://quidam.cc/acerca-de), miembro de la [Free Software Foundation](https://www.fsf.org/) y fundador de [Trisquel GNU/Linux](https://trisquel.info/es).  
 Finalizaremos este episodio escuchando comentarios de oyentes en la última sección, Área de Notificaciones.

 Las **imágenes utilizadas en la portada, **el logo de la Free Software Foundation y el ñu levitando son propiedad de [fsf.org](https://www.fsf.org/). El hombre en 3D se ha liberado desde Pixabay con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/photo-1816349>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 Arhur Yul – Cinema Intro: <https://www.jamendo.com/track/1503848/>  
 GarnaVutka – Corporate: <https://www.jamendo.com/track/1354699>  
 Zoran Sebic – Innovative Logo: <https://www.jamendo.com/track/1487845>  
 GarnaVutka – Crystal Wellcome Logo: <https://www.jamendo.com/track/1508030>  
 GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): <https://www.jamendo.com/track/1464611>  
 MegaEnx – Magic Rainbow: <https://soundcloud.com/megaenx/magic-rainbow>  
 Dj Nen – Acuarium: <https://jamen.do/t/1565212>  
 Dj Nen – Above my dreams: <https://jamen.do/t/1441779>  
 Mc Chino Calm and Harmony: <https://soundcloud.com/mike-chino/mike-chino-calm-and-harmony>  
 MegaEnx – Space Debris (rsf Remix Mkii): <https://soundcloud.com/rsfmu/space-debris-remix>

 **Bibliografía utilizada para realizar este episodio:**

 <https://www.ochobitshacenunbyte.com/2011/06/30/linux-gnu-de-que-me-hablas/>  
 <https://hipertextual.com/archivo/2014/05/diferencias-software-libre-y-open-source/>  
 <https://iyanmv.com/2013/08/04/diferencias-entre-software-libre-y-codigo-abierto/>  
 <https://lignux.com/diferencias-entre-software-libre-open-source-y-freeware/>  
 <https://es.opensuse.org/Software\_libre\_y\_de\_c%C3%B3digo\_abierto>  
 <https://www.gnu.org/philosophy/free-software-for-freedom.es.html>  
 <https://www.gnu.org/philosophy/free-sw.es.html>  
 <https://tecnologia-informatica.com/tipos-licencias-software-libre-comercial/>  
 <https://www.maestrosdelweb.com/licencias-libres-de-software-i/>  
 <https://www.maestrosdelweb.com/licencias-libres-de-software-ii/>  
 <http://www.cobdc.net/programarilliure/codigo-abierto-es-lo-mismo-que-software-libre/>  
 <https://www.shotcut.org/>  
 <http://quidam.cc/acerca-de>  
 <https://youtu.be/5rX8cMUZjmA>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
