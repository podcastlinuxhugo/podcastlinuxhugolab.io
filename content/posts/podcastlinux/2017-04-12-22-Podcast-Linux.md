---
title: "#22 Linux Connexion con la OSL La Laguna"
date: 2017-04-12
author: Juan Febles
categories: [podcastlinux]
img: PL/PL22.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL22
  image: https://podcastlinux.gitlab.io/images/PL/PL22.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL22.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL22.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero. Bienvenido a un episodio más de Podcast Linux.

 Mi nombre es Juan Febles y hoy toca una nueva entrevista Linux Connexion, los programas especiales de este podcast para acercarte a las personas o proyectos que han salido en la sección Comunidad Linux.

 Hoy vamos a cerrar la entrega de 4 capítulos sobre GNU/Linux y educación y esta ocasión es muy especial para mí. Por una parte es la primera entrevista que hago in situ y presencial, nos hallamos en La Oficina de Software Libre de la Universidad de La Laguna. Aquí me encuentro con una mesa de mezclas y 3 micros. Por otra, como muchos saben, este proyecto es el responsable que hace casi 10 años quien les habla, se decantara por el sistema operativo del pingüino. Este podcast ha visto la luz en parte gracias a ellos.

 Tenemos con nosotros a Patricio Báez y Eduardo Nacimiento para debatir y reflexionar sobre el software libre en la Universidad.

 **Contacto OSL ULL:**

 Twitter: [@oslull](https://twitter.com/OSLULL)  
 Correo: [info@osl.ull.es](mailto:info@osl.ull.es)  
 Web: <https://osl.ull.es/>

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Ch Joy – Your Corporate Inspiration

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
