---
title: "#30 Especial Maratón Linuxero"
date: 2017-07-17
author: Juan Febles
categories: [podcastlinux]
img: PL/PL30.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL30
  image: https://podcastlinux.gitlab.io/images/PL/PL30.png
tags: [Especial, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL30.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL30.mp3" type="audio/mpeg">
</audio>

 Hoy comparto contigo un episodio muy especial. Especial porque es la primera vez que publico aquí un episodio emitido en directo, por su duración, más de 2 horas, porque es una charla compartida con muchos compañeros linuxeros y porque es un proyecto ambicioso más grande que este programa.

 El audio lo comparto íntegro del siguiente post: <https://maratonlinuxero.github.io/01-Qu%C3%A9-es-y-c%C3%B3mo-anda-el-Marat%C3%B3n-Linuxero/>

 Llevamos varias emisiones para probar la viabilidad y con esta última pretendemos dar respuesta a varios frentes abiertos del proyecto.

 Lo primero, seguir realizando pruebas de emisiones en directo. Hasta ahora lo hemos intentado con Mumble y no hemos conseguido buenos resultados.A través de Jit.si mejora y facilita mucho, pero sus servidores públicos no permiten más de 1 hora de emisión hacia Youtube, que por su audiencia y chat en vivo, la vemos como la plataforma de emisión adecuada.

 Lo siguiente ha sido utilizar OBS Studio como forma de conectar el audio de Jit.si hacia Youtube. Con esta emisión de 2 horas y media y a falta de pulir algunos contratiempos más durante el directo, vemos que esta fórmula va ganando frente a otras.

 Comentamos nuestras dudas y sugerencias y seguimos en marcha para que el 3 de septiembre a las 15:00 horas horario español, todo esté perfecto.

 No dudes en ponerte en contacto si deseas colaborar en este evento de cualquier manera.  
 Para ello, lo más directo es ponerte en contacto a través del grupo de Telegram para este evento: <https://t.me/maratonlinuxero>

 Aquí tienes la emisión en Youtube:

 Los **métodos de contacto** de Maratón Linuxero son:

 
 * Blog: <https://maratonlinuxero.github.io/>
 * Twitter: <https://twitter.com/maratonlinuxero>
 * Correo: [maratonlinuxero@gmail.com](mailto:maratonlinuxero@gmail.com)
 * Telegram: <https://t.me/maratonlinuxero>
 * Youtube: <https://www.youtube.com/channel/UCqFf7ygeietihuh5GSEj3oQ>
 * Feed Podcast Maratón Linuxero: <http://maratonlinuxero.github.io/feed.xml>
 * Audios en Archive.org: <https://archive.org/details/@maratonlinuxero>
 
  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
