---
title: "#18 Linux Connexion con Bitácora de Ciberseguridad"
date: 2017-02-15
author: Juan Febles
categories: [podcastlinux]
img: PL/PL18.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL18
  image: https://podcastlinux.gitlab.io/images/PL/PL18.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL18.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL18.mp3" type="audio/mpeg">
</audio>

 Tengo la suerte de poder charlar con unos grandes compañeros de la red AVPodcast.  
 Raúl y Sergio nos develan algunos mitos y comentamos en general cómo es nuestro sistema operativo frente a las ciberamenazas.

 Métodos de contacto de Bitácora de Ciberseguridad:

 Web: <http://avpodcast.net/bitacora-ciberseguridad/>  
 Correo: [ciberseguridad@avpodcast.net](mailto:ciberseguridad@avpodcast.net)  
 Twitter: <https://twitter.com/BitaCiber>  
 Sergio: [https://twitter.com/s\_rsolis](https://twitter.com/s_rsolis)  
 Raúl: <https://twitter.com/raula49>

  Para ver los servicios que se están ejecutando en nuestro ordenador podemos  
 ejecutar el comando ps aux. Nos mostrará el usuario propietario del servicio, los  
 recursos que usa su PID, que es el identificador de proceso y servicio en cuestión.  
 Para ver los servicios que dejan puertos abiertos podemos ejecutar netstat -lp,  
 que nos mostrará el protocolo, el puerto y el servicio que abre dicho puerto.  
 Con estos dos comandos encontraremos qué hay abierto y por donde se accede.  
 Cosas como:  
 – smbd y nmbd: que es para compartir archivos en el formato samba que  
 es el de carpetas compartidas de windows  
 – Telnet: un protocolo de comunicaciones para comandos remotos que  
 carece de cifrado y es muy peligroso  
 – rlogin, para registrarnos en otras máquinas de nuestra red y usarlas en  
 remoto  
 – rexec, para ejecución comandos en la shell de otro ordenador  
 – FTP, para que se acceda a una carpeta de archivos compartidos en red  
 mediante este protocolo. Aunque existe una versión segura, SFTP, el  
 FTP clásico tampoco dispone de cifrado de comunicación.  
 – automount, sirve para montar automaticamente sistemas de archivos,  
 locales o en red. Si no lo usamos, fuera  
 – named, Hay casos en los que nos encontramos un servidor DNS en  
 nuestro propio equipo linux que quizás no usemos porque usamos el  
 configurado en el router de casa o la oficina  
 – lpd, es un servicio de servidor de impresora Al hablar de distribuciones de seguridad, según con quién hables, yo haría tres categorías:  
 – Pentesting: para hacer pruebas de seguridad en modo ofensivo  
 – Análisis forense: para analizar información, para realizar peritajes infor-  
 máticos, es decir, para averiguar qué se hizo o no con un equipo o si  
 ciertos datos han podido ser manipulados  
 4- Privacidad: orientadas al anonimato y la privacidad, tanto en el uso local  
 como, principalmente, al navegar en red.  
 Pentesting  
 Pues la más clásica para pentesting es Kali, basada en Debian y enfocada a seguri-  
 dad ofensiva. Es la heredera de BackTrack y la única de este estilo que he usado.  
 También para pentesting está BackBox, basada en Ubuntu, como segunda en  
 popularidad detrás de Kali.  
 Otras distribuciones para pentesting son:  
 – Fedora Security Spin  
 – BlackArch  
 – Pentoo  
 – Cyborg Linux  
 – Weakerth4n  
 – Samurai Web Testing Framework  
 – y otro clásico: KNOPPIX, que ahora que recuerdo probablemente sea la  
 primera live que usé en mi vida  
 Forense  
 En cuanto a análisis forenses una de las más populares es Caine, además de los  
 servicios, paquetes y aplicaciones que incluye para análisis de smartphones, dis-  
 cos, dumpeo de memoria, etcétera, está diseñada para no escribir en ningún dis-  
 positivo que conectemos, sólo para leer. Así no se corrompen las evidencias lega-  
 les.  
 5Otras distros para análisis forense muy conocidas son DEFT y Santoku.  
 Privacidad  
 Para trabajar de forma extremadamente segura y lo más anónimamente posible te-  
 nemos:  
 – TAILS: The Amnesiac Incognito Live System. Es una distro live basada en  
 Debian especialmente diseñada para no dejar huella cuando la usamos  
 para navegar online. Modifica la dirección MAC de la tarjeta de red, si-  
 mula ser un sistema operativo Windows, navega a través de TOR. Ade-  
 más, cuando apagamos el ordenador no queda ningún registro, log, ca-  
 ché o similar a no ser que lo configuremos a propósito para que lo guar-  
 de. Además dispone de herramientas de cifrado de datos y de comuni-  
 caciones.  
 – Whonix es directamente una imagen de máquina virtual para Birtual-  
 box. Por un lado es una pasarela TOR y por otro un terminal que funcio-  
 na únicamente a través de TOR  
 – Una que ha ganado fama porque la comentó el tristemente famoso  
 Eduard Snowden, es Qubes OS. Básicamente crea máquinas virtuales  
 para hacer cada cosa que hay que hacer aislando unas de las otras.  
 – UPR es Ubuntu Privacy Remix que como imaginaréis, está basada en  
 Ubuntu, pero por lo que leo, no es lo más amigable en cuanto usabili-  
 dad que existe.  
 – Por último mencionaremos JonDo, que es otra Live dedicada a la anoni-  
 mización  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
