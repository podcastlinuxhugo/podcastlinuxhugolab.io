---
title: "#02 Un pingüino en mi USB Podcast Linux"
date: 2016-07-14
author: Juan Febles
categories: [podcastlinux]
img: PL/PL02.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL02
  image: https://podcastlinux.gitlab.io/images/PL/PL02.png
tags: [USB, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL02.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL02.mp3" type="audio/mpeg">
</audio>

Bienvenidos de nuevo a un episodio de Podcast Linux, tu podcast de GNU Linux.

 Un podcast para acercar al usuario de a pie el mundo del sistema operativo del pingüino.

 Gestor de Arranque: Resumen del programa

 Núcleo Kernel: Distribuciones live  
 <http://www.linux-es.org/livecd>

 Gestor de Paquetes: Unetbootin (Linux, Mac y Windows) y el comando para la terminal dd  
 <https://unetbootin.github.io/>  
 <https://es.wikipedia.org/wiki/UNetbootin>

 sudo fdisk -l  
 dd if=imagen.iso of=/dev/sdX bs=4M  
 dd if=/dev/zero of=/dev/sdX  
 dd if=/dev/urandom of=/dev/sdX

 Comunidad Linux: Youtuber Ruphone  
 <https://www.youtube.com/channel/UCh2jNxmLem7Q2DqgBvd6tQg>

 Área de Notificaciones: Espacio para los mensajes de los oyentes.  
 <http://gnulinux.pw/>  
 <http://salmorejogeek.com/>  
 <http://www.systeminside.net/>

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 Artistas:  
 LukHash  
 Cleric  
 Matti Paalanen  
 The Polish Ambassador  
 Kevin MacLeod

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1280800_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
