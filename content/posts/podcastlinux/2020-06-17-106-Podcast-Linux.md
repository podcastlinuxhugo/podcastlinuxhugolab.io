---
title: "#106 Hardware Vant Edge"
date: 2020-06-17
author: Juan Febles
categories: [podcastlinux]
img: PL/PL106.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL106
  image: https://podcastlinux.gitlab.io/images/PL/PL106.png
tags: [Hardware, Vant, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL106.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL106.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!   
Bienvenido a otra entrega de Podcast Linux, la número 106. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy tenemos un nuevo episodio Hardware, para dar a conocer un dispositivo GNU/Linux.  
Hace semanas que tengo el nuevo [Vant Edge](https://www.vantpc.es/edge), un ultraportátil ligero como una pluma, potente como un avión y con un diseño espectacular.  
Quédate conmigo para analizar lo último de [Vant](https://www.vantpc.es).  

Podcast Linux ya no forma parte de AVPodcast. Más info: <https://podcastlinux.com/95-Linux-Express/>  

**Enlaces de interés:**  
Vant Edge: <https://www.vantpc.es/edge>  

**Vídeos:**  
Unboxing: <https://youtu.be/DchTSiyG5M8>  
Review: <https://youtu.be/HEybEgnQ3js>  

La imagen del Vant Edge es propiedad de su propia empresa.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Podcast Linux - 7 AM: <https://archive.org/details/PodcastLinux-7AM>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f121061060_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC" title="CC SA-BY" width="88" height="31" />
