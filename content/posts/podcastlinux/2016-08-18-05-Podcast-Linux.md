---
title: "#05 Linux Connexion con Yoyo Fernández"
date: 2016-08-18
author: Juan Febles
categories: [podcastlinux]
img: PL/PL05.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL05
  image: https://podcastlinux.gitlab.io/images/PL/PL05.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL05.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL05.mp3" type="audio/mpeg">
</audio>

Iniciamos unos episodios especiales. Linux Connexion.  
 Con ellos pretendo acercar a las personas que han salido en la sección Comunidad Linux.  
 El motivo es bien sencillo.  
 Lo más importante de GNU/Linux, en mi humilde opinión, no es ni sus distribuciones, ni sus aplicaciones, ni su código, ni su filosofía. Son las personas que lo hacen posible, y que con su trabajo y esfuerzo, fomentan el uso de nuestro sistema operativo preferido.

 Hoy en este episodio especial Linux Connexion tenemos con nosotros a Yoyo Fernández, invitado del 3º episodio de la sección Comunidad Linux. Un Linuxero muy especial para mí.  
 @yoyo308  
 @salmorejogeek  
 @killallradio  
 http:/www.salmorejogeek.com

 Recuerda que puedes contactar conmigo de la siguiente manera:  
 Twitter @podcastlinux  
 podcastlinux@gmail.com

 No te pierdas ningún episodio nuevo. Suscríbete a mi podcast en Ivoox y Itunes.

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 Artistas:  
 Arstex

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1285850_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
