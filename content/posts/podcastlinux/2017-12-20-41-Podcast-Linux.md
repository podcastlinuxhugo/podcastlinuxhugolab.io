---
title: "#41 Gaming y GNU/Linux"
date: 2017-12-20
author: Juan Febles
categories: [podcastlinux]
img: PL/PL41.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL41
  image: https://podcastlinux.gitlab.io/images/PL/PL41.png
tags: [Gaming, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL41.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL41.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero. Bienvenido a otro episodio de Podcast Linux.  
 Mi nombre es Juan Febles y 2 veces al mes nos reunimos aquí, un lugar de encuentro para usuarios domésticos de escritorio del sistema operativo del ñu y el pingüino.  
 Ponte cómodo porque el episodio 41 Gaming y GNU/Linux comienza ya.

 En el Núcleo Kernel nos adentraremos en el mundo del gaming y la oferta que nos ofrece GNU/Linux.  
 En el Gestor de Paquetes jugaremos un poco a Super TuxKart, un arcade de carreras 3D para toda la familia, de gran calidad y muy adictivo.  
 <https://supertuxkart.net>  
 En Comunidad Linux nos acompañará Álvaro Nova, podcaster, youtuber, fotógrafo y gamer. Linuxero de corazón y gran conocedor de este mundo.  
 Twitter:[ https://twitter.com/DioxCorp](https://twitter.com/DioxCorp)  
 OneLinuxGamer: [https://www.ivoox.com/podcast-onelinuxgamer\_sq\_f119948\_1.html](https://www.ivoox.com/podcast-onelinuxgamer_sq_f119948_1.html)  
 Youtube: <https://www.youtube.com/user/DioxCorp>  
 Por último, en Área de Notificaciones, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 **Bibliografía para el episodio:**

 [https://es.wikipedia.org/wiki/Videojuegos\_en\_Linux](https://es.wikipedia.org/wiki/Videojuegos_en_Linux)  
 <https://www.gamingonlinux.com/>  
 <https://itsfoss.com/linux-gaming-distributions/>  
 <https://es.wikipedia.org/wiki/SuperTuxKart>  
 [ https://lignux.com/supertuxkart/](https://lignux.com/supertuxkart/)  
 <https://www.4mhz.es/>  
 <https://es.wikipedia.org/wiki/Wine>  
 <https://es.wikipedia.org/wiki/PlayOnLinux>  
 <https://en.wikipedia.org/wiki/Ataribox>  
 <https://es.wikipedia.org/wiki/OpenArena>  
 [https://es.wikipedia.org/wiki/0\_A.D.](https://es.wikipedia.org/wiki/0_A.D.)  
 <https://play0ad.com/>  
 <https://scratch.mit.edu/about>  
 <https://es.wikipedia.org/wiki/Steam>  
 <https://www.muylinux.com/2017/02/13/steam-3-000-juegos-linux/>  
 <https://es.wikipedia.org/wiki/SteamOS>  
 <https://www.softzone.es/2017/01/17/steamos-historia-presente-del-sistema-operativo-valve/>  
 <https://fossbytes.com/gaming-linux-distros-best/>

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Rolemusic – The black frame  
 Rolemusic – Poppies  
 Super TuxKart – Main Theme  
 Rolemusic – Leafless Quince Tree  
 Rolemusic – A ninja among culturachippers

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
