---
title: "#79 Especial Wacom Intuos S BT"
date: 2019-06-05
author: Juan Febles
categories: [podcastlinux]
img: PL/PL79.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL79
  image: https://podcastlinux.gitlab.io/images/PL/PL79.png
tags: [Hardware, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL79.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL79.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y tenemos el primer análisis de un periférico en el programa. Hoy quiero compartir mi experiencia con la tableta Wacom Intuos S BT.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 79, Especial Wacom Intuos S BT.

 Y también anunciarte que queda poco para el **3º aniversario** y quiero **pedirte un audio donde comentes lo que quieras**: felicitaciones, propuestas, lo que más te gusta, lo que crees que hay que mejorar…  
 Manda tu audio a Telegram: [@juanfebles](https://t.me/juanfebles) o por correo [podcastlinux@avpodcast.net](mailto:podcastlinux@avpodcast.net)

 **Enlaces de interés:  
 **<https://www.wacom.com/es-es/products/pen-tablets/wacom-intuos>  
 [https://linuxwacom.github.io/https://wiki.archlinux.org/index.php/Wacom\_tablet](https://linuxwacom.github.io/https://wiki.archlinux.org/index.php/Wacom_tablet)  
 <https://7grados.injiniero.es/2019/03/09/wacom-intuos-bt-s-en-ubuntu-18-04-bionic-beaver/>  
 <https://joshuawoehlke.com/wacom-intuos-and-xsetwacom-on-ubuntu-18-04/>  
 <https://bartssolutions.blogspot.com/2015/02/how-to-configure-wacom-intuos-express.html>**  
 **

 **Vídeos:  
 **Unboxing: <https://youtu.be/75kR0u7rQIs>  
 Instalación:[ https://youtu.be/05do8BTjMaY](https://youtu.be/05do8BTjMaY)

 **La imagen del hombre 3d utilizada en la portada, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):**[ https://pixabay.com/images/id-2320135/](https://pixabay.com/images/id-2320135/)  
 La imagen de la tableta Wacom es propiedad de su propia empresa.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 GarnaVutka – Inspiring Happy Acoustic (Full version) (2019) : <http://jamen.do/t/1655163>

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
