---
title: "#17 Linux Connexion con Alexandre Filgueira"
date: 2017-02-01
author: Juan Febles
categories: [podcastlinux]
img: PL/PL17.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL17
  image: https://podcastlinux.gitlab.io/images/PL/PL17.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL17.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL17.mp3" type="audio/mpeg">
</audio>

 Mi nombre es Juan Febles y hoy toca una entrevista Linux Connexion, los programas especiales de este podcast para acercarte a las personas o proyectos que han salido en la sección Comunidad Linux.

 El invitado de hoy fue nombrado el la sección Comunidad Linux del programa 16 sobre Antergos, y no podría ser otro que Alexandre Filgueira, creador y coordinador de esta distro nacida en España.

 Tuvimos una interesante charla, él desde Lima, Perú, donde reside ya desde hace varios años. Comentamos el presente y futuro de Antergos, el equipo que lo conforma, nuevas funcionalidades en mente, el panorama GNU/Linux y su trabajo actual.

 Ha sido un privilegio poder conversar con el artífice de mi distro preferida, la cual di buena cuenta de ella en el anterior episodio 16. Te invito a escuchar si no lo has hecho ya, para tener una visión total de Antergos.

 
 * Twitter: [@faidoc](https://twitter.com/faidoc)
 * Web: <http://www.alexfilgueira.com/>
 * Correo: [alexfilgueira@antergos.com](mailto:alexfilgueira@antergos.com)
 


 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 
 * LukHash – The Other Side
 * Stereofloat – Low Fligth
 
Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
