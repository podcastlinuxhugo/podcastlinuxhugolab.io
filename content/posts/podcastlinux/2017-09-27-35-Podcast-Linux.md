---
title: "#35 Formatos Libres"
date: 2017-09-27
author: Juan Febles
categories: [podcastlinux]
img: PL/PL35.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL35
  image: https://podcastlinux.gitlab.io/images/PL/PL35.png
tags: [Formatos, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL35.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL35.mp3" type="audio/mpeg">
</audio>


 Muy buenas Linuxero. Bienvenido a otro programa de Podcast Linux.  
 Mi nombre es Juan Febles y cada 15 días comparto, un nuevo tema o entrevista, del sistema operativo de escritorio que más nos gusta: GNU/Linux.

 En el **Núcleo Kernel** nos adentraremos en los formatos de archivos, contenedores, códecs y la importancia que sean libres y de estándar internacional.

 En el **Gestor de Paquetes** te hablaré de Inkscape, una aplicación de diseño gráfico vectorial libre.  
 <https://inkscape.org/es/>

 En **Comunidad Linux** compartiremos nuestra pasión por el ñu y el pingüino con Lorenzo Carbonell, más conocido como Atareao.  
 <https://www.atareao.es/>

 Por último, en **Área de Notificaciones**, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 **Bibliografía** para el episodio:  
 <http://www.madrimasd.org/informacionidi/analisis/analisis/analisis.asp?id=32382>  
 <https://blog.desdelinux.net/formatos-propietarios-y-sus-alternativas-libres/>  
 [http://www.eldiario.es/turing/software\_libre/estandar-documentos-Administraciones-Publicas-Espana\_0\_311469935.html](http://www.eldiario.es/turing/software_libre/estandar-documentos-Administraciones-Publicas-Espana_0_311469935.html)  
 <http://cesol.org.uy/contenido/ley-programas-computacion-formato-abierto-estandar>  
 [https://es.wikipedia.org/wiki/Formato\_abierto](https://es.wikipedia.org/wiki/Formato_abierto)  
 [https://es.wikipedia.org/wiki/Est%C3%A1ndar\_abierto](https://es.wikipedia.org/wiki/Est%C3%A1ndar_abierto)  
 [https://es.wikipedia.org/wiki/Advanced\_Audio\_Coding](https://es.wikipedia.org/wiki/Advanced_Audio_Coding)  
 <https://acrobat.adobe.com/es/es/why-adobe/about-adobe-pdf.html>  
 <https://es.wikipedia.org/wiki/PDF>  
 <https://archivisticafacil.wordpress.com/2015/05/24/la-normalizacion-en-los-archivos/>  
 <http://www.universidad.edu.uy/renderResource/index/resourceId/4760/siteId/1>  
 <https://fsfe.org/activities/os/os.en.html>  
 <https://www.gnu.org/software/plotutils/>  
 <https://www.gnu.org/philosophy/why-audio-format-matters.html>  
 <https://audio-video.gnu.org/docs/formatguide.html>  
 <http://multimedia.uoc.edu/blogs/fem/es/codec-y-contenedor/>  
 <http://explicandotecnologia.blogspot.com.es/2011/01/diferencia-entre-formatos-contenedores.html>

 Toda la **música utilizada** en este episodio se distribuye bajo la **licencia libre Creative Commons:**

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Igor Pumphonia – My love is real (Original Mix)  
 Igor Pumphonia – Spent me a little more time (Original Mix)  
 Igor Pumphonia – Principles of lust (Original Mix)  
 Igor Pumphonia – Feel the magic (2nd version)  
 Igor Pumphonia – Around the earth (Original Mix)

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
