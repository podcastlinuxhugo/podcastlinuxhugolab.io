---
title: "#39 GNU/Linux y Móviles"
date: 2017-11-22
author: Juan Febles
categories: [podcastlinux]
img: PL/PL39.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL39
  image: https://podcastlinux.gitlab.io/images/PL/PL39.png
tags: [Móviles, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL39.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL39.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero. Bienvenido a otro episodio de Podcast Linux.  
 Mi nombre es Juan Febles y quincenalmente nos encontramos en este programa para usuarios de escritorio del sistema operativo del ñu y el pingüino.  
 Sube el volumen porque el episodio 39 GNU/Linux y móviles comienza ya.

 En el Núcleo Kernel intentaremos conocer el mundo de la telefonía móvil y sus coqueteos con GNU/Linux.  
 En el Gestor de Paquetes ahondaremos en KDE Connect, la aplicación para que tu ordenador y smartphone se entiendan a la perfección.  
 <https://community.kde.org/KDEConnect>  
 En Comunidad Linux nos acompañará Aleix Pol, desarrollador, vicepresidente de KDE Internacional y miembro activo en Plasma Mobile.  
 <https://twitter.com/aleixpol>  
 [http://www.ivoox.com/podcast-podcast-kde-espana\_sq\_f1249423\_1.html](http://www.ivoox.com/podcast-podcast-kde-espana_sq_f1249423_1.html)  
 Por último, en Área de Notificaciones, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.  
 Rifa solidaria: <http://www.asexve.es/>

 **Bibliografía para el episodio:**  
 <https://en.wikipedia.org/wiki/Maemo>  
 [https://en.wikipedia.org/wiki/Nokia\_N900](https://en.wikipedia.org/wiki/Nokia_N900)  
 [https://en.wikipedia.org/wiki/Ubuntu\_Edge](https://en.wikipedia.org/wiki/Ubuntu_Edge)  
 <https://www.indiegogo.com/projects/ubuntu-edge#/>  
 <http://maruos.com/#/>  
 <https://www.linuxadictos.com/linux-on-galaxy-la-nueva-convergencia-samsung-gnulinux.html>  
 <https://www.muycomputer.com/2014/07/14/cinco-linux-para-smartphones/>  
 [https://wikipedia.org/wiki/List\_of\_open-source\_mobile\_phones](https://wikipedia.org/wiki/List_of_open-source_mobile_phones)  
 <https://www.gnu.org/philosophy/android-and-users-freedom.es.html>  
 <https://m.xataka.com/moviles/asi-es-replicant-la-version-libre-de-android-que-quiere-conquistar-tu-smartphone-en-2017/>  
 <https://www.ubuntizando.com/postmarketos-proyecto-basado-en-linux-permitira-extender-la-vida-util-de-tu-smartphone-por-10-anos/>  
 <https://lamiradadelreplicante.com/2017/08/21/debian-no-se-olvida-de-los-dispositivos-moviles/>

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Alexey Anisimov – Summer Pop  
 Alexey Anisimov – Corporate House  
 Alexey Anisimov – Summer  
 Alexey Anisimov – Ambient Corporate Tecnology  
 Alexey Anisimov – Ambient Minimal

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
