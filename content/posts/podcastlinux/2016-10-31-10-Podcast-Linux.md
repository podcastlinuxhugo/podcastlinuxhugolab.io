---
title: "#10 Raspberry Pi y GNU/Linux"
date: 2016-10-31
author: Juan Febles
categories: [podcastlinux]
img: PL/PL10.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL10
  image: https://podcastlinux.gitlab.io/images/PL/PL10.png
tags: [Raspberry Pi, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL10.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL10.mp3" type="audio/mpeg">
</audio>

![El Sistema Operativo Rasbian con el Entorno de Escritorio PIXEL funcionando desde la Raspberry Pi 3]({{< ABSresource url="/images/PL/Cvc5hzrWIAAGNqs-300x162.jpg" >}})

 Muy buenas Linuxero. Bienvenido a otro episodio más de Podcast Linux.  
 Mi nombre es [Juan Febles](https://twitter.com/Juan Febles__febles) y cada quincena tengo una cita contigo para hablar de lo que más nos gusta: GNU/Linux.

 En el Núcleo Kernel hablaré de la [Raspberry Pi](https://www.raspberrypi.org/) y su íntima relación con GNU/Linux.

 ![Las 3 Raspberry Pi, la 1, la 2 y la 3.]({{< ABSresource url="/images/PL/IMG_20160617_181201-300x169.jpg" >}})

 ![Raspberry Pi 3]({{< ABSresource url="/images/PL/IMG_20160615_191919-300x225.jpg" >}})

 En el Gestor de Paquetes le damos un repaso a [Kodi](https://kodi.tv/about/), un Media Center multiplataforma.

 ![Probando el media center Kodi desde la Raspberry Pi en mi televisor. ]({{< ABSresource url="/images/PL/CvX308VXgAAPqu2-300x169.jpg" >}})

 El invitado a Comunidad Linux es [Gabriel Viso](https://twitter.com/gvisoc), podcaster del podcast [Pitando.net](http://pitando.net/).  
 Por último, en Área de Notificaciones, le daré un repaso a los mensajes que he recibido en este último mes.


 Nos vemos en 15 días Linuxeros!!!!  
 Un abrazo muy fuerte a todos.

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Felixjd – Make Believe  
 Gyako – Illusion  
 Olivier Girardot – Sneak peek  
 Boogie Belgique – Smile  
 Gabriel Viso – Sintonía Pitando.net  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1285850_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
