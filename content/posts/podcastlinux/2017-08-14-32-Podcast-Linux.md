---
title: "#32 Linux Connexion con Reciclanet"
date: 2017-08-14
author: Juan Febles
categories: [podcastlinux]
img: PL/PL32.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL32
  image: https://podcastlinux.gitlab.io/images/PL/PL32.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL32.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL32.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero!!!  
 Bienvenido a una nueva entrega, exactamente la número 32, de Podcast Linux. Un fuerte abrazo de quien te habla, Juan Febles. Siempre intento en Linux Connexion entrevistar a todos las personas y proyectos que pasan por la sección Comunidad Linux. Reciclanet lo hizo en el episodio 3 y por fin hoy vamos a poder conocerlos. Reciclanet, Asociación bilbaína Educativa, Ecologista y Solidaria, formada por voluntarios que fomentan el uso de GNU/Linux en ordenadores reciclados para así darles una segunda vida.  
 Tenemos con nosotros a Ramón Barrenetxea, Coordinador del proyecto.

 Métodos de contacto de Reciclanet:

 Web: <http://www.reciclanet.org/>  
 Correo: [reciclanet@reciclanet.org](mailto:reciclanet@reciclanet.org)  
 Twitter: [@reciclanet](https://twitter.com/reciclanet)

 Antes de pasar a los métodos de contacto te animo a que participes y colabores en el [Maratón Linuxero](https://maratonlinuxero.github.io/), evento que se realizará el 3 de septiembre desde las 3 de la tarde hora española con directos sobre software libre. Tienes toda la información en [maratonlinuxero.github.io.](https://maratonlinuxero.github.io/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
