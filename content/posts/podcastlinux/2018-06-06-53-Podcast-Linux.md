---
title: "#53 Libera tu móvil"
date: 2018-06-06
author: Juan Febles
categories: [podcastlinux]
img: PL/PL53.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL53
  image: https://podcastlinux.gitlab.io/images/PL/PL53.png
tags: [Móvil, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL53.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL53.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy quiero compartir contigo mis experiencias con dispositivos móviles y GNU/Linux. Fiel a nuestra cita quincenal, vamos a disfrutar en torno a media hora de un nuevo episodio.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 53, libera tu móvil, una nuevo capítulo que empieza ya.

 En el **Núcleo Kernel** compartiré los pasos y reflexiones a la hora de instalar roms libres en diferentes móviles.  
 En el **Gestor de Paquetes** hablaremos de ADB y Fastboot, dos comandos indispensables para cambiar el sistema operativo de tu móvil Android.

 adb devices  
 adb reboot  
 adb reboot-recovery  
 adb reboot-bootloader  
 fastboot devices  
 fastboot oem unlock  
 fastboot reboot  
 fastboot reboot-bootloader  
 fastboot flash partición archivo.img

 En **Comunidad Linux** te presentaré a Marcos Costales, podcaster y desarrollador de aplicaciones en Ubuntu Touch.  
 Podcast: [https://www.ivoox.com/podcast-ubuntu-otras-hierbas\_sq\_f1412582\_1.html](https://www.ivoox.com/podcast-ubuntu-otras-hierbas_sq_f1412582_1.html)  
 Twitter: <https://twitter.com/costalesdev>  
 Finalizaremos este episodio escuchando comentarios de los oyentes en la última sección, el **Área de Notificaciones**.

 La **imagen del Nexus 5 utilizada** se ha liberado desde Pixabay con licencia Creative Commons 0:  
 <https://pixabay.com/photo-1294370/>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other SideLukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 BACKIT – Mystic Construct  
 BACKIT – Techy  
 BACKIT – Deep Ideas  
 BACKIT – Solar Fields  
 BACKIT – Lucky Waves

 **Bibliografía para realizar el episodio:**

 <https://www.plasma-mobile.org/>  
 <https://ubports.com/>  
 <https://lineageos.org/>  
 <https://twrp.me/>  
 <https://f-droid.org/>  
 <https://freeyourandroid.blogspot.com.es>  
 <https://www.androidpit.es/que-es-adb-comandos-mas-importantes>  
 <https://www.proandroid.com/adb-fastboot-instalarlos-guia-definitiva/>  
 <https://www.movilzona.es/tutoriales/android/root/principales-comandos-para-adb-y-fastboot-guia-basica/>  
 <https://ubunlog.com/open-store-instalar-en-ubuntu-phone/>  
 <https://sailfishos.org/>  
 <https://wiki.ubuntu.com/costales>  
 <https://compilando.audio/index.php/2017/12/22/podcast-19-ciberseguridad-con-yaiza-rubio-y-ubucon-2018-en-gijon/>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
