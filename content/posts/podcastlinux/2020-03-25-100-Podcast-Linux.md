---
title: "#100 Especial Preguntas y Respuestas"
date: 2020-03-25
author: Juan Febles
categories: [podcastlinux]
img: PL/PL100.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL100
  image: https://podcastlinux.gitlab.io/images/PL/PL100.png
tags: [Especial, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL100.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL100.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Hoy estamos de celebración. Hemos llegado al episodio 100. Para celebrar este momento realizaremos un episodio muy especial.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 100, Especial Preguntas y respuestas.  

Enlaces de interés:  
Interfaz de Audio: <https://www.behringer.com/Categories/Behringer/Computer-Audio/Interfaces/UMC204HD/p/P0BK0#googtrans(en|es)>  
Micrófono: <https://www.behringer.com/Categories/Behringer/Microphones/Dynamic/XM8500/p/P0120#googtrans(en|es)>  
Tagoror Educativo Podcast: <https://www.ivoox.com/podcast-tagoror-educativo-podcast_sq_f1251790_1.html>  
Onda Salle: <https://www.ivoox.com/podcast-onda-salle_sq_f1151497_1.html>  

Enlaces de interés:  
Web MaratonPod: <https://www.maratonpod.es/>  
Youtube MaratonPod: <https://www.youtube.com/MaratonPod>  
Twitter MaratonPod: <https://twitter.com/maratonpod>  

Toda la música utilizada en este episodio se distribuye bajo la licencia Creative Commons:  
MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
Roda Music – Short Trip: <https://www.free-stock-music.com/roa-music-short-trip.html>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
