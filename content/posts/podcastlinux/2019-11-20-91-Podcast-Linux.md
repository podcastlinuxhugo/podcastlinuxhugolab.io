---
title: "#91 Linux Connexion con Luis del Valle"
date: 2019-11-20
author: Juan Febles
categories: [podcastlinux]
img: PL/PL91.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL91
  image: https://podcastlinux.gitlab.io/images/PL/PL91.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL91.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL91.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!

Bienvenido a otra entrega, la número 91, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy está con nosotros Luis del Valle, apasionado del mundo de Arduino y del Movimiento Maker, está detrás del blog Programarfacil.com, del podcast La tecnología para Todos y del Campus de Programarfacil.  

Recordar a los oyentes que estamos en una sala Jitsi para esta charla, un servicio libre para videoconferencias, y agradecer a Neodigit, nuestro proveedor de alojamiento y servicios de confianza de habla hispana para todo Avpodcast.  

Dentro de poco llegaremos al episodio 100. A ver qué se nos ocurre para festejar ese episodio tan redondo.  

**Enlaces de interés:**

Blog Programar Facíl: [https://programarfacil.com](https://programarfacil.com)  
Academia Programar Fácil: [https://campus.programarfacil.com/?menu-curso-arduino](https://campus.programarfacil.com/?menu-curso-arduino)  
Podcast La Tecnología para Todos: [https://programarfacil.com/categoria/podcast/](https://programarfacil.com/categoria/podcast/)  
Canal Youtube Programar Fácil: [https://www.youtube.com/channel/UCSGPM_kJJ6jiTG4_y_mJXLA](https://www.youtube.com/channel/UCSGPM_kJJ6jiTG4_y_mJXLA)

Vídeo de Mariano Sigman: [https://youtu.be/bFrSwbhe8Bo](https://youtu.be/bFrSwbhe8Bo)

La **imagen utilizada en la portada y el logo** han sido cedidos por Luis del Valle y es de su propiedad.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Punch Deck – The Traveler: [https://www.free-stock-music.com/punch-deck-the-traveler.html](https://www.free-stock-music.com/punch-deck-the-traveler.html)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1291890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
