---
title: "#88 Creative Commons"
date: 2019-10-09
author: Juan Febles
categories: [podcastlinux]
img: PL/PL88.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL88
  image: https://podcastlinux.gitlab.io/images/PL/PL88.png
tags: [Creative Commons, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL88.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL88.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy tenemos episodio formal. En poco más de media hora te comentaré qué son las Creative Commons, su filosofía y sus diferentes licencias.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 88, Creative Commons.

En el núcleo Kernel le daremos un repaso a las diferentes [Creative Commons](https://creativecommons.org/), su significado y diferencias a otras licencias. En Gestor de Paquetes conoceremos [Search Creative Commons](https://search.creativecommons.org/), un buscador de contenido con este tipo de licencias para que puedas encontrar obras con algunos derechos reservados. En Comunidad Linux, tendremos a [Irene Soria](https://twitter.com/arenitasoria), diseñadora gráfica, activista de la Cultura Libre y el Software Libre y miembro de Creative Commons México. Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.

Las **imágenes utilizadas en la portada, el hombre 3D** se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): [https://pixabay.com/images/id-4373612/](https://pixabay.com/images/id-4373612/)  
Los **logotipos de Creative Commons y los símbolos de sus licencias** son propiedad de esta organización.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Arhur Yul – Cinema Intro: [https://www.jamendo.com/track/1503848/](https://www.jamendo.com/track/1503848/)  
GarnaVutka – Corporate: [https://www.jamendo.com/track/1354699](https://www.jamendo.com/track/1354699)  
Zoran Sebic – Innovative Logo: [https://www.jamendo.com/track/1487845](https://www.jamendo.com/track/1487845)  
GarnaVutka – Crystal Wellcome Logo: [https://www.jamendo.com/track/1508030](https://www.jamendo.com/track/1508030)  
GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): [https://www.jamendo.com/track/1464611](https://www.jamendo.com/track/1464611)  
Mrpetelee Rzd5 – Upbeat: [https://www.free-stock-music.com/mrpetelee-rzd5-upbeat.html](https://www.free-stock-music.com/mrpetelee-rzd5-upbeat.html)  
MBB – Destination: [https://www.free-stock-music.com/mbb-destination.html](https://www.free-stock-music.com/mbb-destination.html)  
Free Music – Sky: [https://www.free-stock-music.com/fm-freemusic-sky.html](https://www.free-stock-music.com/fm-freemusic-sky.html)  
Serge Narcissoff – Upbeat Acoustic Fun: [https://www.free-stock-music.com/serge-narcissoff-upbeat-acoustic-fun.html](https://www.free-stock-music.com/serge-narcissoff-upbeat-acoustic-fun.html)

**Guía Rápida para una web libre:** [http://softlibre.com.ar/blog/una-guia-rapida-por-la-red-libre/](http://softlibre.com.ar/blog/una-guia-rapida-por-la-red-libre/)

**Bibliografía utilizada para realizar este episodio:  
**[https://cursos.com/que-es-copyright/](https://cursos.com/que-es-copyright/)  
[https://cursos.com/licencias-creative-commons/](https://cursos.com/licencias-creative-commons/)  
[https://es.wikipedia.org/wiki/Derecho_de_copia_privada](https://es.wikipedia.org/wiki/Derecho_de_copia_privada)  
[https://www.eipe.es/blog/6-tipos-de-licencias-creative-commons/](https://www.eipe.es/blog/6-tipos-de-licencias-creative-commons/)  
[https://www.culturaydeporte.gob.es/cultura-mecd/areas-cultura/propiedadintelectual/la-propiedad-intelectual.html](https://www.culturaydeporte.gob.es/cultura-mecd/areas-cultura/propiedadintelectual/la-propiedad-intelectual.html)  
[https://www.julianmarquina.es/cc-search-el-mejorado-buscador-de-creative-commons-en-el-que-localizar-mas-de-300-millones-de-imagenes/](https://www.julianmarquina.es/cc-search-el-mejorado-buscador-de-creative-commons-en-el-que-localizar-mas-de-300-millones-de-imagenes/)  
[https://www.gnu.org/licenses/copyleft.es.html](https://www.gnu.org/licenses/copyleft.es.html)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1288888_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
