---
title: "#48 Linux Connexion Ubucon Europe 18"
date: 2018-03-28
author: Juan Febles
categories: [podcastlinux]
img: PL/PL48.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL48
  image: https://podcastlinux.gitlab.io/images/PL/PL48.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL48.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL48.mp3" type="audio/mpeg">
</audio>

 Muy buenas, amante de GNU/Linux y el Software Libre. Estás escuchando Podcast Linux, el programa para usuarios y usuarias domésticos del sistema operativo del ñu y el pingüino.  
 Hoy, en el episodio 48, disfrutaremos de un Linux Connexion, las entrevistas para conocer más de cerca los proyectos y personas que normalmente pasan por la sección Comunidad Linux, aunque este no es el caso.  
 Y esto es porque un evento tan importante como el que vamos a hablar tiene tanta relevancia como para traerlos directamente a un programa. Estamos con algunos de los responsables de Ubucon Europa 18, las conferencias sobre Ubuntu y software libre que se realizan desde hace dos años en el viejo continente. Esta es su tercera entrega.

 Tenemos con nosotros a Marcos Costales, Javier Teruelo, Manuel Cogolludo y Sergi Quiles.

 Contacta con **Ubucon Europa 18**:  
 Telegram: <https://t.me/ubuconEU2018>  
 Twitter: <https://twitter.com/ubuconeurope>  
 Google +: <https://plus.google.com/+UbuConEurope>  
 Facebook:[ https://www.facebook.com/UbuConEurope/](https://www.facebook.com/UbuConEurope/)

 **Crónicas Ubucons:**  
 <http://thinkonbytes.blogspot.com.es/2017/09/2-ubucon-europea.html>  
 <https://www.innerzaurus.com/ubuntu-touch/articulos-touch/cronicas-la-ubucon-paris-2017>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other Side  
 Dj Rostej – Long Way

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
