---
title: "#102 Linux Connexion con VPS Educativo Libre"
date: 2020-04-22
author: Juan Febles
categories: [podcastlinux]
img: PL/PL102.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL102
  image: https://podcastlinux.gitlab.io/images/PL/PL102.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL102.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL102.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!
Bienvenido a otra entrega de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles.  
Estamos con [Pedro Mosqueteroweb](https://twitter.com/mosqueteroweb). Como bien sabes él es podcaster, bloguero y profesor en Formación profesional en la rama de Computación.  

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y agradecer a [Neodigit](https://neodigit.net/), nuestro proveedor de alojamiento y servicios de confianza de habla hispana para todo [Avpodcast](https://avpodcast.net/).  

Enlaces de interés:  
Web: <https://educacion.mosqueteroweb.eu/>  
Grupo Telegram: <https://t.me/joinchat/AFleDUqtfvhZJ61K9yS0aA>  
Twitter: <https://twitter.com/mosqueteroweb>  
Empresa hosting: <https://clouding.io/>  
 

Las imágenes utilizadas en la portada, los logos de los servicios son propiedad de esos mismos proyectos y el hombre 3D se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/images/id-1834091>  

Toda la música utilizada en este episodio se distribuye bajo la licencia Creative Commons:  

MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 Roa Music – Spring: <https://www.free-stock-music.com/roa-music-spring.html>  


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
