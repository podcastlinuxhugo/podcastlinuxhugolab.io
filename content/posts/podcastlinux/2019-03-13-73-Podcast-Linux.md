---
title: "#73 Audio y GNU/Linux"
date: 2019-03-13
author: Juan Febles
categories: [podcastlinux]
img: PL/PL73.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL73
  image: https://podcastlinux.gitlab.io/images/PL/PL73.png
tags: [Audio, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL73.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL73.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy volvemos con un episodio formal. En poco más de media hora te comentaré cómo sacarle todo el jugo al audio desde GNU/Linux y el Software Libre.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 73, Audio y GNU/Linux.

 En el núcleo Kernel le daremos un repaso al mundo del audio en GNU/Linux. En Gestor de Paquetes [Ardour](http://www.ardour.org/), el editor multipista profesional con el que realizo Podcast Linux. Conoceremos a [JoseGDF](https://www.josegdf.net/) en Comunidad Linux, apasionado de la música y linuxero con muchos trabajos, podcast y cursos sobre este apasionante mundo. Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.

 **Lista de programas:**

 **Audio Digital:**  
 Muestreo Analógico Digital: [https://es.wikipedia.org/wiki/Muestreo\_digital](https://es.wikipedia.org/wiki/Muestreo_digital)  
 MIDI: <https://www.midi.org/>

 **Servidores de audio:**  
 ALSA: <https://www.alsa-project.org>  
 PulseAudio: <https://www.freedesktop.org/wiki/Software/PulseAudio/>  
 Jack: <http://www.jackaudio.org/>  
 PipeWire: <https://pipewire.org/>

 **Complementos:**  
 Plugins LADSPA: <https://www.ladspa.org/>  
 Calf Studio Gear: <https://calf-studio-gear.org/>

 **Formatos Libres:**  
 MusicXML:[ https://www.musicxml.com/](https://www.musicxml.com/)  
 Ogg: <https://www.xiph.org/ogg/>  
 Flac: <https://xiph.org/flac/>

 **Creación musical:**  
 Musescore: <https://musescore.org/es>  
 Frescobaldi: <http://www.frescobaldi.org/>  
 Linux Multimedia Studio (LMMS): <https://lmms.io/>  
 Guitarix: <http://guitarix.org/>  
 Hydrogen: <http://hydrogen-music.org/>  
 Qtractor: <https://qtractor.sourceforge.io/>  
 MusE: <http://www.muse-sequencer.org/>  
 RoseGarden: <https://sourceforge.net/projects/rosegarden/>

 **Grabación y edición:**  
 Audacity: <https://www.audacityteam.org/>  
 Ardour: <http://www.ardour.org/>

 **Emisión:**  
 Icecast: <http://www.icecast.org/>  
 Mixxx: <https://mixxx.org/> . [Curso DJMao Mix](https://www.youtube.com/watch?v=1aaaLKeIXiI&list=PLt_wY1uOEKnSj-Sebg9jku4hLAONMtxto)  
 IDJC: <http://idjc.sourceforge.net/>  
 Butt: <https://danielnoethen.de/>

 **Estaciones de radio:**  
 GRadio: <http://www.g-radio.org/>  
 Rivendell: <http://rivendellaudio.org/>

 **Utilidades:**  
 Sound Converter: <https://soundconverter.org/>  
 PulseEffects: <https://github.com/wwmm/pulseeffects>

 **Distribuciones:**  
 Ubuntu Studio: <https://ubuntustudio.org/>  
 EterTics: <https://gnuetertics.org/>

 **JoséGDF:**  
 Blog: <https://www.josegdf.net/>  
 Grupo Telegram: <https://t.me/HomeStudioLibre>  
 Jamendo: <https://www.jamendo.com/artist/370469/magia-y-tecnologia>

 Las **imagen utilizada en la portada, **se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/images/id-1019815/>

 Toda la **música utilizada en este episodio** se distribuye bajo licencias **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 Arhur Yul – Cinema Intro: <https://www.jamendo.com/track/1503848/>  
 GarnaVutka – Corporate: <https://www.jamendo.com/track/1354699>  
 Zoran Sebic – Innovative Logo: <https://www.jamendo.com/track/1487845>  
 GarnaVutka – Crystal Wellcome Logo: <https://www.jamendo.com/track/1508030>  
 JoséGDF – Protium (Hydrogen-1) (2017): <http://jamen.do/t/1495679>  
 JoséGDF – Deuterium (Hydrogen-2) (2017): <http://jamen.do/t/1495748>  
 JoséGDF – Tritium (Hyrogen-3) (2017): <http://jamen.do/t/1495942>  
 JoséGDF – Luna Roja (2015): <http://jamen.do/t/1274994>  
 JoséGDF – Recuerdos de Otoño (2014): <http://jamen.do/t/1161273>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
