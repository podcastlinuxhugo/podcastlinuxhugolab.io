---
title: "#25 GNU/Linux y NAS"
date: 2017-05-24
author: Juan Febles
categories: [podcastlinux]
img: PL/PL25.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL25
  image: https://podcastlinux.gitlab.io/images/PL/PL25.png
tags: [NAS, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL25.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL25.mp3" type="audio/mpeg">
</audio>


 Muy buenas Linuxero. Bienvenido a otro programa de Podcast Linux.  
 Mi nombre es Juan Febles y cada 15 días comparto un nuevo tema o entrevista del sistema operativo de escritorio que más nos gusta: GNU/Linux.  
 Ponte cómodo porque el episodio 25 [GNU/Linux y NAS](https://avpodcast.net/blog/nas/) empieza ya.

 En el Núcleo Kernel nos adentraremos en el mundo NAS y las posibilidades que nos da GNU/Linux.

 En el Gestor de Paquetes te hablaré de NextCloud, tu propia nube en tu hogar.

 <https://nextcloud.com/>  
 [https://colaboratorio.net/davidochobits/sysadmin/2016/nextcloud-almacenamiento-la-nube-mucho-mas-primera-parte/](https://ugeek.github.io/003.-nextcloud-instalar-tu-nube-en-menos-2-minutos/)  
 <https://ugeek.github.io/003.-nextcloud-instalar-tu-nube-en-menos-2-minutos/>

 En Comunidad Linux abriremos las puertas a Ugeek, podcaster y bloguero linuxero que trastea bastante con los servicios en red.

 Twitter:[ @uGeekPodcast](https://twitter.com/uGeekPodcast)  
 Blog: <https://ugeek.github.io/>  
 Canal de Telegram: [t.me/ugeek](https://t.me/ugeek)  
 Feed: <http://feeds.feedburner.com/ugeek>

 Por último, en Área de Notificaciones, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 rsync -avhP [origen] [destino]  
 ls -latrh  
 df -h  
 egrep -R ‘patron’ * –include=*.txt

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 MC Project – Smile  
 MC Project – Summer Trip  
 MC Project – Summer Ibiza  
 MC Project – Gets Up New Day  
 Las imágenes utilizadas en la portada son gratuitas, editables y propiedad de Freepik.es  
 Los logos de NextCloud y Syncthing son propiedad de estas empresas.

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
