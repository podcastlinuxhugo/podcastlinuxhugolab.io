---
title: "#70 Alternativas libres a Google"
date: 2019-01-30
author: Juan Febles
categories: [podcastlinux]
img: PL/PL70.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL70
  image: https://podcastlinux.gitlab.io/images/PL/PL70.png
tags: [Google, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL70.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL70.mp3" type="audio/mpeg">
</audio>


 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy volvemos después de mucho tiempo con un episodio formal. En torno a media hora reflexionaremos sobre alternativas libres a Google.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 70 abordando servicios y productos alternativos al gran magnate del Big Data. ¿Te vienes?

 En el núcleo Kernel le daremos un repaso a alternativas al coloso Google.  
 En Gestor de Paquetes analizaremos [DuckDuckGo](https://duckduckgo.com/), un buscador que promete respetar nuestra privacidad.  
 Conoceremos a [Victorhck](https://victorhckinthefreeworld.com/) en Comunidad Linux, activista y comprometido con el Software Libre.  
 Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.

 **Alternativas Libres a Google:**

 Google Search: [Duck Duck Go](https://duckduckgo.com/)  
 Correo Gmail: [Correo Disroot.org](https://disroot.org/es) , [tutanota.com](https://tutanota.com/es/)  
 Google Chrome: [Firefox](https://www.mozilla.org/es-ES/firefox/)  
 Google Drive: [NextCloud](https://nextcloud.com/), [Syncthing](https://syncthing.net/)  
 Google Meet (HangOuts): [Jitsi](https://meet.jit.si/)  
 Google Maps:[ Open Street Map](https://www.openstreetmap.org), [Maps.me](https://f-droid.org/es/packages/com.github.axet.maps/)  
 Google Earth: [Marble](https://marble.kde.org/)  
 Google Home: [Mycroft](https://mycroft.ai/)  
 Google Photos: [NextCloud Photos](https://nextcloud.com/files/), [Syncthing](https://syncthing.net/)  
 Youtube: [MediaGoblin](https://www.mediagoblin.org/), [PeerTube](https://joinpeertube.org/en/), [Archive.org](https://archive.org/)  
 Google Keep: [Notas NextCloud](https://nextcloud.com/)  
 Google Calendar: [Calendario NextCloud](https://nextcloud.com/groupware/)  
 Traductor Google: [Apertium](https://www.apertium.org)  
 Google Classroom: [Moodle](https://moodle.org/)  
 Google Play: [F Droid](https://f-droid.org/)  
 Android: [UBPorts](https://ubports.com/es_ES/), [LineageOS](https://lineageos.org/), [PureOS](https://www.pureos.net/)  
 Chromebook: [Vant Mini Moove](http://www.vantpc.es/producto/minimoove-n5000), [Pinebook](https://www.pine64.org/?page_id=3707)  
 ChromeOS: [GNU/Linux](https://es.wikipedia.org/wiki/GNU/Linux)  
 Google +: [GNU Social](https://gnu.io/social/), [Diaspora](https://joindiaspora.com/), [Mastodon](https://mastodon.social/about)  
 Pixel: [Librem 5 (Purism)](https://puri.sm/products/librem-5/)

 Las **imágenes utilizadas en la portada, **se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/photo-1019993> y <https://pixabay.com/photo-1762248>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 Arhur Yul – Cinema Intro: <https://www.jamendo.com/track/1503848/>  
 GarnaVutka – Corporate: <https://www.jamendo.com/track/1354699>  
 Zoran Sebic – Innovative Logo: <https://www.jamendo.com/track/1487845>  
 GarnaVutka – Crystal Wellcome Logo: <https://www.jamendo.com/track/1508030>  
 GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): <https://www.jamendo.com/track/1464611>  
 GarnaVutka – Ambient Corporate Inspiring Background Full version: <http://jamen.do/t/1396691>  
 GarnaVutka – Funny Funk Disco Groove Short version: <http://jamen.do/t/1354273>  
 GarnaVutka – Inspiring Full version: <http://jamen.do/t/1352946>  
 GarnaVutka – Happy: <http://jamen.do/t/1523734>  
 GarnaVutka – The Future Bass: <http://jamen.do/t/1528074>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
