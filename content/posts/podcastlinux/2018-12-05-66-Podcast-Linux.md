---
title: "#66 Especial Pinebook"
date: 2018-12-05
author: Juan Febles
categories: [podcastlinux]
img: PL/PL66.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL66
  image: https://podcastlinux.gitlab.io/images/PL/PL66.png
tags: [Hardware, Pinbook, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL66.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL66.mp3" type="audio/mpeg">
</audio>

 Muy buenas amante de GNU/Linux y el Software Libre.  
 Bienvenido a un nuevo episodio, el número 66, de Podcast Linux. Un saludo muy fuerte y cariñoso de quien te habla, Juan Febles.  
 Hoy te dejo un especial sobre [Pinebook](https://www.pine64.org/?page_id=3707), el netbook ARM de bajo coste y Open Source de [Pine64](https://www.pine64.org/).

 Sorteo:  
 Gana unas pegatinas Pinebook sencillamente. Sígueme en Twitter y retuitea el siguiente tuit y podrán ser tuyas: <https://twitter.com/podcastlinux/status/1070194887344164864>

 **Enlaces:  
 **<https://www.pine64.org/>  
 [https://www.pine64.org/?page\_id=3707](https://www.pine64.org/?page_id=3707)  
 <https://github.com/pine64dev/PINE64-Installer/blob/master/README.md>  
 [https://docs.google.com/spreadsheets/d/1\_S3A3OmpLpnLTIRnhK31IDZOonq2-CFGIxT1fgM5GqY/edit#gid=0](https://docs.google.com/spreadsheets/d/1_S3A3OmpLpnLTIRnhK31IDZOonq2-CFGIxT1fgM5GqY/edit#gid=0)  
 <https://www.aliexpress.com/snapshot/0.html?spm=a2g0s.9042647.0.0.4f0063c0m4frQ5&orderId=507409149712724&productId=1000005632582>

 La **imagen del hombre 3D utilizada en la portada **se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0: ](https://creativecommons.org/publicdomain/zero/1.0/deed.es)<https://pixabay.com/photo-2322811>  
 La imagen del pinebook se ha liberado desde [wikimedia](https://commons.wikimedia.org/):[ https://commons.wikimedia.org/wiki/File:Pinebook\_11\_Inch.jpg](https://commons.wikimedia.org/wiki/File:Pinebook_11_Inch.jpg)

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 GarnaVutka – Inspiring Spring Full version:[ http://jamen.do/t/1558187](http://jamen.do/t/1558187)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
