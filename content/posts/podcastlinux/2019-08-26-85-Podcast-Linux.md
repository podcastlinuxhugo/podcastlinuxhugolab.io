---
title: "#85 Linux Connexion con Paco Estrada"
date: 2019-08-26
author: Juan Febles
categories: [podcastlinux]
img: PL/PL85.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL85
  image: https://podcastlinux.gitlab.io/images/PL/PL85.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL85.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL85.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega, la número 85, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy está con nosotros [Paco Estrada](https://twitter.com/pacoestrada77), la voz del Software Libre.  
Podcaster y divulgador de GNU/Linux, ganador a mejor medio en los Open Adwars de 2018 y un gran comunicador, no podía faltar en este podcast.

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y agradecer a [Neodigit](https://www.neodigit.net/), nuestro proveedor de alojamiento y servicios de confianza de habla hispana para todo Avpodcast.

**Enlaces de interés:**  
Compilando Podcast: [https://compilando.audio/](https://compilando.audio/)  
Novuslinux: [http://novuslinux.blogspot.com/](http://novuslinux.blogspot.com/)  
Open Awards: [https://openexpoeurope.com/es/oe2019/open-awards-2019/](https://openexpoeurope.com/es/oe2019/open-awards-2019/)  
Open Expo Podcast: [https://openexpoeurope.com/es/category/podcast/](https://openexpoeurope.com/es/category/podcast/)

Las **imagen utilizada en la portada** ha sido cedida por el propio Paco Estrada.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Mixaund – Ready to go: [https://www.free-stock-music.com/mixaund-ready-to-go.html](https://www.free-stock-music.com/mixaund-ready-to-go.html)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1285850_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
