---
title: "#15 Linux Connexion con Jen0f0nte"
date: 2017-01-06
author: Juan Febles
categories: [podcastlinux]
img: PL/PL15.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL15
  image: https://podcastlinux.gitlab.io/images/PL/PL15.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL15.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL15.mp3" type="audio/mpeg">
</audio>

![Poratada del episodio con Jen0f0nte al frente]({{< ABSresource url="/images/PL/Linux-Connexion-con-Jen0f0nte-300x300.png" >}})

 Muy buenas Linuxero. Bienvenido a un episodio más de Podcast Linux.  
 Mi nombre es Juan Febles y hoy toca una entrevista Linux Connexion, los programas especiales de este podcast para acercarte a las personas o proyectos que han salido en la sección Comunidad Linux.  
 En esta ocasión tenemos a Jenofonte, linuxero de pro, del que ya me oíste mencionar en el episodio #08 Sabores a montones.

 Hablaremos de sus inicios, la filosofía del software libre, la producción de material multimedia en GNU/Linux, sus proyectos de divulgación y qué tiene entre manos en el presente y sus ideas de futuro.  
 Aquí tienes la forma de contactar con él:  
 Twitter: [@jen0f0nte](https://twitter.com/jen0f0nte)  
 Blog: <http://unade25.blogspot.com.es/>  
 Colaboratorio: <https://colaboratorio.net/>  
 Youtube: <https://www.youtube.com/user/jen0f0nte>  
 Email: [unade25@gmail.com](mailto:unade25@gmail.com)

 El feed del programa es <https://podcastlinux.com/feed>

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 LukHash – The Other Side  
 MeHiLove – Lagoon

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
