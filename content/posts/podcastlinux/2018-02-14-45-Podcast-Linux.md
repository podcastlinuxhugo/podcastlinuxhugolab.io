---
title: "#45 Distros Ligeras"
date: 2018-02-14
author: Juan Febles
categories: [podcastlinux]
img: PL/PL45.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL45
  image: https://podcastlinux.gitlab.io/images/PL/PL45.png
tags: [Distros, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL45.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL45.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy estoy puntual a la cita quincenal con un episodio para mostrarte mis experiencias con distros ligeras.

 En el **Núcleo Kernel** repasaré las distros ligeras que he probado en un netbook de hace más de 10 años para comprobar sus posibilidades en dispositivos antiguos.  
 **Puppy Linux:**[ http://puppylinux.org](http://puppylinux.org)  
 **Peppermint OS:**[ https://peppermintos.com/](https://peppermintos.com/)  
 **BunsenLabs:** <https://www.bunsenlabs.org/>  
 **Raspberry Pi Desktop:** <https://www.raspberrypi.org/downloads/raspberry-pi-desktop/>  
 **Bodhi Linux:** <http://www.bodhilinux.com/>  
 **Linux Lite:** <https://www.linuxliteos.com/>  
 **Lubuntu:** <https://lubuntu.net/>  
 **Lxle Linux:** <http://lxle.net/>  
 **Minino:** <https://minino.galpon.org/es>

  En el **Gestor de Paquetes** te hablaré de Etcher, aplicación multiplataforma para grabar las ISOS de tus distros preferidas en tus pendrive o tarjetas SD.  
 En **Comunidad Linux** conoceremos a GALPON, Grupo de Amigos de Linux de Pontevedra, Comunidad que está detrás de Minino, una distro ligera de la que me he enamorado.  
 Por último, en **Área de Notificaciones**, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other SideLukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 FantomStudio – Lucky Corporate  
 FantomStudio – FantonStudio Ambient  
 FantomStudio – Happy Folk  
 FantomStudio – The Corporate Background  
 FantomStudio – Fashion Ambient

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
