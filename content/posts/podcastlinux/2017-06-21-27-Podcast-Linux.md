---
title: "#27 Especial Slimbook One"
date: 2017-06-21
author: Juan Febles
categories: [podcastlinux]
img: PL/PL27.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL27
  image: https://podcastlinux.gitlab.io/images/PL/PL27.png
tags: [Slimbook, Hardware, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL27.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL27.mp3" type="audio/mpeg">
</audio>


 Muy buenas linuxero, bienvenido a otro episodio más de Podcast linux, esta vez un programa especial. Mi nombre es Juan Febles y hoy quiero compartir la experiencia con el Slimbook One, el único dispositivo de sobremesa de esta marca valenciana. Después de un mes tengo suficientes datos para realizar una review en condiciones acerca de este producto.

 Ponte en contacto con Slimbook:

   
 * Slimbook One: <https://slimbook.es/one>
 * Twitter: [@SlimbookEs](https://twitter.com/SlimbookEs)
 * Web: <https://slimbook.es>
 * Canal de Telegram: [t.me/slimbook](https://t.me/slimbook)
 * Correo: [info@slimbook.es](mailto:info@slimbook.es)
 * Hashtag Twitter: [#SlimbookOne](https://twitter.com/hashtag/SlimbookOne)
 
 Fotos:

 ![]({{< ABSresource url="/images/PL//comparativasize-300x169.jpg" >}})

 ![]({{< ABSresource url="/images/PL/testconsumo-300x169.png" >}})

 ![]({{< ABSresource url="/images/PL/SlimbookOnepalmamano-300x169.jpg" >}})


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
