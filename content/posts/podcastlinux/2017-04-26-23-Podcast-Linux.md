---
title: "#23 La Terminal"
date: 2017-04-26
author: Juan Febles
categories: [podcastlinux]
img: PL/PL23.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL23
  image: https://podcastlinux.gitlab.io/images/PL/PL23.png
tags: [Terminal, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL23.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL23.mp3" type="audio/mpeg">
</audio>
  

 Muy buenas Linuxero. Bienvenido a una nueva entrega de Podcast Linux.  
 Mi nombre es Juan Febles y 2 veces al mes me pongo frente al micrófono para compartir el sistema operativo de escritorio que más nos gusta: GNU/Linux.  
 Empezamos este episodio como siempre, con el resumen del programa.

 En el Núcleo Kernel hablaremos de la Terminal, la herramienta más potente de nuestro sistema operativo.

 Algunos trucos interesantes a tener en cuenta.  
 **Ctrl+Atl+T:** atajo de teclado para abrir el Terminal.  
 **Ctrl+Shitf+T**: Abrir una pestaña nueva dentro de la Terminal.  
 **El tabulador** te ayuda a autocumplimentar comandos y nombres de ficheros.  
 **Ctrl L:** Limpia la Terminal al igual que el comando clear  
 **Cursores arriba y abajo**: busca en el histórico de comandos  
 **Ctrl + Insert o Ctrl+Atl+ C** copia texto seleccionado  
 **Shift + Insert o Ctrl+Atl+V** pega texto seleccionado  
 **Ctrl + a:** Ir al inicio de la línea  
 **Ctrl + e:** Ir al final de la línea  
 **Alt + f:** Ir hacia adelante una palabra  
 **Alt + b:** Ir hacia atrás una palabra

 Los principales comandos que deberías conocer al menos, a mi entender, son éstos:

 **ls** muestra un listado del contenido de un directorio  
 **cd** cambia de directorio  
 **mkdir** crea un nuevo directorio  
 **rmdir** elimina un directorio  
 **cp** copia un archivo  
 **mv** mueve un archivo  
 **rm** elimina un archivo  
 **cat** visualiza un archivo de texto  
 **nano** edita un archivo de texto  
 **chmod** cambia el atributo de un archivo  
 **clear** limpia la terminal  
 **dd** copia y clona archivos, particiones o dispositivos  
 **gzip** Crea un archivo comprimido *.gz  
 **crontab** programa tareas a realizar  
 **rsync** sincroniza archivos y carpetas (fundamental para copias de seguridad)  
 **top** monitoriza procesos y recursos en tiempo real  
 **df** informa las particiones, espacio utilizado y disponible.  
 **Fdisk** gestiona las particiones.  
 **Killall** destruye un proceso específico (por ejemplo Killall Audacity)

 <https://computernewage.com/2013/04/04/primeros-pasos-con-la-terminal-de-linux/>  
 <https://openwebinars.net/blog/La-guia-definitiva-para-aprender-a-usar-la-terminal-de-Linux/>

 En el Gestor de Paquetes te hablaré de Klavaro, un programa de mecanografía libre.

 <http://klavaro.sourceforge.net/>  
 <https://lignux.com/klavaro-mecanografia-para-todos/>

 En Comunidad Linux daremos la bienvenida a Davidochobits, un gran divulgador, administrador de sistemas y amante del software libre.

 <https://twitter.com/ochobitsunbyte?lang=es>  
 <https://www.ochobitshacenunbyte.com/>

 Por último, en Área de Notificaciones, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Lightning Traveler – Electronic Ambient  
 Lightning Traveler – Travel Ambient  
 Lightning Traveler – Travel Commercial  
 Lightning Traveler – Corporate Inspiration

 Las imágenes utilizadas en la portada son gratuitas, editables y propiedad de Freepik.es

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
