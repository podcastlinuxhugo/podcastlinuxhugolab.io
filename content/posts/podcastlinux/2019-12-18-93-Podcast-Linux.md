---
title: "#93 Productividad y GNU/Linux 2"
date: 2019-12-18
author: Juan Febles
categories: [podcastlinux]
img: PL/PL93.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL93
  image: https://podcastlinux.gitlab.io/images/PL/PL93.png
tags: [Productividad, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL93.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL93.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy estás en un episodio formal. En poco más de media hora te daré a conocer la segunda parte de cómo puedes sacarle el máximo provecho a tu distribución GNU/Linux.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 93, Productividad en GNU/Linux 2.

En el núcleo Kernel terminaremos con la terminal, scripts y cron para ser más eficiente en GNU/Linux. En Gestor de Paquetes seguiremos con algunos consejos de la terminal, la aplicación más importante de nuestras distribuciones. En Comunidad Linux, continuaremos con Atareao y algunos cursos de interés alojados en su web. Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.  
Recuerda: Falta poco para el episodio 100\. Anímate a compartir qué te gustaría que hiciese en ese episodio. ¿Un episodio recopilatorio, una charla en seguidores?¿Preguntas y respuestas sobre Podcast Linux? ¿Otra cosa? Compártelo en los comentarios del episodio.

**La terminal:** **Atajos de teclado**  
Ctrl + Alt +T: Abrir Terminal  
Flechas arriba/abajo: Historial de comandos  
History: Historial de comandos (día y hora)  
Ctrl + R: Búsqueda en el Historial de comandos  
Alt + . : escribe los últimos comandos a continuación de donde estemos  
Ctrl + L: Limpia terminal (clear)  
Ctrl +Mayús + C: Copiar desde la Terminal  
Ctrl +Mayús + V: Pegar en la terminal  
Ctrl + A: Cursor al Inicio de la línea  
Ctrl + E: Cursor al final de la línea  
Ctrl + U: Borra desde el cursor hasta el inicio de la línea. Todo si está al final.  
Ctrl + K: Borra desde el cursor hasta el final de la línea.  
Ctrl + Y: Deshacer el último borrado que se ha realizado.  
Ctrl + W: Borra palabra antes del cursor  
Ctrl + Mayús + T: Nueva pestaña  
Ctrl + Mayús + 8: Dividir terminal horizontalmente  
Ctrl + Mayús + 9: Dividir terminal verticalmente

**Comandos:**  
cd:  
cd ..:  
cd -: Cambia a la última ruta usada con este comando  
!!: Último comando utilizado (sudo !!)  
!$: Repetir el argumento del último comando utilizado  
&&: Une comandos  
ll: ls -l  
date: Fecha y hora  
cal: ncal -b -3  
uptime: cuánto tiempo lleva encendido el ordenador  
Calcourse: Utilidad de calendario, tareas y organización  
wget:  
curl: curl wttr.in/tenerife curl v2.wttr.in/tenerife  
– y:  
shuf: Número al azar. shuf -i 1-100 -n 1  
at:  
locate: locate *.mkv  
find: find . -size +1024M  
shopt -s cdspell: Te corrige cuando te equivocas al escribir el nombre de un directorio, al intentar cambiar a él con la orden cd. (Documentos sin mayúsculas)

dd: sudo fdisk -l  
sudo dd bs=4M if=/Descargas/sd.img of=/dev/mmcblk1 status=progress conv=fsync

Actualizar sistema:  
sudo pkcon refresh && pkcon update -y  
sudo apt update && sudo apt upgrade -y  
sudo apt autoremove

rsync: Realizar copias de seguridad mediante sincronización.  
rsync opciones origen destino

-v, muestra los resultados de la ejecución.  
-a, mantiene el usuario, grupo, permisos, fecha y hora, así como los enlaces simbólicos. Esta opción es equivalente a -rlptgoD (recursivo, copia los enlaces simbólicos, mantiene los permisos, fecha y hora, grupo, propietario, archivos de dispositivos y especiales)  
-z, comprime la información antes de realizar la transferencia.  
-h, nos da las tasas de transferencia y el tamaño de los archivos en unidades razonables. Si no se especifica esta opción, todo la información nos la dará en bytes y bytes/s. Esta opción nos es tan necesaria como las demás pero siempre es una ayuda.  
–delete. Con esta opción se borrará todo lo que esté en el destino y no esté en el origen.

rsyn -avzh –delete origen destino

Shutdown: Shutdown – h now (Ctrl + Alt + Supr)  
Reboot: reboot

**Alias**  
alias actualiza= sudo apt update && sudo apt upgrade -y  
Para que persista nano .bashrc (sección alias como ll)

**Scripts:**  
#!/bin/bash  
echo Hola mundo

sudo chmod +x archivo.sh (permisos de ejecución)

**Automatizar con Cron**  
crontab -l: Listar las tareas programadas  
crontab -e: Editar las tareas programadas

##########################################################  
#minuto (0-59),  
#| hora (0-23),  
#| | día del mes (1-31),  
#| | | mes (1-12),  
#| | | | día de la semana (0-6 donde 0=Domingo)  
#| | | | | comandos o scripts  
*****  
##########################################################

**Documentar:**  
[HackMD.io](https://gitlab.com/): MarkDown. Compartir, editar, sólo ver, privado.  
Repositorios git [Gitlab](https://gitlab.com/): GitLab Pages: Jekyll, Hugo, Pelican

La **imagen utilizada en la portada, el hombre 3D** se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): [https://pixabay.com/images/id-1019763](https://pixabay.com/images/id-1019763)

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Arhur Yul – Cinema Intro: [https://www.jamendo.com/track/1503848/](https://www.jamendo.com/track/1503848/)  
GarnaVutka – Corporate: [https://www.jamendo.com/track/1354699](https://www.jamendo.com/track/1354699)  
Zoran Sebic – Innovative Logo: [https://www.jamendo.com/track/1487845](https://www.jamendo.com/track/1487845)  
GarnaVutka – Crystal Wellcome Logo: [https://www.jamendo.com/track/1508030](https://www.jamendo.com/track/1508030)  
GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): [https://www.jamendo.com/track/1464611](https://www.jamendo.com/track/1464611)  
MBB – Clouds: [https://www.free-stock-music.com/mbb-clouds.html](https://www.free-stock-music.com/mbb-clouds.html)  
MBB – Brezee: [https://www.free-stock-music.com/mbb-breeze.html](https://www.free-stock-music.com/mbb-breeze.html)  
MBB – Feel good: [https://www.free-stock-music.com/mbb-feel-good.html](https://www.free-stock-music.com/mbb-feel-good.html)  
MBB – Ibiza: [https://www.free-stock-music.com/mbb-ibiza.html](https://www.free-stock-music.com/mbb-ibiza.html)  
MBB – Takeoff: [https://www.free-stock-music.com/mbb-takeoff.html](https://www.free-stock-music.com/mbb-takeoff.html)  
MBB – Good Vibes: [https://www.free-stock-music.com/mbb-good-vibes.html](https://www.free-stock-music.com/mbb-good-vibes.html)

**Bibliografía utilizada para realizar este episodio:**

[https://www.ochobitshacenunbyte.com/2018/12/04/los-42-comandos-mas-importantes-en-gnu-linux/](https://www.ochobitshacenunbyte.com/2018/12/04/los-42-comandos-mas-importantes-en-gnu-linux/)  
[https://ugeek.github.io/blog/post/2019-04-06-atajos-de-terminal.html](https://ugeek.github.io/blog/post/2019-04-06-atajos-de-terminal.html)  
[https://itsfoss.com/linux-command-tricks/](https://itsfoss.com/linux-command-tricks/)  
[https://openwebinars.net/blog/15-atajos-teclado-mas-utilizados-shell-linux/](https://openwebinars.net/blog/15-atajos-teclado-mas-utilizados-shell-linux/)  
[https://blog.desdelinux.net/apagar-y-reiniciar-mediante-comandos](https://blog.desdelinux.net/apagar-y-reiniciar-mediante-comandos)  
[https://blog.desdelinux.net/ejecuta-un-comando-a-la-hora-que-quieras-con-at](https://blog.desdelinux.net/ejecuta-un-comando-a-la-hora-que-quieras-con-at)  
[https://airsynth.es/2015/03/atajos-de-teclado-que-suelo-usar/](https://airsynth.es/2015/03/atajos-de-teclado-que-suelo-usar/)  
[https://www.forosla.com/algunos-trucos-para-el-terminal-gnu-linux/](https://www.forosla.com/algunos-trucos-para-el-terminal-gnu-linux/)  
[https://www.ochobitshacenunbyte.com/2014/11/18/copias-de-seguridad-con-rsync/](https://www.ochobitshacenunbyte.com/2014/11/18/copias-de-seguridad-con-rsync/)  
[https://www.atareao.es/software/utilidades/mejorando-el-terminal-de-ubuntu-con-alias/](https://www.atareao.es/software/utilidades/mejorando-el-terminal-de-ubuntu-con-alias/)  
[https://www.atareao.es/software-linux/sincronizacion-a-fondo-con-rsync/](https://www.atareao.es/software-linux/sincronizacion-a-fondo-con-rsync/)  
[https://www.atareao.es/tutorial/terminal/](https://www.atareao.es/tutorial/terminal/)  
[https://www.atareao.es/tutorial/scripts-en-bash/](https://www.atareao.es/tutorial/scripts-en-bash/)  
[https://ugeek.github.io/blog/post/2019-10-15-cron-ejemplos.html](https://ugeek.github.io/blog/post/2019-10-15-cron-ejemplos.html)  
[https://www.atareao.es/software/utilidades/programacion-de-tareas-con-cron/](https://www.atareao.es/software/utilidades/programacion-de-tareas-con-cron/)  
[https://www.ochobitshacenunbyte.com/2018/03/22/programar-tareas-en-linux-facilmente-con-cron-y-crontab/](https://www.ochobitshacenunbyte.com/2018/03/22/programar-tareas-en-linux-facilmente-con-cron-y-crontab/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1293890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
