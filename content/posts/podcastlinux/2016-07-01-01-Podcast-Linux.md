---
title: "#01 Antecedentes. Podcast Linux"
date: 2016-07-01
author: Juan Febles
categories: [podcastlinux]
img: PL/PL01.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL01
  image: https://podcastlinux.gitlab.io/images/PL/PL01.png
tags: [Antecedentes, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL01.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL01.mp3" type="audio/mpeg">
</audio>

Bienvenido al primer episodio de Podcast Linux, tu podcast de GNU Linux.

 Un podcast para acercar al usuario de a pie el mundo del sistema operativo del pingüino.

 En todos los episodios tendremos estas secciones:

 Gestor de Arranque: Resumen del programa

 Núcleo Kernel: Parte central.  
 Te comentaré el objetivo de este podcast y mi relación con el sistema operativo GNU Linux  
 <http://ddigital.webs.ull.es/la-universidad-de-la-laguna-celebra-el-da-mundial-del-software-libre/>  
 <http://bardinux.ull.es/>  

 Gestor de Paquetes: Analizo una aplicación Libre  
 Libre Office  
 <https://es.libreoffice.org/>  
 <https://es.wikipedia.org/wiki/LibreOffice>

 Comunidad Linux: Sección para dar a conocer linuxeros, comunidades, blogs,….  
 Grupo Gnu Linux en Español de Telegram  
 <https://telegram.me/GnuLinuxGrupo>  
 <https://telegram.org/>

 Área de Notificaciones: Espacio para los mensajes de los oyentes.  
 @podcastlinux  
 podcastlinux@gmail.com

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 Artistas:  
 LukHash  
 Roocow  
 Visager  
 Cleric  
 Matti Paalanen  
 The Polish Ambassador

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1280800_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
