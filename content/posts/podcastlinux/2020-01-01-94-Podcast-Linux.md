---
title: "#94 Linux Connexion con Atareao"
date: 2020-01-01
author: Juan Febles
categories: [podcastlinux]
img: PL/PL94.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL94
  image: https://podcastlinux.gitlab.io/images/PL/PL94.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL94.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL94.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega, la número 94, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy está con nosotros Lorenzo Carbonell, Bloguero, podcaster y desarrollador en GNU/Linux, lo puedes escuchar cada lunes y jueves en su podcast y leer cuando quieras en atareao.es

Recordar a los oyentes que estamos en una sala Jitsi para esta charla, un servicio libre para videoconferencias, y agradecer a [Neodigit](https://neodigit.net/), nuestro proveedor de alojamiento y servicios de confianza de habla hispana para todo [Avpodcast](https://avpodcast.net).  

Y que dentro de poco llegaremos al episodio 100. ¿Un episodio recopilatorio, una charla con seguidores? ¿Preguntas y respuestas sobre Podcast Linux? ¿Otra cosa? A ver qué se nos ocurre para festejar ese episodio tan redondo. Compártelo en los comentarios del episodio.

**Enlaces de interés:**

Blog Atareao: [https://www.atareao.es/](https://www.atareao.es/)  
Podcast Atareao: [https://www.atareao.es/podcasts/](https://www.atareao.es/podcasts/)  
Canal de Telegram: [https://t.me/canal_atareao](https://t.me/canal_atareao)  
Fondos productivos: [https://www.atareao.es/podcast/fondos-de-pantalla-productivos/](https://www.atareao.es/podcast/fondos-de-pantalla-productivos/)  

Las **imagen utilizada en la portada** ha sido cedida por Lorenzo Carbonell y es de su propiedad.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Mixaund – Success: [https://www.free-stock-music.com/mixaund-success.html](https://www.free-stock-music.com/mixaund-success.html)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1294890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
