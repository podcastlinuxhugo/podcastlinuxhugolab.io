---
title: "#19 GNU/Linux en la escuela"
date: 2017-03-01
author: Juan Febles
categories: [podcastlinux]
img: PL/PL19.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL19
  image: https://podcastlinux.gitlab.io/images/PL/PL19.png
tags: [Educación, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL19.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL19.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxer@. Bienvenido a una nueva entrega de Podcast Linux.  
 Mi nombre es Juan Febles y cada 2 semanas tengo una cita contigo para hablar de sistema operativo de escritorio que más nos apasiona: GNU/Linux. Iniciamos este episodio con el resumen del programa.

 En el Núcleo Kernel tomaremos el pulso a GNU/Linux dentro de las escuelas.

 <https://lignux.com/distribuciones-gnu-linux-enfocadas-en-la-educacion>  
 [https://es.wikipedia.org/wiki/Anexo:Distribuciones\_GNU/Linux\_de\_Espa%C3%B1a](https://es.wikipedia.org/wiki/Anexo:Distribuciones_GNU/Linux_de_Espa%C3%B1a)  
 <http://www.maderapaloma.com/html/info-gnu-linux.html>

 En el Gestor de Paquetes hablaré de Audacity, el software con el que hago este programa sonoro.  
 <http://www.audacityteam.org/>  
 [http://wiki.audacityteam.org/wiki/Release\_Notes\_2.1.2](http://wiki.audacityteam.org/wiki/Release_Notes_2.1.2)

 En Comunidad Linux comentaré el proyecto Lliurex, uno de los primeros en promover el software libre en los centros educativos de España, más específicamente en la Generalitat Valeciana.

 <http://lliurex.net>  
 <https://es.wikipedia.org/wiki/LliureX>

 Por último, en Área de Notificaciones, le daré un repaso a los mensajes recibidos en los últimos episodios.


 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 AntarticBreeze – Sun is up  
 AntarticBreeze – Sumer time  
 AntarticBreeze – Paradise  
 AntarticBreeze – Tecnological World

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
