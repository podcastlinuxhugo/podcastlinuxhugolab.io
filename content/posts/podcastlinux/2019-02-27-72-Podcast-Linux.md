---
title: "#72 Especial Vant MiniMOOVE"
date: 2019-02-27
author: Juan Febles
categories: [podcastlinux]
img: PL/PL72.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL72
  image: https://podcastlinux.gitlab.io/images/PL/PL72.png
tags: [Hardware, Vant, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL72.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL72.mp3" type="audio/mpeg">
</audio>

 Muy buenas amante de GNU/Linux y el Software Libre!!!  
 Bienvenido a un nuevo episodio, el número 72, de Podcast Linux. Un saludo muy fuerte y cariñoso de quien te habla, Juan Febles.

 Hoy vamos a conocer al chiquitín de la casa [Vant](https://www.vantpc.es/), el [MiniMOOVE](https://www.vantpc.es/minimoove).

 **Sorteo Pack Vant:** <https://twitter.com/podcastlinux/status/1100641075990069249>

 **Enlaces de interés:**  
 Vant MiniMOOVE:[ https://www.vantpc.es/minimoove](https://www.vantpc.es/minimoove)  
 Unboxing Vant MiniMOOVE: <https://youtu.be/9-K9kaZyd2g>**  
 **

 La **imagen utilizada **en la portada es el es propiedad de la marca Vant y es cedida por ella.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 GarnaVutka – Chill: <http://jamen.do/t/1615704>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
