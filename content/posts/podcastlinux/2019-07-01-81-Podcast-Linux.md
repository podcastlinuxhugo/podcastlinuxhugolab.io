---
title: "#81 Especial 3º Aniversario"
date: 2019-07-01
author: Juan Febles
categories: [podcastlinux]
img: PL/PL81.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL81
  image: https://podcastlinux.gitlab.io/images/PL/PL81.png
tags: [Especial, Aniversario, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL81.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL81.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy estamos de cumpleaños.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 81, Especial 3º Aniversario.  

**Sorteos:**  
Para participar,haz RT al tuit del episodio y sigue a Neodigit, Slimbook, Vant, Wacom España y Podcast Linux: [https://twitter.com/podcastlinux/status/1145558625546657792](https://twitter.com/podcastlinux/status/1145558625546657792)  
4 sorteos independientes en los que debes seguir a la marca, a Podcast Linux y hacer RT en el tuit.  
Neodigit: Multidominio 10G por un año.  
Vant: Disco SSD + Teclado y ratón Tux.  
Slimbook:Camiseta+ pegatinas + Webcamcover  
Wacom España:Tableta One by Wacom  

La imagen del hombre 3d utilizada en la portada, se ha liberado desde **[Pixabay](https://pixabay.com/)** con licencia **[Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):**[https://pixabay.com/images/id-1907154/](https://pixabay.com/images/id-1907154/)  
Los logos de la empresas colaboradoras son propiedad de cada una de ellas.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
GarnaVutka – Upbeat Tropical Summer Pop (Full version) (2019) : [http://jamen.do/t/1654988](http://jamen.do/t/1654988)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1281810_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
