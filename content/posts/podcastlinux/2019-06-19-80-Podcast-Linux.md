---
title: "#80 Especial Vant Life A"
date: 2019-06-19
author: Juan Febles
categories: [podcastlinux]
img: PL/PL80.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL80
  image: https://podcastlinux.gitlab.io/images/PL/PL80.png
tags: [Hardware, Vant, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL80.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL80.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y tenemos el primer análisis de un ordenador AMD. Hoy quiero que conozcas un sobremesa de la marca.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 80, Especial Vant Life A.

Sorteo de teclado + ratón y pegatinas. Haz RT al tuit del episodio y sigue a Vant y Podcast Linux: [https://twitter.com/podcastlinux/status/1141218091763998720](https://twitter.com/podcastlinux/status/1141218091763998720)

Antes de empezar me gustaría recordarte que Podcast Linux forma parte de la red [AvPodcast](https://avpodcast.net), y todo Avpodcast esta alojado en [Neodigit.net](https://neodigit.net/) , nuestro proveedor de confianza de habla hispana. Son profesionales serios y trabajan con muchas soluciones de Software Libre. Gracias Neodigit.net por hacer que éste y muchísimos podcasts salgan a la luz.

Y también anunciarte que queda poco para el **3º aniversario** y quiero **pedirte un audio donde comentes lo que quieras**: felicitaciones, propuestas, lo que más te gusta, lo que crees que hay que mejorar…  
Manda tu audio a Telegram: [@juanfebles](https://t.me/juanfebles) o por correo [podcastlinux@avpodcast.net](mailto:podcastlinux@avpodcast.net)

**Enlaces de interés:**  
Vant Life A: [https://www.vantpc.es/life-a](https://www.vantpc.es/life-a)Placa madre: [https://www.asrock.com/MB/AMD/Fatal1ty%20AB350%20Gaming-ITXac/index.asp](https://www.asrock.com/MB/AMD/Fatal1ty%20AB350%20Gaming-ITXac/index.asp)  
Fuente de alimentación: [http://coolbox.es/cajas-pc-y-componentes/3443-fuente-de-alimentacion-coolbox-s300-80-plus-bronze.html](http://coolbox.es/cajas-pc-y-componentes/3443-fuente-de-alimentacion-coolbox-s300-80-plus-bronze.html)  
Comparativa CPU: [https://cpu.userbenchmark.com/](https://cpu.userbenchmark.com/)  
Comparativa GPU: [https://gpu.userbenchmark.com/](https://gpu.userbenchmark.com/)

**Vídeos:**  
Unboxing: [https://youtu.be/xZVpPxNUNfo](https://youtu.be/xZVpPxNUNfo)  
Configuración: [https://youtu.be/bxo2ggqx5m0](https://youtu.be/bxo2ggqx5m0)

La imagen del hombre 3d utilizada en la portada, se ha liberado desde **[Pixabay](https://pixabay.com/)** con licencia **[Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):**[https://pixabay.com/images/id-1013695/](https://pixabay.com/images/id-1013695/)  
La imagen del Life A de Vant es propiedad de su propia empresa.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
GarnaVutka – Upbeat Corporate Uplifting Motivacional Full Version (2019) : [http://jamen.do/t/1658148](http://jamen.do/t/1658148)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1280800_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
