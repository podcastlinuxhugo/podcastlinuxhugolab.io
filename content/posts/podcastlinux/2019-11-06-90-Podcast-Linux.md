---
title: "#90 Arduino"
date: 2019-11-06
author: Juan Febles
categories: [podcastlinux]
img: PL/PL90.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL90
  image: https://podcastlinux.gitlab.io/images/PL/PL90.png
tags: [Arduino, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL90.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL90.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy tenemos episodio sobre una placa de Hardware Libre. En poco más de media hora te comentaré qué es Arduino y todas las posibilidades que tiene.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 90, Arduino.

En el núcleo Kernel conocerás [Arduino](https://www.arduino.cc/), la placa con microcontrolador que ha revolucionado la educación, robótica, domótica y el mundo maker. En Gestor de Paquetes conoceremos [Fritzing](https://fritzing.org/), un programa libre multiplataforma para diseñar gráficamente tus proyectos electrónicos. En Comunidad Linux, tendremos a [Luis del Valle](https://programarfacil.com/luis-del-valle/), bloguero y podcaster detrás del proyecto Programar Fácil. Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.

Las **imágenes utilizadas en la portada, el hombre 3D** se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): [https://pixabay.com/images/id-2339835/](https://pixabay.com/images/id-2339835/)  
El **logotipo Arduino** es propiedad de esta empresa.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Arhur Yul – Cinema Intro: [https://www.jamendo.com/track/1503848/](https://www.jamendo.com/track/1503848/)  
GarnaVutka – Corporate: [https://www.jamendo.com/track/1354699](https://www.jamendo.com/track/1354699)  
Zoran Sebic – Innovative Logo: [https://www.jamendo.com/track/1487845](https://www.jamendo.com/track/1487845)  
GarnaVutka – Crystal Wellcome Logo: [https://www.jamendo.com/track/1508030](https://www.jamendo.com/track/1508030)  
GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): [https://www.jamendo.com/track/1464611](https://www.jamendo.com/track/1464611)  
Liqwyd – Coffee: [https://www.free-stock-music.com/liqwyd-coffee.html](https://www.free-stock-music.com/liqwyd-coffee.html)  
MBB – Fantastic: [https://www.free-stock-music.com/mbb-fantastic.html](https://www.free-stock-music.com/mbb-fantastic.html)  
MBB – Waves: [https://www.free-stock-music.com/mbb-waves.html](https://www.free-stock-music.com/mbb-waves.html)  
Relic Music – DEADM Cruising: [https://www.free-stock-music.com/relic-music-deadm-cruising.html](https://www.free-stock-music.com/relic-music-deadm-cruising.html)  
Free Music – I Wish You A Happy Way: [https://www.free-stock-music.com/fm-freemusic-i-wish-you-a-happy-way.html](https://www.free-stock-music.com/fm-freemusic-i-wish-you-a-happy-way.html)

**Bibliografía utilizada para realizar este episodio:**  
[https://www.arduino.cc](https://www.arduino.cc)  
[https://forum.arduino.cc/index.php?board=32.0](https://forum.arduino.cc/index.php?board=32.0)  
[https://es.wikipedia.org/wiki/Arduino](https://es.wikipedia.org/wiki/Arduino)  
[https://aprendiendoarduino.wordpress.com/2015/03/29/microcontrolador-vs-microprocesador](https://aprendiendoarduino.wordpress.com/2015/03/29/microcontrolador-vs-microprocesador)  
[https://core-electronics.com.au/tutorials/compare-arduino-boards.html](https://core-electronics.com.au/tutorials/compare-arduino-boards.html)  
[http://diwo.bq.com/partes-del-codigo-arduino](http://diwo.bq.com/partes-del-codigo-arduino)  
[http://rduinostar.com/documentacion/general/protoboard-que-es-y-como-funciona/](http://rduinostar.com/documentacion/general/protoboard-que-es-y-como-funciona/)  
[https://fritzing.org/](https://fritzing.org/)  
[https://programarfacil.com/](https://programarfacil.com/)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1290890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
