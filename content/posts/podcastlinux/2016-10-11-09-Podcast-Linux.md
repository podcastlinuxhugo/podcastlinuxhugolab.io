---
title: "#09 Especial Lenovo ThinkPad X220"
date: 2016-10-11
author: Juan Febles
categories: [podcastlinux]
img: PL/PL09.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL09
  image: https://podcastlinux.gitlab.io/images/PL/PL09.png
tags: [Thinkpad, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL09.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL09.mp3" type="audio/mpeg">
</audio>


 Muy buenas linuxeros!!!  
 Un saludo de quien les habla, Juan Febles.  
 Hoy tocaría tener una entrevista con algún proyecto o linuxero, pero me ha sido imposible conseguir alguien a tiempo. Pido disculpas por ello.  
 No quería fallar en mi ritmo de publicaciones. Los más de 700 oyentes no se lo merecen. Agradecer a cada uno de ustedes la escucha.

 Por eso tienes aquí este especial. Voy hablar de mi nueva, entre comillas, adquisición y desde dónde se inicia la decisión del cambio de computadora.

 <http://www.reciclanet.org/>

 <http://abierta.org/>

 <https://www.info-computer.com/>

 <http://www.ordenadoresocasioncanarias.com/>


 Hasta dentro de 15 días Linuxeros!!!!  
 Un abrazo muy fuerte.

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1285850_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
