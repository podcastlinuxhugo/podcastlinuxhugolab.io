---
title: "#83 Especial #TLP2019"
date: 2019-07-29
author: Juan Febles
categories: [podcastlinux]
img: PL/PL83.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL83
  image: https://podcastlinux.gitlab.io/images/PL/PL83.png
tags: [Especial, TLP, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL83.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL83.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y vuelvo por tercer año consecutivo a la [TLP-Tenerife](https://tlp-tenerife.com/), el evento tecnológico más importante de Canarias. Intentaré transmitir qué se cuece aquí y divulgar el Software Libre y GNU/Linux en algunas charlas y talleres.

**Enlaces de interés:  
**[https://tlp-tenerife.com/](https://tlp-tenerife.com/)  
[https://tlp.ull.es/](https://tlp.ull.es/)  
[https://osl.ull.es/](https://osl.ull.es/)  
[https://gamika.es/](https://gamika.es/)  
[http://canariarcades.com/](http://canariarcades.com/)  
[https://www.ivoox.com/podcast-podcast-interynet_sq_f133744_1.html](https://www.ivoox.com/podcast-podcast-interynet_sq_f133744_1.html)  
[https://github.com/dvarrui/](https://github.com/dvarrui/)  
[https://www.emezeta.com/](https://www.emezeta.com/)

**Fotos:**  
![]({{< ABSresource url="/images/PL/D_wyEgjWkAM24JC-300x169.jpeg" >}}) 
![]({{< ABSresource url="/images/PL/D_0uZXQW4AAuENS-300x225.jpeg" >}})  
![]({{< ABSresource url="/images/PL/D_1xxFoW4AARtkF-300x225.jpeg" >}})
![]({{< ABSresource url="/images/PL/IMG_20190715_110236-300x169.jpg" >}}) 
![]({{< ABSresource url="/images/PL/IMG_20190715_112822-300x169.jpg" >}})
![]({{< ABSresource url="/images/PL/IMG_20190717_104001-300x169.jpg" >}})
![]({{< ABSresource url="/images/PL/IMG_20190717_111042-300x169.jpg" >}})  
![]({{< ABSresource url="/images/PL/IMG_20190719_101146-300x169.jpg" >}})
![]({{< ABSresource url="/images/PL/IMG_20190719_184728-300x169.jpg" >}})  
![]({{< ABSresource url="/images/PL/IMG_20190719_184936-300x169.jpg" >}})  

Las **imágenes utilizadas en la portada,** el logo TLP-Tenerife es propiedad de [Innova7](http://innova7.org/) y el hombre en 3D se ha liberado desde Pixabay con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): [https://pixabay.com/photo-1027305](https://pixabay.com/photo-1027305)

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1283830_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
