---
title: "#107 Especial 4º Aniversario"
date: 2020-07-01
author: Juan Febles
category: [podcastlinux]
img: PL/PL107.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL107
  image: https://podcastlinux.gitlab.io/images/PL/PL107.png
tags: [Especial, Aniversario, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL107.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL107.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otro episodio de Podcast Linux, el número 107. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy estamos de aniversario, Podcast Linux cumple 4 años.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 107, Especial 4º Aniversario.  
Hace poco este proyecto ha tomado un nuevo rumbo y voy a compartir los nuevos cambios y futuros episodios para la 5ª temporada.  

**Enlaces de interés:**  
Gitlab:<https://gitlab.com/podcastlinux/podcastlinux.gitlab.io>  
Archive.org:<https://archive.org/details/@podcast_linux>  
Podcli:<https://www.atareao.es/podcast/descargar-y-escuchar-podcast-en-ubuntu/>  
HashOver:<https://www.barkdull.org/software/hashover>  
Hackmd.io: <https://hackmd.io/@podcastlinux>  
Pixabay:<https://pixabay.com/>  
Free Stock Music:<https://www.free-stock-music.com/>  
Freesound:<https://freesound.org/>  
Shotcut:<https://www.shotcut.org/>  
LMMS:<https://lmms.io/>  

**La imagen utilizada en portada**, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):<https://pixabay.com/images/id-1907155/>  

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

Podcast Linux - Wake Up: <https://archive.org/details/PodcastLinux-wakeup>  
Podcast Linux - Afternoon: <https://archive.org/details/podcastlinux-afternoon>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed.xml>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f121071070_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
