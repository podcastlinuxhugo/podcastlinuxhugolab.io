---
title: "#03 Y no estaba muerto, no, no"
date: 2016-07-29
author: Juan Febles
categories: [podcastlinux]
img: PL/PL03.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL03
  image: https://podcastlinux.gitlab.io/images/PL/PL03.png
tags: [Reciclar, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL03.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL03.mp3" type="audio/mpeg">
</audio>

Bienvenidos de nuevo a un episodio de Podcast Linux, tu podcast de GNU/Linux.

 Un podcast quincenal producido por Juan Febles para acercar GNU/Linux al usuario de ordenadores de a pie.

 Gestor de Arranque: Resumen del episodio

 Núcleo Kernel: Revive tu viejo pc

 
[Resucita tu viejo PC con GNU/Linux](http://www.muycomputer.com/2016/03/18/distros-linux-ligeras)  
> [Dale una segunda vida a tu viejo ordenador con estas distribuciones Linux](http://www.adslzone.net/2016/01/30/dale-una-segunda-vida-a-tu-viejo-ordenador-con-estas-distribuciones-linux/)  
 <http://lubuntu.net/>  
 <http://puppylinux.org>  
<http://www.bodhilinux.com/>  


Gestor de Paquetes: Clonezilla: <http://clonezilla.org/>  

 Comunidad Linux: [Reciclanet](http://www.reciclanet.org/)  

 Área de Notificaciones: Espacio para los mensajes de los oyentes.  
 <http://avpodcast.net/>  
 <http://salmorejogeek.com/>

 

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 Artistas:  
 LukHash  
 Cleric  
 Matti Paalanen  
 The Polish Ambassador  
 Avallon  
 Arc North  
 O M II N  
 YAVERCLAP

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1285850_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
