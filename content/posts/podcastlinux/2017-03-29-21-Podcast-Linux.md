---
title: "#21 GNU/Linux en la Universidad"
date: 2017-03-29
author: Juan Febles
categories: [podcastlinux]
img: PL/PL21.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL21
  image: https://podcastlinux.gitlab.io/images/PL/PL21.png
tags: [Educación, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL21.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL21.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero. Bienvenido a una nueva entrega de Podcast Linux.  
 Mi nombre es Juan Febles y quincenalmente me cuelo en tu reproductor favorito para hablar del sistema operativo de escritorio que nos une: GNU/Linux.

 En el Núcleo Kernel seguiremos la serie de episodios relacionados con la educación hablando de GNU/Linux en la Universidad.  
 <http://www.portalprogramas.com/software-libre/ranking-universidades/>

 En el Gestor de Paquetes te hablaré de Scratch, un lenguaje visual de programación para facilitarte la entrada a este fantástico mundo.  
 <https://scratch.mit.edu/>  
 [https://es.wikipedia.org/wiki/Scratch\_(lenguaje\_de\_programaci%C3%B3n)](https://es.wikipedia.org/wiki/Scratch_(lenguaje_de_programaci%C3%B3n))

 En Comunidad Linux quiero compartir contigo el proyecto con el que conocí GNU/Linux; la Oficina de Software Libre de la Universidad de La Laguna.  
 <https://osl.ull.es/>

 Por último, en Área de Notificaciones, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 **Podcasts comentados:**

 Compliando Podcast: [https://www.ivoox.com/podcast-compilando-podcast\_sq\_f1388684\_1.html](https://www.ivoox.com/podcast-compilando-podcast_sq_f1388684_1.html)

 Café con Podcast:[ https://www.ivoox.com/cafe-podcast\_sq\_f1358805\_1.html](https://www.ivoox.com/cafe-podcast_sq_f1358805_1.html)

 Ugeek Podcast: [https://www.ivoox.com/ugeek\_sq\_f1383493\_1.html](https://www.ivoox.com/ugeek_sq_f1383493_1.html)

 Polux Crivillé: [https://www.ivoox.com/perfil-apoluxcriville\_aj\_6523379\_1.html](https://www.ivoox.com/perfil-apoluxcriville_aj_6523379_1.html)


 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 The Moose – Far away (Instrumental)  
 Alex Che – Corporate Inspiring  
 Alex Che – Corporate Memories  
 Alex Che – Uplifting Corporate

 Las imágenes utilizadas en la portada son gratuitas, editables y propiedad de Freepik.es

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
