---
title: "#75 Especial PC Reciclado"
date: 2019-04-10
author: Juan Febles
categories: [podcastlinux]
img: PL/PL75.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL75
  image: https://podcastlinux.gitlab.io/images/PL/PL75.png
tags: [Reciclaje, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL75.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL75.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y tenemos un especial entre manos. En poco menos de media hora te comentaré cómo tener un pc sin gastarnos mucho dinero y además ser ecológicos y respetar el medio ambiente.Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 75, vamos a hacernos con un PC Reciclado.

 **Enlaces de interés:  
 **Vídeo Voro: <https://youtu.be/HgmbU7XKkzs>  
 Disco Datos Yoyo: <https://salmorejogeek.com/2016/10/21/particion-de-datos-en-mediadatos-para-que-es-y-por-que-la-hago-en-linux%ef%bb%bf/>  
 FlashRom.org: <https://flashrom.org>  
 Grupo Telegram Pásate a GNU/Linux: <https://t.me/pasategnulinux>  
 Proyecto #PCReciclado: <https://twitter.com/hashtag/PCReciclado>  
 Episodio 3 Y no estaba muerto, no, no: <https://podcastlinux.com/03-Podcast-Linux>  
 Episodio 32 Linux Connexion con Reciclanet: <https://podcastlinux.com/32-Podcast-Linux>  
 Episodio 45 Distros Ligeras: <https://podcastlinux.com/45-Podcast-Linux>  

 **Las imágenes utilizadas en la portada, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es):**<https://pixabay.com/images/id-2142831/> y<https://pixabay.com/images/id-161501/>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 GarnaVutka – Corporate Inspirational (2019): <http://jamen.do/t/1623557>

Recuerda que puedes **contactar** conmigo de la siguiente manera:

 
 * Twitter: [@podcastlinux](https://twitter.com/podcastlinux)
 * Mastodon: [@podcastlinux](https://mastodon.social/@podcastlinux)
 * Correo: [podcastlinux@avpodcast.net](mailto:podcastlinux@avpodcast.net)
 * Web: [avpodcast.net/podcastlinux](https://avpodcast.net/podcastlinux/)
 * Blog: <https://podcastlinux.com>
 
 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
