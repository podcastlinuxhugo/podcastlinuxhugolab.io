---
title: "#97 Vídeo y GNU/Linux"
date: 2020-02-12
author: Juan Febles
categories: [podcastlinux]
img: PL/PL97.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL97
  image: https://podcastlinux.gitlab.io/images/PL/PL97.png
tags: [Vídeo, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL97.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL97.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy toca episodio formal. En poco más de media hora le echaremos un vistazo a la edición de vídeo en GNU/Linux.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 97, Vídeo y GNU/Linux.  

En el núcleo Kernel te daré a conocer las posibilidades que tenemos en el software libre en referencia a la edición de vídeo. En Gestor de Paquetes conocerás a [Shotcut](https://www.shotcut.org/), un editor de vídeo libre multiplataforma para iniciarte en este mundo. En Comunidad Linux, nos visitará de nuevo [Álvaro Nova](https://alvaronova.com/index.es.html), Linuxero con el que veremos qué nos ofrece el Software Libre con respecto a la edición de vídeo frente a otras propuestas privativas. Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.  

Falta poco para el episodio 100. Y ya he decidido qué temática va a tener. Será un preguntas y respuestas sobre Podcast Linux. Todo lo que quieras saber sobre este proyecto o de mí puedes enviarlo en forma de pregunta, ya sea por medio de audio o texto. Responderé a todas ellas en el episodio 100. Envía tus audios o textos a <podcastlinux@avpodcast.net> o a telegram [@juanfebles](https://t.me/juanfebles).  

Programas:  

Avidemux: <http://fixounet.free.fr/avidemux/>  
Handbrake: <https://handbrake.fr/>  
Simple Screen Recorder: <https://www.maartenbaert.be/simplescreenrecorder/>  
OBS Studio: <https://obsproject.com/es/>  
Audacity: <https://www.audacityteam.org/>  
VLC: <https://www.videolan.org/vlc/>  
OpenShot: <https://www.openshot.org/>  
Piviti: <http://www.pitivi.org/>  
Cinelerra: <https://cinelerra.org/>  
KDENlive: <https://kdenlive.org/es/>  
Olive: <https://www.olivevideoeditor.org/>  
Shotcut: <https://www.shotcut.org/>  
Ffmpeg: <https://ffmpeg.org/>  

La imagen utilizada en la portada, el hombre 3D se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/images/id-1874741>  

Toda la música utilizada en este episodio se distribuye bajo la licencia Creative Commons:  

MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
Arhur Yul – Cinema Intro: <https://www.jamendo.com/track/1503848/>  
GarnaVutka – Corporate: <https://www.jamendo.com/track/1354699>  
Zoran Sebic – Innovative Logo: <https://www.jamendo.com/track/1487845>  
GarnaVutka – Crystal Wellcome Logo: <https://www.jamendo.com/track/1508030>  
GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): <https://www.jamendo.com/track/1464611>  
Alex Menco – Inspiring Beat: <https://www.free-stock-music.com/alex-menco-inspiring-beat.html>  
Alexander Nakarada – Corona: <https://www.free-stock-music.com/alexander-nakarada-corona.html>  
Alexander Nakarada – Reborn : <https://www.free-stock-music.com/alexander-nakarada-reborn.html>  
Roda Music – Coastline: <https://www.free-stock-music.com/roa-music-coastline.html>  
Roda Music – Feel Better: <https://www.free-stock-music.com/roa-music-feel-better.html>  



Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
