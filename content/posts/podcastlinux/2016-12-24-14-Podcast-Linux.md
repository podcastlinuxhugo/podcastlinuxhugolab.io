---
title: "#14 Especial Slimbook Katana"
date: 2016-12-24
author: Juan Febles
categories: [podcastlinux]
img: PL/PL14.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL14
  image: https://podcastlinux.gitlab.io/images/PL/PL14.png
tags: [Hardware, Slimbook, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL14.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL14.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxeros. Bienvenidos a un episodio especial de Podcast Linux.  
 Mi nombre es Juan Febles y hoy quiero compartir contigo mis impresiones sobre el Slimbook Katana después de 2 semanas de uso.

 **Slimbook**  
 Twitter: <https://twitter.com/slimbookes>  
 Correo:[info@slimbook.es](mailto:info@slimbook.es)  
 Web: [slimbook.es](https://slimbook.es/)  
 Venta de Slimbook Katana: <https://slimbook.es/pedidos/slimbook-katana>  
 Vídeo: [V Jornadas y Talleres de la UNED de Vila-Real ](https://www.intecca.uned.es/portalavip/grabacion.php?ID_Sala=3&ID_Grabacion=228453&hashData=1390dddfa83dde492235851886d010d8&amp%3BparamsToCheck=SURfR3JhYmFjaW9uLElEX1NhbGEs)(Gracias a [Baltolkien](https://twitter.com/baltolkien))

 ![Slimbook Katana con la Distro Ubuntu Unity]({{< ABSresource url="/images/PL/IMG_20161214_091215-300x169.jpg" >}})

 ![Captura de pantalla del Katana con la Distro Antergos Gnome]({{< ABSresource url="/images/PL/photo_2016-12-24_12-39-46-300x169.jpg" >}})

 ![Primer plano del teclado del katana]({{< ABSresource url="/images/PL/IMG_20161212_124258-300x169.jpg" >}})

 ![Portátil Katana con micrófono para grabar el podcast.]({{< ABSresource url="/images/PL/photo_2016-12-24_13-22-34-300x169.jpg" >}})


 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 LukHash – The Other Side  
 The Polish Ambassador – Erotic Robotics  
 Mehilove – Adove the Clouds  
 Mehilove – All night long

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
