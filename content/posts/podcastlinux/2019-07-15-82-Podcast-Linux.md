---
title: "#82 Hardware Slimbook Zero"
date: 2019-07-15
author: Juan Febles
categories: [podcastlinux]
img: PL/PL82.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL82
  image: https://podcastlinux.gitlab.io/images/PL/PL82.png
tags: [Hardware, Slimbook, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL82.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL82.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y tenemos el análisis del Slimbook Zero. Hoy te presento al chiquitín de la marca Slimbook.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 82, Hardware Slimbook Zero.

Antes de empezar me gustaría recordarte que Podcast Linux forma parte de la red [AvPodcast](https://avpodcast.net/), y todo Avpodcast esta alojado en [Neodigit.net](https://neodigit.net/) , nuestro proveedor de confianza de habla hispana.

Agradecer a [Slimbook](https://slimbook.es/) la cesión de este dispositivo, el primer ordenador de bajo coste de esta marca valenciana.

**Enlaces de interés:**  
Slimbook Zero: [https://slimbook.es/zero](https://slimbook.es/zero)  
Vídeo unboxing: [https://youtu.be/OpNXV0GQ_AE](https://youtu.be/OpNXV0GQ_AE)  
Vídeo encendido y Slimbook MakeUp: [https://youtu.be/TzMF4aWrges](https://youtu.be/TzMF4aWrges)

La imagen del Slimbook Zero utilizada en la portada, se ha liberado desde[ **Slimbook** ](https://slimbook.es/)con licencia **[Creative Commons Atribución – Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/):** [https://slimbook.es/zero#galeria](https://slimbook.es/zero#galeria)

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
GarnaVutka – Upbeat and Inspiring Corporate (Full version) (2018) : [http://jamen.do/t/1537836](http://jamen.do/t/1537836)  


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1282820_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
