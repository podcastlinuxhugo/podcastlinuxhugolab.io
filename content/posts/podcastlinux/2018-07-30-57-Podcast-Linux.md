---
title: "#57 Especial #TLP2018"
date: 2018-07-30
author: Juan Febles
categories: [podcastlinux]
img: PL/PL57.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL57
  image: https://podcastlinux.gitlab.io/images/PL/PL57.png
tags: [Especial, TLP, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL57.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL57.mp3" type="audio/mpeg">
</audio>

 Hoy tenemos un programa muy especial, vuelvo a estar en la Tenerife LanParty, el evento de Tecnología y nuevas tendencias más importante de Canarias. A través de entrevistas y reflexiones intento hacerte llegar todo lo vivido en estos 6 días y divulgar el Software Libre en este lugar tan propicio.

 ![]({{< ABSresource url="/images/PL/DiVXR82WkAAck88.jpglarge-300x169.jpeg" >}}) ![]({{< ABSresource url="/images/PL/DinyekWXcAAp77f.jpglarge-300x178.jpeg" >}}) ![]({{< ABSresource url="/images/PL/Diirx5hU0AAMUYW.jpglarge-300x169.jpeg" >}})

 ![]({{< ABSresource url="/images/PL/DiaLYBLXUAASMHs.jpglarge-300x169.jpeg" >}}) ![]({{< ABSresource url="/images/PL/DitG3l5UEAAL4n8.jpglarge-300x169.jpeg" >}})

 **Enlaces de interés:**  
 <https://tlp-tenerife.com/>  
 <https://tlp.ull.es/>  
 <https://osl.ull.es/>  
 <https://canariarcades.webnode.es/>  
 <https://www.emezeta.com/>  
 <https://www.facebook.com/DigitalCodesign/>  
 <https://gamika.es/>

 Las **imágenes utilizadas en la portada, **el logo TLP es propiedad de [Innova7](http://innova7.org/) y el hombre en 3D se ha liberado desde Pixabay con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/photo-1027428>

 Toda la **música y efectos utilizados en este episodio** se distribuyen bajo la licencia libre **Creative Commons**:

 [LukHash – The Other Side](https://www.jamendo.com/track/1248105/the-other-side)  
 [Rhodesmas – Level Up 02](https://freesound.org/s/320654/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
