---
title: "#86 Redes Sociales Libres"
date: 2019-09-11
author: Juan Febles
categories: [podcastlinux]
img: PL/PL86.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL86
  image: https://podcastlinux.gitlab.io/images/PL/PL86.png
tags: [Redes Sociales, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL86.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL86.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy volvemos con un episodio formal. En poco más de media hora te comentaré las principales redes sociales libres que puedes utilizar.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 86, Redes Sociales Libres.

En el núcleo Kernel le daremos un repaso a las Redes Sociales libres, su filosofía y principales ejemplos. En Gestor de Paquetes analizaremos [Fedilab](https://f-droid.org/es/packages/fr.gouv.etalab.mastodon/), un cliente android para varias Redes Sociales Libres. Conoceremos a [Adrián Perales](https://adrianperales.com/) en Comunidad Linux, bloguero y podcaster defensor de las Redes Sociales Libres. Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.

**Redes Sociales Libres:**  
IRC: [https://freenode.net/](https://freenode.net/)  
XMPP: [https://xmpp.org/](https://xmpp.org/)  
GNU Social: [https://gnu.io/social/](https://gnu.io/social/)  
Diaspora: [https://diasporafoundation.org/](https://diasporafoundation.org/)  
Mastodon: [https://mastodon.social/](https://mastodon.social/)  
Pump.io: [http://pump.io/](http://pump.io/)  
Matrix: [https://about.riot.im/](https://about.riot.im/)  
Signal: [https://www.signal.org](https://www.signal.org)

Las **imágenes utilizadas en la portada, el hombre 3D** se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): [https://pixabay.com/images/id-4373612/](https://pixabay.com/images/id-4373612/)  
Los **logotipos de cada red social o aplicación de mensajería** son propiedad de cada una de ellas.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Arhur Yul – Cinema Intro: [https://www.jamendo.com/track/1503848/](https://www.jamendo.com/track/1503848/)  
GarnaVutka – Corporate: [https://www.jamendo.com/track/1354699](https://www.jamendo.com/track/1354699)  
Zoran Sebic – Innovative Logo: [https://www.jamendo.com/track/1487845](https://www.jamendo.com/track/1487845)  
GarnaVutka – Crystal Wellcome Logo: [https://www.jamendo.com/track/1508030](https://www.jamendo.com/track/1508030)  
GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): [https://www.jamendo.com/track/1464611](https://www.jamendo.com/track/1464611)  
Free Music – Positive Corp: [https://www.free-stock-music.com/fm-freemusic-positive-corp.html](https://www.free-stock-music.com/fm-freemusic-positive-corp.html)  
Free Music – Ambient Presentation: [https://www.free-stock-music.com/fm-freemusic-ambient-presentation.html](https://www.free-stock-music.com/fm-freemusic-ambient-presentation.html)  
Free Music – Inspiring Optimistic Upbeat Energetic Guitar Rhythm: [https://www.free-stock-music.com/fm-freemusic-inspiring-optimistic-upbeat-energetic-guitar-rhythm.html](https://www.free-stock-music.com/fm-freemusic-inspiring-optimistic-upbeat-energetic-guitar-rhythm.html)  
Scandinavianz – Campfire: [https://www.free-stock-music.com/scandinavianz-campfire.html](https://www.free-stock-music.com/scandinavianz-campfire.html)

**Bibliografía utilizada para realizar este episodio:**

[https://adrianperales.com](https://adrianperales.com)  
[https://comunicatelibremente.wordpress.com/](https://comunicatelibremente.wordpress.com/)  
[https://es.wikipedia.org/wiki/Red_social_libre](https://es.wikipedia.org/wiki/Red_social_libre)  
[https://www.alainet.org/es/articulo/196339](https://www.alainet.org/es/articulo/196339)  
[https://hackeandoelgenoma.com/2016/11/diaspora-la-alternativa-libre-facebook-twitter/](https://hackeandoelgenoma.com/2016/11/diaspora-la-alternativa-libre-facebook-twitter/)  
[https://es.wikipedia.org/wiki/Internet_Relay_Chat](https://es.wikipedia.org/wiki/Internet_Relay_Chat)  
[https://es.wikipedia.org/wiki/IRCd#Instalaci%C3%B3n_y_configuraci%C3%B3n_del_IRCd](https://es.wikipedia.org/wiki/IRCd#Instalaci%C3%B3n_y_configuraci%C3%B3n_del_IRCd)  
[https://es.wikipedia.org/wiki/Extensible_Messaging_and_Presence_Protocol](https://es.wikipedia.org/wiki/Extensible_Messaging_and_Presence_Protocol)  
[https://es.wikipedia.org/wiki/Mastodon_(red_social)](https://es.wikipedia.org/wiki/Mastodon_(red_social))  
[https://es.wikipedia.org/wiki/Matrix_(protocolo)](https://es.wikipedia.org/wiki/Matrix_(protocolo))  
[https://es.wikipedia.org/wiki/GNU_Social](https://es.wikipedia.org/wiki/GNU_Social)  
[https://es.wikipedia.org/wiki/Fediverso](https://es.wikipedia.org/wiki/Fediverso)  
[https://es.wikipedia.org/wiki/Diaspora_(red_social)](https://es.wikipedia.org/wiki/Diaspora_(red_social))  
[https://en.wikipedia.org/wiki/Pump.io](https://en.wikipedia.org/wiki/Pump.io)  
[https://www.elotrolado.net/wiki/Pumpio](https://www.elotrolado.net/wiki/Pumpio)  
[https://es.wikipedia.org/wiki/Telegram_Messenger](https://es.wikipedia.org/wiki/Telegram_Messenger)  
[https://es.wikipedia.org/wiki/Signal_(software)](https://es.wikipedia.org/wiki/Signal_(software))  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1286868_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="86" height="31" />
