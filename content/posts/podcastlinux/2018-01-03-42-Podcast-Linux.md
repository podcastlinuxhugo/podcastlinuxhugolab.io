---
title: "#42 Linux Connexion con Álvaro Nova"
date: 2018-01-03
author: Juan Febles
categories: [podcastlinux]
img: PL/PL42.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL42
  image: https://podcastlinux.gitlab.io/images/PL/PL42.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL42.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL42.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero. Bienvenido a otro episodio de Podcast Linux.

 Mi nombre es Juan Febles y cada 15 días tú y yo tenemos una cita en este podcast, un lugar de encuentro para usuarios domésticos de escritorio del sistema operativo del ñu y el pingüino.  
 Hoy toca un Linux Connexion, las entrevistas para conocer más de cerca a personas y proyectos vinculados con GNU/Linux y es continuación del 41 Gaming y GNU/Linux.  
 Nos acompaña Álvaro Nova, más conocido por el ciberespacio como DioxCorp, podcaster, youtuber, fotógrafo y gamer. Linuxero de corazón y gran conocedor de este mundo.

 Contacto:

 Twitter: <https://twitter.com/DioxCorp>  
 Twitch: <https://www.twitch.tv/dioxcorp/>  
 Web: <https://alvaronova.com>

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 RoleMusic – He plays me the best Rhythms

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
