---
title: "#101 Especial #MaratonPodEnCasa"
date: 2020-04-08
author: Juan Febles
categories: [podcastlinux]
img: PL/PL101.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL101
  image: https://podcastlinux.gitlab.io/images/PL/PL101.png
tags: [Directo, MaratonPod, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL101.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL101.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Este especial #MaratonPodEnCasa nace de la organización para realizar un evento en estos días de confinamiento en España.  
Ha sido un placer poder aportar y colaborar en estos directos.  
Mi participación se centró en cómo utilizar Software Libre sin morir en el intento.  

Guion:  

+ Migrar o no migrar, ¿esa es la cuestión?
+ Un proceso, no un fin. No obsesionarse.

¿Qué es Software Libre?  
Licencias con 4 libertades para el usuario:  
+ Usar
+ Estudiar
+ Compartir
+ Mejorar

Filosofía para que la informática haga un mundo mejor.  
Una opción, no una obligación.  

Un proceso:  
+ Archivos Libres
+ Programas Libres
+ Servicios libres
+ Libera tus obras (Cultura Libre)
+ Utilizar Sistemas Operativos Libres

Archivos Libres:  
+ Estándares abiertos
+ Ofimática: .odt .pdf
+ Audio: .ogg .flac
+ Vídeo: contenedor mkv, webm códecs: imagen (VP8, VP9) y audio (Vorbis)
+ Fotografía: .png
+ Dibujo lineal: .svg
+ Dibujo artístico: .png

Programas Libres:
+ Ofimática: LibreOffice
+ Audio: Audacity, Ardour
+ Vídeo: VLC, Shotcut, OBS Studio
+ Fotografía: GIMP, Darktable
+ Dibujo lineal: Inkscape
+ Dibujo artístico:Kryta
+ Maquetación: Scribus
+ Diseño 3D: Freecad, Blender

Servicios Libres:  
+ Buscador Google – Duck Duck Go
+ Correo Gmail – Correo Disroot.org , tutanota.com (1gb gratuito)
+ Chrome – Firefox, Chromium
+ Google Drive – NextCloud, Syncthing
+ Google Meet (HangOuts) – Jitsi Para web (Firefox), IOS y Android
+ Google Maps – Open Street Map, Maps.me(Android)
+ Google Earth – Marble (KDE App). Escritorio y dispositivos móviles
+ Google Home – Mycroft (Asistente virtual y Altavoz inteligente con una Raspberry Pi)
+ Google Photos – NextCloud Photos, Syncthing
+ Youtube – MediaGoblin, PeerTube, Archive.org
+ Google Keep – Notas NextCloud
+ Google Calendar – Calendario NextCloud
+ Traductor Google – Apertium
+ Google Classroom – Moodle
+ Twitter, Facebook – GNU Social, Diaspora, Mastodon

Libera tus obras (Cultura Libre):  
+ Creative Commons vs Copyright
+ Creative Commons y Cultura Libre
+ Atribución, Compatir Igual, Sin obras Derivadas, No Comercial

Utilizar Sistemas Operativos Libres:  
+ Instalar GNU/Linux desde un pendrive
+ Comprar ordenadores GNU/Linux

Enlaces de interés:  
Web MaratonPod: <https://www.maratonpod.es/>  
Youtube MaratonPod: <https://www.youtube.com/MaratonPod>  
Twitter MaratonPod: <https://twitter.com/maratonpod>  

La imagen utilizada en la portada, el logo MaratonPod, es propiedad dela propia organización.  

Toda la música utilizada en este episodio se distribuye bajo la licencia Creative Commons:  
MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
