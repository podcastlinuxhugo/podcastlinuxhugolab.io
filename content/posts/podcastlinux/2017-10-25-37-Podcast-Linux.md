---
title: "#37 Cultura Libre"
date: 2017-10-25
author: Juan Febles
categories: [podcastlinux]
img: PL/PL37.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL37
  image: https://podcastlinux.gitlab.io/images/PL/PL37.png
tags: [Cultura, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL37.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL37.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero. Bienvenido a tu cita quincenal con Podcast Linux.  
 Mi nombre es Juan Febles y cada 2 semanas comparto, un nuevo tema o entrevista, del sistema operativo de escritorio que más nos gusta: GNU/Linux.  
 Ponte cómodo porque el episodio 37 Cultura Libre comienza ya.

 En el Núcleo Kernel nos adentraremos en qué es el movimiento de la Cultura libre, qué conlleva y cómo puedes aportar en ella.  
 En el Gestor de Paquetes te hablaré de MediaWiki, software wiki libre de código abierto escrito en PHP originalmente para su uso en Wikipedia.  
 <https://www.mediawiki.org/wiki/MediaWiki/es>  
 En Comunidad Linux abriremos las puertas a WikiMedia España, asociación por el conocimiento libre, cuyo objetivo, entre otros, es la promoción de proyectos colaborativos como Wikipedia.  
 <https://www.wikimedia.es/>  
 Por último, en Área de Notificaciones, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 Bibliografía para el episodio:  
 [https://es.wikipedia.org/wiki/Cultura\_libre](https://es.wikipedia.org/wiki/Cultura_libre)  
 <https://www.articaonline.com/2014/08/que-es-la-cultura-libre-tema-1-encirc/>  
 [http://www.worcel.com/archivos/6/Cultura\_libre\_Lessig.pdf](http://www.worcel.com/archivos/6/Cultura_libre_Lessig.pdf)  
 [http://manzanamecanica.org/2009/11/que\_es\_cultura\_libre.html](http://manzanamecanica.org/2009/11/que_es_cultura_libre.html)  
 <https://ubunlog.com/como-instalar-mediawiki-en-ubuntu/>  
 <https://sites.google.com/a/albertoruiz.es/albertoruiz/mis-articulos/instalaci>

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Delta Worldwide Group – Adrew Gate – Dare to Dream  
 Delta Worldwide Group – McAlvis – When It Rains  
 Delta Worldwide Group – Ray AndRey – Cerulean  
 Delta Worldwide Group – Santa Jack Prod – Brilliant  
 Delta Worldwide Group – Serge Ray Radar

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
