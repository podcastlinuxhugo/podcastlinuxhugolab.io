---
title: "#89 Linux Connexion con Irene Soria"
date: 2019-10-09
author: Juan Febles
categories: [podcastlinux]
img: PL/PL89.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL89
  image: https://podcastlinux.gitlab.io/images/PL/PL89.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL89.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL89.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega, la número 89, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy está con nosotros [Irene Soria](https://twitter.com/arenitasoria), diseñadora gráfica, activista de la Cultura Libre y el Software Libre y miembro de Creative Commons México.

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y agradecer a [Neodigit](https://www.neodigit.net/), nuestro proveedor de alojamiento y servicios de confianza de habla hispana para todo Avpodcast.

**Enlaces de interés:**

Twitter Irene Soria: [https://twitter.com/arenitasoria](https://twitter.com/arenitasoria)  
Blog Irene Soria: [http://www.irenesoria.com/](http://www.irenesoria.com/)  
Tesis Software Libre y Diseño Gráfico: [https://archive.org/details/SoriaGuzmanIrene-Tesis2012](https://archive.org/details/SoriaGuzmanIrene-Tesis2012)  
Libro Ética Hacker, seguridad y vigilancia: [http://www.ucsj.edu.mx/pdf/EticaHackerSeguridadVigilancia.pdf](http://www.ucsj.edu.mx/pdf/EticaHackerSeguridadVigilancia.pdf)  
Fanzine Que no quede huella: [https://www.derechosdigitales.org/wp-content/uploads/que-no-quede-huella.pdf](https://www.derechosdigitales.org/wp-content/uploads/que-no-quede-huella.pdf)  
Cultura Libre: [https://www.derechosdigitales.org/culturalibre/cultura_libre.pdf](https://www.derechosdigitales.org/culturalibre/cultura_libre.pdf)  
El DRM y el control de lo que lees: [http://editorial.centroculturadigital.mx/articulo/el-drm-y-el-control-de-lo-que-lees](http://editorial.centroculturadigital.mx/articulo/el-drm-y-el-control-de-lo-que-lees)  
Cuando seamos Hackers: [https://www.tierraadentro.cultura.gob.mx/cuando-seamos-hackers/](https://www.tierraadentro.cultura.gob.mx/cuando-seamos-hackers/)  
Vídeo Creative Commons México: [https://vimeo.com/319545705](https://vimeo.com/319545705)

Las **imagen utilizada en la portada** ha sido cedida por Irene Soria. Realizada por el fotógrafo Sebastian Ter Burg y bajo licencia **Creative Commons** Atribución.

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
MBB – Destination: [https://www.free-stock-music.com/mbb-destination.html](https://www.free-stock-music.com/mbb-destination.html)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1289890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
