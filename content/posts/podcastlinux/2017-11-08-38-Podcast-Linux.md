---
title: "#38 Linux Connexion con Wikimedia España"
date: 2017-11-08
author: Juan Febles
categories: [podcastlinux]
img: PL/PL38.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL38
  image: https://podcastlinux.gitlab.io/images/PL/PL38.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL38.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL38.mp3" type="audio/mpeg">
</audio>

 Muy buenas Linuxero!!!  
 Bienvenido a otro Linux Connexion, exactamente la número 38, de Podcast Linux. Un cordial saludo de quien te habla, Juan Febles. Hoy tenemos con nosotros a Helena e Iván, miembros de Wikimedia España.  
 Muy buenas.

 Helena, alias Santamarcanda en las redes, es portuguesa, aunque ahora reside en Canarias al quedarse enamorada de sus islas. A sus 24 años colabora con la asociación Wikimedia España para poder llevar a cabo proyectos relacionados con el conocimiento libre, tanto aportando contenido multimedia como mejorando y traduciendo artículos en 3 idiomas (español, Inglés y Portugués). Desde que empezó a indagar en el conocimiento libre algo le hizo interesarse de forma «lógica» por el uso de un software que se correspondiera con sus ideales, así que desde hace tiempo que trabaja con Debían 9 en su entorno de escritorio de Gnome 3.  
 “El conocimiento elimina la ignorancia, y saber nos hace libres y respetuosos con el mundo y todos sus habitantes.”

 Iván, alias Ivanhercaz en las redes, ha cumplido en 2017 su sueño de graduarse como historiador en la ULPGC y es un apasionado de las Humanidades Digitales. Desde hace más de un año colabora con la asociación Wikimedia España y sus proyectos para la difusión del conocimiento libre, aportando su granito tanto mejorando artículos, creándolos, aportando contenido multimedia o ejerciendo su función de reversor. Como no, también le apasiona el Software libre y el hardware, así que en Podcast Linux se siente como en casa. Utiliza Debían 9 con entorno de escritorio Cinnamon.  
 “En Wikipedia tienes un gran campo con el que poder trabajar todo tipo de ideas así que nos anima a todos a indagar en el gran mundo del conocimiento libre.”

 **Contacto:**  
 <https://www.wikimedia.es/>  
 [https://twitter.com/wikimedia\_es](https://twitter.com/wikimedia_es)  
 [https://t.me/wikimedia\_es](https://t.me/wikimedia_es)  
 [https://twitter.com/Santamarcanda\_](https://twitter.com/Santamarcanda_)  
 <https://twitter.com/IvanHerCaz>  
 [helenaamaralteixeira@gmail.com](mailto:helenaamaralteixeira@gmail.com)  
 [Ivanhercaz@gmail.com](mailto:Ivanhercaz@gmail.com)  
 <https://www.ivanhercaz.com>  
 <https://www.santamarcanda.wordpress.com>

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 meHiLove – Fashion Inspiration

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
