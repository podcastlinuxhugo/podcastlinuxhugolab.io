---
title: "#34 Directo Maratón Linuxero"
date: 2017-09-11
author: Juan Febles
categories: [podcastlinux]
img: PL/PL34.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL34
  image: https://podcastlinux.gitlab.io/images/PL/PL34.png
tags: [Directo, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL34.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL34.mp3" type="audio/mpeg">
</audio>

 Muy buenas linuxeros.

 Bienvenidos al episodio 34 de Podcast Linux, mi nombre es Juan Febles y hoy estamos en un marco incomparable, el I Maratón Linuxero. Hoy he querido estar con amigos para hablar sobre una época que no viví, pero que siempre está en boca de todos, los inicios de GNU/Linux, sus comunidades y filosofía. Con ellos conversaremos sobre los 90 y principios del 2000 antes de que irrumpiera Ubuntu y lo cambiara todo.

  **Gabriel Viso** es usuario de GNU/Linux desde 1997, un año antes de empezar los estudios de ingeniero de telecomunicación en la Universidad de Vigo. Desde entonces, ha usado Linux en distintas versiones hasta la actualidad. Actualmente vive en Sydney donde trabaja en el departamento de tecnología de un banco, y tiene varios podcasts a través de los que castiga a los oyentes de diferentes maneras. Si os promete regularidad «no le hagáis caso.” Estuvo con nosotros en el [Linux Connexion 11](https://avpodcast.net/podcastlinux/11-linux-connexion-gabriel-viso-pitando-net-podcast-linux/).

 **Patricio García** es Licenciado y Doctor en Informática por la Universidad de Las Palmas de Gran Canaria, profesor del Departamento de Ingeniería Informática y de Sistemas de la Universidad de La Laguna, y actualmente dirige la Oficina de Software Libre de la misma. Trabaja con entornos Unix y Linux desde principios de los años noventa, usándolos en sus clases e investigaciones. Estuvo con nosotros en el [Linux Connexion 22](https://avpodcast.net/podcastlinux/osl/).

 **Alejandro López**, es director comercial de Slimbook y usuario de Debian en sus inicios, aunque en aquellos años tenía mucho tirón Red Hat y dió buena cuenta de ello dedicándole bastante tiempo. También pasó por distribuciones como Mandrake y SUSE, conociendo escritorios como KDE. Siempre estuvo más cómodo con Gnome, hasta la llevada de Gnome 3, que le hizo volver a KDE. Ha probado escritorios como Xcfe, Lxde, Cinnamon y Mate. Aunque actualmente administra servidores y programa, hace años cachareaba instalando Puppy Linux en maquinitas y gestores de turnos que sólo disponían de 128MB de disco. Estuvo con nosotros en los Linux Connexion [12](https://avpodcast.net/podcastlinux/slimbook/) y 29.

 **Roberto Ruisánchez** es Ingeniero informático, y actualmente trabaja en una empresa de servicios informáticos. Sus comienzos con Linux fueron en Citius Debian, en 1998, y desde entonces no se ha sentido a gusto en otras distribuciones que no tuvieran apt-get. Aparte de en varios ordenadores ha instalado linux en el router, en el NAS, en la raspberry e incluso en el TDT. Actualmente da la lata desde varios podcast en tiempoescaso.es y es compañero junto a Gabriel de Droidtalks.

 Una charla muy entretenida

 Para conocer más a fondo este proyecto, te dejo sus métodos de contacto:

 
 * Blog: [Maratón Linuxero](https://maratonlinuxero.org/)
 * Correo: [maratonlinuxero@gmail.com](mailto:maratonlinuxero@gmail.com)
 * Telegram: [@maratonlinuxero](https://telegram.me/maratonlinuxero)
 * Mastodon: [maratonlinuxero@mastodon.social](https://mastodon.social/@maratonlinuxero)
 * Twitter: [@maratonlinuxero](https://www.twitter.com/maratonlinuxero)
 * Youtube: <https://www.youtube.com/maratonlinuxero>
 * Radio Maratón: [http://www.radiomaraton.ml](http://www.radiomaraton.ml/)
 * Feed Podcast: [Maratón Linuxero](https://maratonlinuxero.org/feed.xml)
 * Audios en Archive.org: [@maratonlinuxero](https://archive.org/details/@maratonlinuxero)
 
 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 LukHash – The Other Side  
 ADDICT SOUND – Way to Success (2016)

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
