---
title: "#58 Linux Connexion con Dabo"
date: 2018-08-13
author: Juan Febles
categories: [podcastlinux]
img: PL/PL58.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL58
  image: https://podcastlinux.gitlab.io/images/PL/PL58.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL58.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL58.mp3" type="audio/mpeg">
</audio>

 Muy buenas amante de GNU/Linux y el Software Libre.  
 Bienvenido a otra entrega, la número 58, de Podcast Linux. Un saludo muy fuerte de quien está detrás del micrófono, Juan Febles.  
 Hoy tengo el inmenso placer y privilegio de estar con David Hernández, más conocido como Dabo, incansable comunicador, divulgador y creador de muchos proyectos como Dabo Blog, ApacheCtl, Caborian, DebianHachers y sin duda alguna, la joya por los que muchos te conocimos DaboBlog Podcast.

 **Métodos de contacto:**

 Podcast: [https://www.ivoox.com/podcast-daboblog-podcast\_sq\_f12610\_1.html](https://www.ivoox.com/podcast-daboblog-podcast_sq_f12610_1.html)  
 Feed Dabo Blog Podcast: [https://www.ivoox.com/daboblog-podcast\_fg\_f12610\_filtro\_1.xml](https://www.ivoox.com/daboblog-podcast_fg_f12610_filtro_1.xml)  
 Blog: <https://daboblog.com/>  
 Twitter: <https://twitter.com/daboblog>  
 Correo: [dabo@riseup.net](mailto:dabo@riseup.net)

 Recordar a los oyentes que estamos en una sala Jitsi cedida por [Neodigit.net](https://neodigit.net/), nuestro proveedor de confianza de habla hispana, y vamos a hablar con quien inició el podcasting sobre GNU/Linux en español.

 La **imagen utilizada en la portada** es propiedad de [ConectaCon](https://www.facebook.com/ConectaCon/photos/a.524464434277167.1073741834.245338748856405/526502594073351/?type=3&theater) y tenemos permiso para usarla como portada en este episodio.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 [LukHash – The Other Side](https://www.jamendo.com/track/1248105/the-other-side)  
 [Actual Proof – Venus in Cancer](https://www.soundclick.com/html5/v4/player.cfm?songID=5850755)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
