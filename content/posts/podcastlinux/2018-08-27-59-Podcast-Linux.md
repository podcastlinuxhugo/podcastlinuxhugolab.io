---
title: "#59 Linux Connexion con Manz"
date: 2018-08-27
author: Juan Febles
categories: [podcastlinux]
img: PL/PL59.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL59
  image: https://podcastlinux.gitlab.io/images/PL/PL59.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL59.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL59.mp3" type="audio/mpeg">
</audio>

 Muy buenas amante del Software Libre!!!  
 Te doy la bienvenida a otra entrega de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles.  
 Hoy va a ser un Linux Connexion muy especial, ya que va a ser presencial. Tengo a mi lado a José Román Hernández, más conocido como Manz, es el autor de [Emezeta.com](https://www.emezeta.com/), uno de los blogs de tecnología e informática más conocidos de Canarias. Utiliza GNU/Linux desde hace más de 15 años y es un divulgador activo del mundo de la Tecnología, el Software Libre y el Desarrollo Web.

 Actualmente, imparte cursos con la [Oficina de Software Libre de la Universidad de La Laguna](https://osl.ull.es/), generalmente centrados en el desarrollo y diseño web o en temáticas relacionadas como el perfeccionamiento del uso de la Terminal de Linux, administración de servidores o Diseño gráfico con Inkscape.

 Creador de plataformas de aprendizaje como [LenguajeCSS.com](https://lenguajecss.com/), [LenguajeHTML.com](https://lenguajehtml.com/), [LenguajeJS.com](https://lenguajejs.com/) o [TerminaldeLinux.com](https://terminaldelinux.com/), actualmente es director y profesor del curso de [Programación web FullStack de EOI](https://www.eoi.es/es/cursos/32220/curso-de-programacion-fullstack-santa-cruz-de-tenerife) en Tenerife y también mentoriza proyectos de emprendedores y startups con base tecnológica.

 **Métodos de contacto:**  
 Blog: <https://www.emezeta.com>  
 Twitter: <https://twitter.com/Manz>  
 Correo: [manz@emezeta.com](mailto:manz@emezeta.com)

 La **imagen utilizada en la portada** es propiedad de [Manz](https://aboutme.imgix.net/background/manz_1330447960_47.jpg).

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 [LukHash – The Other Side](https://www.jamendo.com/track/1248105/the-other-side)  
 [BACKIT – Future City](https://www.jamendo.com/track/1535184/future-city)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
