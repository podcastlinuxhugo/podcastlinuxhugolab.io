---
title: "#105 Linux Connexion con Gabriel Viso"
date: 2020-06-03
author: Juan Febles
categories: [podcastlinux]
img: PL/PL105.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL105
  image: https://podcastlinux.gitlab.io/images/PL/PL105.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL105.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL105.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  
Bienvenido a otra entrega de Podcast Linux, la número 105. Un saludo muy fuerte de quien te habla, Juan Febles. Estamos con [Gabriel Viso](https://gvisoc.com/), podcaster español que reside en Australia desde hace unos años. Estuvo con nosotros en el [episodio 11](https://podcastlinux.com/11-Podcast-Linux/) para hablar de Raspberry con sus podcasts Pitando y Bajo la Carcasa.  
Actualmente sigue con [Sobre La Marcha](https://anchor.fm/gvisoc), un popurrí de Tecnología de sus experiencias y algunas noticias sobre seguridad y privacidad.  

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y agradecer a [Neodigit](https://neodigit.net/), nuestro proveedor de alojamiento y servicios de confianza de habla hispana.  

Enlaces de interés:  
Web: <https://gvisoc.com/>  
Twitter: <https://twitter.com/gvisoc>  
Sobre La Marcha: <https://anchor.fm/gvisoc>  

Las imagen utilizada en la portada, la foto de Gabriel Viso,  es propiedad de Gabriel Viso y cedida por él mismo para este episodio.  

Toda la música utilizada en este episodio se distribuye bajo la licencia Creative Commons:  

MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
Roa Music - Nightfall: <https://www.free-stock-music.com/roa-music-nightfall.html>  


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
