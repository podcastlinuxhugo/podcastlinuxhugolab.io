---
title: "#08 Sabores a montones"
date: 2016-09-29
author: Juan Febles
categories: [podcastlinux]
img: PL/PL08.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL08
  image: https://podcastlinux.gitlab.io/images/PL/PL08.png
tags: [Entornos de Escritorio, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL08.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL08.mp3" type="audio/mpeg">
</audio>

 **NOTICIÓN:** Podcast Linux pasa a formar parte de la red AV Podcast. <http://avpodcast.net/>

 En el Núcleo Kernel hablaré de los principales entornos de escritorio.

 En el Gestor de Paquetes te presento a OpenShot, un sencillo editor de vídeo multiplataforma. <http://www.openshot.org/>

 El invitado de Comunidad Linux es Jenofonte, un gran divulgador del software libre de producción multimedia.

 Twitter: [@jen0f0nte](https://twitter.com/jen0f0nte)

 Blog: <http://unade25.blogspot.com.es/>

 Youtube: <https://www.youtube.com/user/jen0f0nte>

 Email: [unade25@gmail.com](mailto:unade25@gmail.com)

 Por último, en Área de Notificaciones, le daré un repaso a los mensajes que he recibido en este último mes.

  

 Recuerda que puedes contactar conmigo de la siguiente manera:

 a través de Twitter [@podcastlinux](https://twitter.com/podcastlinux)

 por correo [podcastlinux@gmail.com](mailto:podcastlinux@gmail.com)

 y el la web [avpodcast.net/podcastlinux](http://avpodcast.net/podcastlinux/)

 No te olvides suscribirte en Ivoox o Itunes para no perderte ninguno de mis episodios.

 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:

 Artistas:

 LukHash

 Cleric

 Matti Paalanen

 The Polish Ambassador

 Olivier Giraldot

 Social Bot

 Floating isle

 Rosver

 Prysm

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1285850_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
