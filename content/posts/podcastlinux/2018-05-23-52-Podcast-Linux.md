---
title: "#52 Linux Connexion con Mosqueteroweb"
date: 2018-05-23
author: Juan Febles
categories: [podcastlinux]
img: PL/PL52.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL52
  image: https://podcastlinux.gitlab.io/images/PL/PL52.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL52.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL52.mp3" type="audio/mpeg">
</audio>
  
 Muy buenas de nuevo, amante de GNU/Linux. Te doy la bienvenida a otro episodio de Podcast Linux.  
 Mi nombre es Juan Febles y quincenalmente coincidimos en este podcast, un espacio abierto donde se invita a quienes nos gusta el sistema operativo del ñu y el pingüino.

 Hoy toca un Linux Connexion, las entrevistas para conocer más de cerca a personas y proyectos vinculados con GNU/Linux y es continuación del 51 Chromebook.

 Por fin coincido con Mosqueteroweb, Pedro. Como bien sabes él es podcaster, bloguero y profesor en Formación profesional en la rama de Computación.  
 Recordar a los oyentes que estamos en una sala Jitsi cedida por [Neodigit.net](https://neodigit.net/) , nuestro proveedor de confianza de habla hispana, y vamos a profundizar sobre el sistema operativo de Google.

  

 **Contacto:**  
 Twitter: <https://twitter.com/mosqueteroweb>  
 Telegram: <https://t.me/mosqueteroweb>  
 Podcast: <http://feeds.feedburner.com/mosqueterowebLinux>  
 Youtube: <https://www.youtube.com/PedroMosqueteroweb>

 La imagen utilizada es el logo de Mosqueteroweb.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other SideLukHash – The Other Side  
 AlexZavesa – Night Corporate

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
