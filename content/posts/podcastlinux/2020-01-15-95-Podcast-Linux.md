---
title: "#95 Fotografía y GNU/Linux"
date: 2020-01-15
author: Juan Febles
categories: [podcastlinux]
img: PL/PL95.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL95
  image: https://podcastlinux.gitlab.io/images/PL/PL95.png
tags: [Fotografía, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL95.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL95.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy toca episodio formal. En poco más de media hora le echaremos un vistazo a la fotografía dentro del mundo GNU/Linux.  
Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 95, Fotografía y GNU/Linux.  

En el núcleo Kernel te daré a conocer las posibilidades que tenemos en el software libre en referencia a la Fotografía. En Gestor de Paquetes tendremos a [GIMP](https://www.gimp.org/), el programa de edición fotográfica por excelencia. En Comunidad Linux, nos visitará Álvaro Nova, [fotógrafo](https://alvaronova.com/), [podcaster](https://www.ivoox.com/podcast-foreveralonepodcast_sq_f119948_1.html) y Linuxero con el que veremos qué nos ofrece el Software Libre con respecto al tratamiento y edición de imágenes. Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.  

Antes de pasar de sección: falta poco para el episodio 100. Anímate a compartir qué te gustaría que hiciese en ese episodio. ¿Un episodio recopilatorio, una charla en seguidores?¿Preguntas y respuestas sobre Podcast Linux? ¿Otra cosa? Compártelo en los comentarios del episodio.  

También quiero recordar a [Kira](https://twitter.com/podcastlinux/status/1212046476068708352), nuestra perrita que nos dejó hace días y que quiero agradecer todo el cariño que nos dio a Marta y a mí especialmente, y a todos los que la conocieron. Un besote muy cariñoso y no pares de correr y ser feliz.

<img src="{{< ABSresource url="/images/PL/IMG_20190530_100238-300x169.jpg" >}}" alt="Kira" title="Kira" width="300" height="169" />

Programas:  

DarkTable: <https://www.darktable.org/>  
RawTherapee: <https://www.rawtherapee.com/>  
UfRaw: <http://ufraw.sourceforge.net/>  
LightZone: <https://lightzoneproject.org/>  
Image Magick: <https://imagemagick.org/>  
GIMP: <https://www.gimp.org/>  

Libro de [Jen0fonte](https://twitter.com/jen0f0nte) sobre Fotografía Digital Libre: <https://colaboratorio.net/jen0f0nte/multimedia/fotografia/2018/fin-de-flac-y-bienvenido-fotografia-libre/>  

La imagen utilizada en la portada, el hombre 3D se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/images/id-1889033>          

Toda la música utilizada en este episodio se distribuye bajo la licencia Creative Commons:  

MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
Arhur Yul – Cinema Intro: <https://www.jamendo.com/track/1503848/>  
GarnaVutka – Corporate: <https://www.jamendo.com/track/1354699>  
Zoran Sebic – Innovative Logo: <https://www.jamendo.com/track/1487845>  
GarnaVutka – Crystal Wellcome Logo: <https://www.jamendo.com/track/1508030>  
GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): <https://www.jamendo.com/track/1464611>  
MusicbyAnden – Skyline: <https://www.free-stock-music.com/musicbyaden-skyline.html>  
MusicbyAnden – River: <https://www.free-stock-music.com/musicbyaden-river.html>  
MusicbyAnden – Nigth: <https://www.free-stock-music.com/musicbyaden-night.html>  
MusicbyAnden – Colours: <https://www.free-stock-music.com/musicbyaden-colours.html>  
MusicbyAnden – Adventure: <https://www.free-stock-music.com/musicbyaden-adventure.html>  


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1295890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
