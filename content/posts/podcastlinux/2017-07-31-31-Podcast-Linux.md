---
title: "#31 Especial TLP2017"
date: 2017-07-31
author: Juan Febles
categories: [podcastlinux]
img: PL/PL31.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL31
  image: https://podcastlinux.gitlab.io/images/PL/PL31.png
tags: [Especial, TLP, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL31.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL31.mp3" type="audio/mpeg">
</audio>

 Muy buenas linuxer@, bienvenid@ a otro especial. En esta ocasión no es un directo, sino el evento tecnológico más importante de mi isla, la Tenerife LanParty ( TLP2017 ).  
 Gracias a la OSL de la Universidad de La Laguna he podido compartir varios días en este evento y quiero hacerte llegar todo lo vivido allí.

 Los diferentes audios recogidos son:

 [Molotov Studio](http://www.molotov-studios.es/)  
 [Canariarcade](https://www.facebook.com/pg/canariarcade.maquinasrecreativas/about/?ref=page_internal)

 ![]({{< ABSresource url="/images/PL/IMG_20170719_150910-300x169.jpg" >}})

 [WikiMedia España](https://www.wikimedia.es)

 ![]({{< ABSresource url="/images/PL/IMG_20170721_103615-300x169.jpg" >}})

 [Juan Fernando de la Rosa](https://www.youtube.com/playlist?list=PLg5lkP4zsMJRKRkYhl1nSoChDqNZbo1XU) y [FreeCad](https://www.freecadweb.org/?lang=es_ES)

 Varias entrevistas para dar a conocer el podcasting.

 ![]({{< ABSresource url="/images/PL/IMG_20170720_184126-300x225.jpg" >}})

 ![]({{< ABSresource url="/images/PL/photo_2017-07-20_20-08-44-300x225.jpg" >}})

 ![]({{< ABSresource url="/images/PL/photo_2017-07-20_19-55-09-300x169.jpg" >}})

 [Gamika Podcast](http://gamika.es/podcast/)

 ![]({{< ABSresource url="/images/PL/IMG_20170720_195030-300x169.jpg" >}})

 Patricio García, director de la [OSL](http://osl.ull.es/) de la [Universidad de La Laguna](https://www.ull.es/).

 ![]({{< ABSresource url="/images/PL/IMG_20170719_153446-300x169.jpg" >}})

  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
