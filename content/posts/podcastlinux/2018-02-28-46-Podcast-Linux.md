---
title: "#46 Linux Connexion con GALPon"
date: 2018-02-28
author: Juan Febles
categories: [podcastlinux]
img: PL/PL46.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL46
  image: https://podcastlinux.gitlab.io/images/PL/PL46.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL46.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL46.mp3" type="audio/mpeg">
</audio>

***Atención: Hay algunas partes en la entrevista, sobre todo al principio, que la calidad del audio no es la óptima a la que estamos acostumbrados. Pido disculpas de antemano. Creo que el contenido de la charla sí está a la altura de todos los episodios del programa y disfrutarán de este proyecto como yo lo hice al grabarlo.** *

 Muy buenas, amante de GNU/Linux y el Software Libre. Estás escuchando Podcast Linux, el programa para usuarios y usuarias domésticos del sistema operativo del ñu y el pingüino.  
 Hoy, en el episodio 46, tendremos un Linux Connexion, las entrevistas para conocer más de cerca los proyectos y personas que presento en la sección Comunidad Linux.  
 En esta ocasión tengo el lujo de compartir este espacio con Galpon, Grupo de Amigos de Linux de Pontevedra, y en especial con Miguel Anxo Bouzada y Antonio Sánchez. Ambos son los desarrolladores principales y coordinadores de la distro ligera Minino, uno de los grandes proyectos de esta asociación.

 Contacta con **GALPon**:  
 Web: <https://www.galpon.org/>  
 Correo: [info@galpon.org](mailto:info@galpon.org)  
 Telegram: <https://t.me/galpon>

 **Minino:  
 **Web:[ https://minino.galpon.org/es](https://minino.galpon.org/es)  
 Correo: [minino@galpon.org](mailto:minino@galpon.org)  
 Telegram: <https://t.me/galponminino>**  
 **

 **PicarOS:  
 **Web: <https://minino.galpon.org/es/videotutoriales>  
 Telegram: <https://t.me/picaros>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other SideLukHash – The Other Side  
 FantomStudio – Ambient Inspiring

 Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
