---
title: "#76 Especial FLISoL 2019"
date: 2019-04-24
author: Juan Febles
categories: [podcastlinux]
img: PL/PL76.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL76
  image: https://podcastlinux.gitlab.io/images/PL/PL76.png
tags: [Especial, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL76.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL76.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y tenemos un especial muy especial entre manos. Hoy te quiero acercar un evento muy importante en el Software Libre.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 76, FLISol 2019.

 Antes de empezar me gustaría recordarte que Podcast Linux forma parte de la red [AvPodcast](https://avpodcast.net), y todo Avpodcast esta alojado en [Neodigit.net](https://neodigit.net/) , nuestro proveedor de confianza de habla hispana. Son profesionales serios y trabajan con muchas soluciones de Software Libre. Gracias Neodigit.net por hacer que éste y muchísimos podcasts salgan a la luz.

 **Enlaces de interés:  
 **FLISol: <https://flisol.info>  
 FliSOL Tenerife: <https://flisol.info/FLISOL2019/Espana/Tenerife>  
 Mumble: <http://www.mumble.info/>  
 Jitsi: <https://meet.jit.si/>  
 Telegram FLISOL Tenerife: <https://t.me/flisoltenerife19>  
 Gitlab: Próximamente…  
 Inkscape: <https://inkscape.org/es/>  
 Hackmd.io: <https://hackmd.io/>  
 Framasoft.org: <https://framasoft.org/en/>  
 Framadate (Planificación horario): <https://framadate.org/>

 ![]({{< ABSresource url="/images/PL/cartel-flisol-tenerife-300x212.png" >}})

 **La imagen utilizada en la portada, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): **<https://pixabay.com/images/id-1871375/>  
 El logo de FLISOL es propiedad del propio proyecto.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 GarnaVutka – Corporate Ambient (Full version) (2019) : <http://jamen.do/t/1643395>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
