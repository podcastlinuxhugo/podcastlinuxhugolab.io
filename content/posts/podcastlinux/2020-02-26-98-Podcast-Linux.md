---
title: "#98 Linux Connexion con Álvaro Nova"
date: 2020-02-26
author: Juan Febles
categories: [podcastlinux]
img: PL/PL98.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL98
  image: https://podcastlinux.gitlab.io/images/PL/PL98.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL98.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL98.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  

Bienvenido a otra entrega, la número 98, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy está con nosotros Álvaro Nova, Ya lo tuvimos en el Linux Connexion sobre gaming y GNU/Linux. (Ep. 42), y fotografía (Ep. 96). Es un renacentista del S. XXI: gaming, fotografía, vídeo, podcasting, energía solar, raspberry Pi. Veremos cómo podemos afrontar la edición de vídeo desde el Software Libre.  
Puedes ver parte de su trabajo fotográfico en [alvaronova.com](https://alvaronova.com/)  

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y agradecer a [Neodigit](https://neodigit.net/), nuestro proveedor de alojamiento y servicios de confianza de habla hispana para todo [Avpodcast](https://avpodcast.net/).  

Y que falta poco para el episodio 100. Ya he decidido qué temática va a tener. Será un preguntas y respuestas sobre Podcast Linux. Todo lo que quieras saber sobre este proyecto o de mí puedes enviarlo en forma de pregunta, ya sea por medio de audio o texto. Responderé a todas ellas en el episodio 100. Envía tus audios o textos a <podcastlinux@avpodcast.net> o a telegram [@juanfebles](https://t.me/juanfebles).  

Álvaro tenía un reto por delante. Él utiliza Davinci Resolve, aplicación Freemium y privativa con fama de ser la que más posibilidades da a nivel profesional en GNU/Linux. Le pediremos que pruebe sus alternativas libres y nos diga a qué nivel están. También un poco de los contenedores y codecs libres así como del hardware y compatibilidad en GNU/Linux de éste. Tarjetas gráficas especialmente.  

Enlaces de interés:  
Blog: https://alvaronova.com/  
Youtube: https://www.youtube.com/channel/UC5xnm-eHlFRO8EEV9gTaNJw  
Forever Alone Podcast: https://www.ivoox.com/foreveralonepodcast_sq_f119948_1.html  
Twitter: https://twitter.com/DioxCorp  

Las imagen utilizada en la portada ha sido cedida por Álvaro Nova y es de su propiedad.  

Toda la música utilizada en este episodio se distribuye bajo la licencia Creative Commons:  

MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
Mixasund – Technology Bussinnes: <https://www.free-stock-music.com/mixaund-technology-business.html>  


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
