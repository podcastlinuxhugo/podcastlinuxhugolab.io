---
title: "Curso Podcasting"
date: 2018-01-03
author: Juan Febles
categories: [curso, podcasting]
img: 2018/cursopodcasting.png
podcast:
  audio: 
  video:
tags: [curso, podcasting, vídeo]
comments: true
---
Hace algún tiempo que llevo realizando un pequeño curso de podcasting con servicios y aplicaciones libres.  
Aquí te dejo los vídeos de [Archive.org](https://archive.org/details/@podcast_linux)

Prólogo:  
<video id="sampleMovie" width="640" height="360" preload controls>
	<source src="https://archive.org/download/CursoDePodcasting/CursoDePodcasting.webm" type='video/webm; codecs="vp8, vorbis"' />
	<source src="https://archive.org/download/CursoDePodcasting/CursoDePodcasting.ogv" type='video/ogg; codecs="theora, vorbis"' />
	<source src="https://archive.org/download/CursoDePodcasting/CursoDePodcasting.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

Tema 0: Previos  
<video id="sampleMovie" width="640" height="360" preload controls>
	<source src="https://archive.org/download/CursoPodcasting_0Previos/CursoPodcasting0Previos.webm" type='video/webm; codecs="vp8, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting_0Previos/CursoPodcasting0Previos.ogv" type='video/ogg; codecs="theora, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting_0Previos/CursoPodcasting0Previos.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

Tema 1: Crea tu propia carátula con [Inkscape](https://inkscape.org/es/)  
<video id="sampleMovie" width="640" height="360" preload controls>
	<source src="https://archive.org/download/CursoPodcasting1Inkscape/CursoPodcasting1Inkscape.webm" type='video/webm; codecs="vp8, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting1Inkscape/CursoPodcasting1Inkscape.ogv" type='video/ogg; codecs="theora, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting1Inkscape/CursoPodcasting1Inkscape.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

Tema 2: Crea tus audios con [Audacity](http://www.audacityteam.org/)  
<video id="sampleMovie" width="640" height="360" preload controls>
	<source src="https://archive.org/download/CursoPodcasting2Audacity/CursoPodcasting2Audacity.webm" type='video/webm; codecs="vp8, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting2Audacity/CursoPodcasting2Audacity.ogv" type='video/ogg; codecs="theora, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting2Audacity/CursoPodcasting2Audacity.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

Tema 3: Hospeda tus audios en [archive.org](https://archive.org)  
<video id="sampleMovie" width="640" height="360" preload controls>
	<source src="https://archive.org/download/CursoPodcasting3ArchiveOrg/CursoPodcasting3ArchiveOrg.webm" type='video/webm; codecs="vp8, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting3ArchiveOrg/CursoPodcasting3ArchiveOrg.ogv" type='video/ogg; codecs="theora, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting3ArchiveOrg/CursoPodcasting3ArchiveOrg.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

Tema 4: Crea tu blog y feed en [GitLab](https://gitlab.com/)  
<video id="sampleMovie" width="640" height="360" preload controls>
	<source src="https://archive.org/download/CursoPodcasting4Gitlab/CursoPodcasting4gitlab.webm" type='video/webm; codecs="vp8, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting4Gitlab/CursoPodcasting4gitlab.ogv" type='video/ogg; codecs="theora, vorbis"' />
	<source src="https://archive.org/download/CursoPodcasting4Gitlab/CursoPodcasting4gitlab.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

Extra 1: Conecta tu feed con servicios privativos (Ivoox y Itunes)  
<video id="sampleMovie" width="640" height="360" preload controls>
	<source src="https://archive.org/download/CursoPodcastingExtra1/CursoPodcastingExtra1.webm" type='video/webm; codecs="vp8, vorbis"' />
	<source src="https://archive.org/download/CursoPodcastingExtra1/CursoPodcastingExtra1.ogv" type='video/ogg; codecs="theora, vorbis"' />
	<source src="https://archive.org/download/CursoPodcastingExtra1/CursoPodcastingExtra1.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

Extra 2: Un micrófono para iniciarse en el podcasting (Proel DM581USB)  
<video id="sampleMovie" width="640" height="360" preload controls>
	<source src="https://archive.org/download/CursoPodcastingExtra2/Proel.webm" type='video/webm; codecs="vp8, vorbis"' />
	<source src="https://archive.org/download/CursoPodcastingExtra2/Proel.ogv" type='video/ogg; codecs="theora, vorbis"' />
	<source src="https://archive.org/download/CursoPodcastingExtra2/Proel.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

Tienes todos los vídeos también en [Youtube](https://www.youtube.com/PodcastLinux):  
<iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/videoseries?list=PLdt4gHTaH61HOOLsyAc2xYzdbinem9ooZ" width="640"></iframe>


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
