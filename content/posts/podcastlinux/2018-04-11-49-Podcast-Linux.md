---
title: "#49 Libera tu router"
date: 2018-04-11
author: Juan Febles
categories: [podcastlinux]
img: PL/PL49.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL49
  image: https://podcastlinux.gitlab.io/images/PL/PL49.png
tags: [Router, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL49.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL49.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy estoy puntual a la cita quincenal con un episodio para animarte a liberar tu red doméstica.  
 Ponte cómodo y disfruta de esta casi media hora de programa, el número 49, porque libera tu router comienza ya.

 En el **Núcleo Kernel** comentaré algunos aspectos a tener en cuenta para liberar y mejorar tu red doméstica con Openwrt y otras soluciones libres.  
 <https://openwrt.org/>  
 <https://piratebox.cc/>  
 <http://librarybox.us/>  
 [ https://nmap.org/](https://nmap.org/)  
 <https://f-droid.org/en/packages/com.vrem.wifianalyzer/>  
 <https://blog.cloudflare.com/announcing-1111/>  
 <https://kodi.tv/>

 En el **Gestor de Paquetes** te hablaré de Nmap, aplicación multiplataforma libre para rastrear tu red y analizar cómo se comporta.  
 <https://nmap.org/>

 En **Comunidad Linux** conoceremos a Hefistion, bloguero, amante de GNU/Linux y conocedor de OpenWrt, Gitlab y la Raspberry Pi, entre otras cosas.  
 [https://twitter.com/Hefistion\_](https://twitter.com/Hefistion_)  
 <https://elblogdelazaro.gitlab.io/>  
 <https://carlmon.gitlab.io/>  
 <https://gitlab.com/hefistion>

 Por último, en **Área de Notificaciones**, le daré un repaso a algunos de los mensajes recibidos en los últimos episodios.

 La **imagen del router utilizada** se ha liberado desde OpenClipart con licencia Creative Commons 0:  
 <https://openclipart.org/detail/195015/wireless-modem>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia libre **Creative Commons**:

 LukHash – The Other SideLukHash – The Other Side  
 Cleric – Short Tech Logo  
 Matti Paalanen – Ambient Logo  
 Matti Paalanen – Logo  
 Matti Paalanen – Bright  
 The Polish Ambassador – Erotic Robotics  
 Infraction – Summer Dance  
 Infraction – Dance Guitar  
 Infraction – Technological Day  
 Infraction – Energetic Opener  
 Infraction – Tech Corporate Morning

 **Bibliografía para realizar el episodio:**

 <https://enavas.blogspot.com.es/2014/02/reutilizar-routers-con-openwrt.html>  
 <http://murana.uy/instalando-openwrt-tp-link-wdr4300/>  
 <https://tombatossals.github.io/instalar-openwrt/>  
 <https://blog.desdelinux.net/aprovecha-tu-router-al-maximo-con-openwrt-libertad-inalambrica/>  
 <https://naseros.com/2016/12/27/como-reutilizar-un-router-como-repetidor-wifi-o-como-router-neutro/>  
 <ttps://www.wifi-online.es/blog\_wifi-online/que-es-pineapple-wifi-la-pina-wifi-2/>  
 <https://www.elotrolado.net/wiki/OpenWRT>  
 <https://omicrono.elespanol.com/2017/03/usos-router-viejo/>  
 <https://conocimientolibre.wordpress.com/2007/06/30/nmap-a-fondo-escaneo-de-redes-y-hosts/>  
 <https://www.eduardocollado.com/2018/04/04/podcast-144-el-mejor-dns/>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
