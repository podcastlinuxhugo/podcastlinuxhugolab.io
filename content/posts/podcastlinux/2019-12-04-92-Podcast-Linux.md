---
title: "#92 Productividad y GNU/Linux"
date: 2019-12-04
author: Juan Febles
categories: [podcastlinux]
img: PL/PL92.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL92
  image: https://podcastlinux.gitlab.io/images/PL/PL92.png
tags: [Productividad, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL92.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL92.mp3" type="audio/mpeg">
</audio>

Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y hoy estás en un episodio formal. En poco más de media hora te comentaré cómo puedes sacarle el máximo provecho a tu distribución GNU/Linux.Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 92, Productividad en GNU/Linux.

En el núcleo Kernel te comentaré qué he aprendido en estos años para ser más eficiente en GNU/Linux. En Gestor de Paquetes conoceremos al comando man. En Comunidad Linux, tendremos a [Atareao](https://www.atareao.es/), gran podcaster, bloguero y desarrollador en GNU/Linux. Terminaremos, como siempre, con el área de notificaciones para conocer los mensajes de oyentes del programa.

Atención: Falta poco para el episodio 100\. Anímate a compartir qué te gustaría que hiciese en ese episodio.

Atajos de teclado comentados:

**Sistema:**  
Súper: Menú de Aplicaciones  
Alt + F2: Lanzador de Aplicaciones, ficheros y búsquedas  
Súper + RePág: Maximiza ventana  
Súper + AvPág: Minimiza página  
Súper + Flechas: Encasilla la página a mitad de esa zona  
Alt + Tab: Cambiar de aplicación  
Alt + F4: Cerrar aplicación o ventana  
Súper + E: Lanzador de gestor de ficheros (KDE Dolphin)  
Ctrl + A: Seleccionar todo  
Ctrl + Mayús + A: Invertir selección  
Mayús + flechas: Seleccionar elementos izquierda, derecha, arriba o abajo.  
Ctrl + 1, 2 ,3: Cambiar modos de visualización (Iconos,Compacto,detallado )  
Ctrl + +: Acercar vista  
Ctrl + -: Alejar vista  
Ctrl + T: Abrir nueva pestaña  
Ctrl + W: Cerrar pestaña  
Ctrl + Tab: Navega por las pestañas  
Ctrl + F: Buscar  
F2: Cambiar nombre fichero  
F10: Crear nueva carpeta  
Alt + Enter: Propiedades del documento o carpeta  
Supr: Eliminar  
Ctrl + Supr: Eliminar definitivamente  
Ctrl + C: Copiar  
Ctrl + X: Cortar  
Ctrl + V: Pegar  
Ctrl + H: Ver Archivos ocultos  
Impr Pant: Captura de Pantalla  
Alt + Impr Pant: Captura de ventana actual  
Mayús + Impr Pant: Captura Escritorio  
Alt + Mayús + Impr Pant: Captura región rectangular  
Ctrl + Z: Deshacer  
Ctrl + Y: Rehacer  
Ctrl + P: Imprimir  
Ctrl + S: Guardar  
Ctrl + Q: Cerrar (Alt + F4)  
Ctrl + Alt +L: Bloquear sesión  
Ctrl + Alt +Supr: Menú de apagado  
Ctrl + Alt +T: Abrir Terminal

**Firefox:**

Ya vistos:  
Ctrl + T: Abrir nueva pestaña  
Ctrl + W: Cerrar pestaña  
Ctrl + Tab: Navega por las pestañas  
Ctrl + F: Buscar  
Ctrl + +: Aumentar tamaño  
Ctrl + -: Disminuir tamaño  
Ctrl + 0: Vista normal

F5: Recargar página  
Alt + F5: Recargar página reemplazando caché  
Barra espaciadora: avanzar página  
Mayús +Barra espaciadora: retroceder página  
F6 o Alt + D o Ctrl + L: Selecciona la barra de direcciones  
Alt + Enter: Duplica la pestaña (Primero F6)  
F11: Pantalla completa  
Ctrl + H: Panel de Historial  
Ctrl + Mayús + H: Catálogo de Historial  
Ctrl + Mayús + P: Nueva ventana privada  
Ctrl + D: Añadir página a marcadores  
Ctrl + Mayús + T: Deshacer cerrar pestaña  
Alt + 1 a 8: Navega por las pestañas 1 a 8  
Alt + 9: Va a la última pestaña  
Ctrl + AvPág: Avanza por las pestañas  
Ctrl + RePág: Retrocede por las pestañas  
Alt + Flechas izquierda/derecha: navega por el historial de la pestaña

Texto:  
Ctrl + A: Seleccionar todo  
Tab: Ir al próximo enlace  
Mayús + flechas arriba/abajo: Seleccionar líneas o renglones  
Mayús + flechas izquierda/derecha: Seleccionar letra a letra  
Ctrl +Mayús + flechas izquierda/derecha: Seleccionar palabra a palabra  
Ctrl + flechas izquierda/derecha: Moverse palabra a palabra  
Ctrl + Supr: Elimina palabra a palabra a la derecha (Ctrl Z)  
Ctrl + Retroceso: Elimina palabra a palabra a la izquierda  
Inicio: Moverse al principio de la línea/página sin edición  
Fin: Moverse al final de la línea/página sin edición  
Ctrl + Inicio: Moverse al principio de la página con edición  
Ctrl +Fin: Moverse al final de la página con edición

Reproducción multimedia  
Barra espaciadora: Pausar/reanudar  
Flechas arriba abajo: Subir bajar volumen  
M: Silenciar/dar volumen  
Flechas izquierda derecha: adelantar o retroceder reproducción

La **imagen utilizada en la portada, el hombre 3D** se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): [https://pixabay.com/images/id-1019782](https://pixabay.com/images/id-1019782)

Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

MegaEnx – Tomorrow: [https://soundcloud.com/megaenx/tomorrow](https://soundcloud.com/megaenx/tomorrow)  
Arhur Yul – Cinema Intro: [https://www.jamendo.com/track/1503848/](https://www.jamendo.com/track/1503848/)  
GarnaVutka – Corporate: [https://www.jamendo.com/track/1354699](https://www.jamendo.com/track/1354699)  
Zoran Sebic – Innovative Logo: [https://www.jamendo.com/track/1487845](https://www.jamendo.com/track/1487845)  
GarnaVutka – Crystal Wellcome Logo: [https://www.jamendo.com/track/1508030](https://www.jamendo.com/track/1508030)  
GarnaVutka – Upbeat Dance Nu Jazz Indie Pop (Lite version): [https://www.jamendo.com/track/1464611](https://www.jamendo.com/track/1464611)  
MBB – Clouds: [https://www.free-stock-music.com/mbb-clouds.html](https://www.free-stock-music.com/mbb-clouds.html)  
MBB – Brezee: [https://www.free-stock-music.com/mbb-breeze.html](https://www.free-stock-music.com/mbb-breeze.html)  
MBB – Feel good: [https://www.free-stock-music.com/mbb-feel-good.html](https://www.free-stock-music.com/mbb-feel-good.html)  
MBB – Ibiza: [https://www.free-stock-music.com/mbb-ibiza.html](https://www.free-stock-music.com/mbb-ibiza.html)  
MBB – Takeoff: [https://www.free-stock-music.com/mbb-takeoff.html](https://www.free-stock-music.com/mbb-takeoff.html)  
MBB – Good Vibes: [https://www.free-stock-music.com/mbb-good-vibes.html](https://www.free-stock-music.com/mbb-good-vibes.html)

**Bibliografía utilizada para realizar este episodio:**

[https://www.emezeta.com/articulos/como-trabajar-mas-rapido-el-metodo-geek](https://www.emezeta.com/articulos/como-trabajar-mas-rapido-el-metodo-geek)  
[https://www.emezeta.com/articulos/la-gran-guia-de-supervivencia-de-la-terminal-de-linux](https://www.emezeta.com/articulos/la-gran-guia-de-supervivencia-de-la-terminal-de-linux)  
[https://ayudalinux.com/mejores-atajos-teclado-linux/](https://ayudalinux.com/mejores-atajos-teclado-linux/)  
[https://openwebinars.net/blog/15-atajos-teclado-mas-utilizados-shell-linux/](https://openwebinars.net/blog/15-atajos-teclado-mas-utilizados-shell-linux/)  
[https://es.ccm.net/faq/2606-atajos-de-teclado-en-firefox](https://es.ccm.net/faq/2606-atajos-de-teclado-en-firefox)  
[https://blog.desdelinux.net/atajos-del-teclado-para-dolphin/](https://blog.desdelinux.net/atajos-del-teclado-para-dolphin/)  
[https://ubuntusp.com/questions/17936/teclas-de-vinculacion-acciones-en-kde-4](https://ubuntusp.com/questions/17936/teclas-de-vinculacion-acciones-en-kde-4)  
[https://airsynth.es/2015/03/atajos-de-teclado-que-suelo-usar/](https://airsynth.es/2015/03/atajos-de-teclado-que-suelo-usar/)  
[https://support.mozilla.org/es/kb/accesos-directos-realiza-rapidamente-tareas-comune](https://support.mozilla.org/es/kb/accesos-directos-realiza-rapidamente-tareas-comune)  

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1292890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
