---
title: "#65 Mesa Redonda: Pásate a GNU/Linux"
date: 2018-11-21
author: Juan Febles
categories: [podcastlinux]
img: PL/PL65.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL65
  image: https://podcastlinux.gitlab.io/images/PL/PL65.png
tags: [Mesa Redonda, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL65.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL65.mp3" type="audio/mpeg">
</audio>

 Muy buenas amante de GNU/Linux y el Software Libre!!!  
 Bienvenido a un nuevo episodio, el número 65, de Podcast Linux. Un saludo muy fuerte y cariñoso de quien te habla, Juan Febles.

 Hoy estrenamos nuevo formato, Mesa redonda. En este tipo de episodios traeré a expertos en una temática para dialogar y reflexionar sobre ella. Creo que será más enriquecedora el poder escuchar varias voces experimentadas y que entre ellas puedan aportar muchas ideas y experiencia en un mismo programa.

 Agradecer y dar la bienvenida a [RikyLinux](https://twitter.com/Rikylinux), [Yoyo Fernández](https://twitter.com/yoyo308) y [Elav](https://twitter.com/elavdeveloper).

 **Riky Linux:  
 **Twitter: <https://twitter.com/Rikylinux>  
 Web: <https://clubdesoftwarelibre.wordpress.com/>

 **Yoyo Fermández:  
 **Twitter:[ https://twitter.com/yoyo308](https://twitter.com/yoyo308)  
 Web: <https://salmorejogeek.com/>

 **Elav:  
 **Twitter: <https://twitter.com/elavdeveloper>  
 Web: <https://www.systeminside.net/>

 Recuerda que tenemos un **grupo de Telegram** para resolver dudas de instalaciones: <https://t.me/pasategnulinux>


 La **imagen utilizada en la portada **se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): <https://pixabay.com/photo-1019995/>

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 GarnaVutka – Background Ambient Full version: <http://jamen.do/t/1396695>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
