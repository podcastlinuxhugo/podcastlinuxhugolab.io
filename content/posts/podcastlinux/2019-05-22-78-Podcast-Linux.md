---
title: "#78 Especial Componentes Aliexpress"
date: 2019-05-22
author: Juan Febles
categories: [podcastlinux]
img: PL/PL78.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL78
  image: https://podcastlinux.gitlab.io/images/PL/PL78.png
tags: [Hardware, podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL78.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL78.mp3" type="audio/mpeg">
</audio>

 Muy buenas, oyente de Podcast Linux, mi nombre es Juan Febles y tenemos otro especial entre manos. Hoy quiero compartir mi experiencia de compra en AliExpress de componentes para el ordenador.  
 Ya estés en trayecto, haciendo deporte o tareas del hogar, paseando o en tu casa, te doy la bienvenida al episodio 78, Componentes AliExpress.

 **Enlaces de interés:**  

 Disco SSD: <https://es.aliexpress.com/item/SSD-SATA3-2-5inch-60GB-120G-240GB-480G-Hard-Drive-Disk-HD-HDD-factory-directly-KingDian/32645451815.html>  
 Memoria RAM: <https://es.aliexpress.com/item/4GB-2pcs-2GB-DDR3-PC3-10600U-1333Mhz-memory-for-AMD-mainboard-ONLY-Free-Shipping/32222954458.html>  
 Gráfica GTX 750: <https://es.aliexpress.com/item/Asus-GTX-750-FML-2GD5-GTX750-GTX-750-2G-D5-DDR5-128-poco-PC-de-escritorio/32864886963.html>  
 HardInfo: <http://hardinfo.org/>

 **Vídeos:**

 Unboxing: [https://youtu.be/VNpZ\_jsYqFA](https://youtu.be/VNpZ_jsYqFA)  
 Instalación: <https://youtu.be/bUxosPwI8HU>

 **Comandos para la terminal:**  
 Disco duro:  
 sudo hdparm -Tt /dev/sda  
 Memoria:  
 sudo dmidecode --type memory | less  
 memtester 100 5  
 Gráfica:  
 glxinfo -B

 **La imagen utilizada en la portada, se ha liberado desde [Pixabay](https://pixabay.com/) con licencia [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.es): **<https://pixabay.com/images/id-1026507/>  
 El logo de Aliexpress es propiedad de esta propia empresa.

 Toda la **música utilizada en este episodio** se distribuye bajo la licencia **Creative Commons**:

 MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
 GarnaVutka – Ambient Background Music (Full version) (2019) : <http://jamen.do/t/1641577>

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
