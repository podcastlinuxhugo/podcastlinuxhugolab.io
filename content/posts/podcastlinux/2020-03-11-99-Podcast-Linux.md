---
title: "#99 Linux Connexion con EsLibre"
date: 2020-03-11
author: Juan Febles
categories: [podcastlinux]
img: PL/PL99.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL99
  image: https://podcastlinux.gitlab.io/images/PL/PL99.png
tags: [Linux Connexion, podcastlinux]
comments: true
---
<audio controls>
   <source src="https://archive.org/download/podcast_linux/PL99.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL99.mp3" type="audio/mpeg">
</audio>

¡¡¡Muy buenas amante del Software Libre!!!  

Bienvenido a otra entrega, la número 99, de Podcast Linux. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy está con nosotros [Jesús González Barahona](https://gestion2.urjc.es/pdi/ver/jesus.gonzalez.barahona), uno de los coordinadores del evento [EsLibre](https://eslib.re/2020/). Trabaja como docente e investigador en la [Universidad Rey Juan Carlos](https://www.urjc.es/), y actualmente es coordinador también de [OfiLibre](https://ofilibre.gitlab.io/), la Oficina de Conocimiento y Cultura Libres de esta universidad.  

Recordar a los oyentes que estamos en una sala [Jitsi](https://meet.jit.si/) para esta charla, un servicio libre para videoconferencias, y agradecer a [Neodigit](https://neodigit.net/), nuestro proveedor de alojamiento y servicios de confianza de habla hispana para todo [Avpodcast](https://avpodcast.net/).  

No queda nada para el episodio 100. Será un preguntas y respuestas sobre Podcast Linux. Todo lo que quieras saber sobre este proyecto o de mí puedes enviarlo en forma de pregunta, ya sea por medio de audio o texto. Responderé a todas ellas en el episodio 100. Envía tus audios o textos a <podcastlinux@avpodcast.net> o a telegram [@juanfebles](https://t.me/juanfebles).  

Enlaces de interés:  

Web: <https://eslib.re/2020/>  
Repositorio Gitlab: <https://gitlab.com/eslibre>  
Mastodon: <https://hostux.social/@eslibre/>  
Matrix: <https://matrix.to/#/#esLibre:matrix.org>  
Grupo Telegram: <https://t.me/esLibre>  
Twitter: <https://twitter.com/esLibre_>  

Las imagen utilizada en la portada es el logo Eslibre, cedido por ellos y de su propiedad.


Toda la música utilizada en este episodio se distribuye bajo la licencia Creative Commons:  

MegaEnx – Tomorrow: <https://soundcloud.com/megaenx/tomorrow>  
Jay Someday – It´s Time: <https://www.free-stock-music.com/jay-someday-its-time.html>  


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1297890_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
