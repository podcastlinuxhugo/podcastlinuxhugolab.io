---
title: "#06 #ConLinuxSíSePuede"
date: 2016-09-01
author: Juan Febles
categories: [podcastlinux]
img: PL/PL06.png
podcast:
  audio: https://archive.org/download/podcast_linux/PL06
  image: https://podcastlinux.gitlab.io/images/PL/PL06.png
tags: [podcastlinux]
comments: true
---
<audio controls>
  <source src="https://archive.org/download/podcast_linux/PL06.ogg" type="audio/ogg">
  <source src="https://archive.org/download/podcast_linux/PL06.mp3" type="audio/mpeg">
</audio>

Muy buenas Linuxeros. Bienvenidos a otro episodio de Podcast Linux.  
 Mi nombre es Juan Febles y te invito a disfrutar de este proyecto que comparte el sistema operativo libre del pingüino a los usuarios domésticos de ordenadores.

 En el Núcleo Kernel reflexionaré sobre las posibilidades que ofrece GNU/Linux. Con Linux sí se puede.  
 Pasaremos al Gestor de Paquetes donde te hablaré del archiconocido reproductor multimedia VLC y algunas de sus características más interesantes.  
 El invitado de Comunidad Linux es Tanhausser (@tannhausser), blogero al mando de lamiradadelreplicante.com  
 Por último le daré un repaso a los mensajes que he recibido en estas semanas.


 Toda la música utilizada en este episodio se distribuye bajo la licencia libre Creative Commons:  
 Artistas:  
 LukHash  
 Cleric  
 Matti Paalanen  
 The Polish Ambassador  
 Chris Zabriskie  
 Chill Carrier  
 Michett  
 Rolemusic  
 BoxCat Games

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  


Somos un millar los que estamos en el canal de [Telegram](https://telegram.me/podcastlinux)  donde podrás enterarte al momento qué estoy haciendo y qué planifico para los siguientes programas. Únete en [t.me/podcastlinux](https://t.me/podcastlinux)  
También tenemos un grupo para solventar problemas de instalación en GNU/Linux: [t.me/pasategnulinux](https://t.me/pasategnulinux)  

Pásate si quieres también por el canal de [Youtube](https://www.youtube.com/podcastlinux) para visualizar mis vídeos.  

Me encantan los comentarios que me dejas en los episodios. Te invito a que compartas tus experiencias y opiniones allí.   Respondo a todas ellas.  
No te olvides suscribirte en [Ivoox](http://www.ivoox.com/podcast-podcast-linux_sq_f1285850_1.html), [Itunes](https://itunes.apple.com/es/podcast/podcast-linux/id1130775643?mt=2) y [Spotify](https://open.spotify.com/show/2OBMJtMRmGGc6yVnZNuTX6) para no perderte ninguno de mis episodios.  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
