---
title: "#26 Linux Express"
date: 2017-10-18
author: Juan Febles
categories: [linuxexpress]
img: 2017/26LinuxExpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/%2326%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Otro Linux Express más para los que se les hace larga la espera quincenal de Podcast Linux.  

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2326%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #36 Linux Connexion con Atareao](http://avpodcast.net/podcastlinux/atareao).
+ Próximo episodio Cultura Libre.
+ [Maratón Linuxero 1.1](https://maratonlinuxero.org/)
+ [Día del Podcast](http://diadelpodcast.com/)
+ Curso: Crea tu propio podcast libre: [Archive.org](https://archive.org/details/@podcast_linux) y [Youtube](https://www.youtube.com/playlist?list=PLdt4gHTaH61HOOLsyAc2xYzdbinem9ooZ)
+ Nuevo [Blog de PodcastLinux](https://podcastlinux.gitlab.io/) gracias a [Hefistion_](https://twitter.com/Hefistion_)

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
