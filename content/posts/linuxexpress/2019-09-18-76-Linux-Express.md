---
title: "#76 Linux Express"
date: 2019-09-18
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/76linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/76linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/76linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#86 Redes Sociales Libres](https://avpodcast.net/podcastlinux/redessocialeslibres)
+ Próximo episodio: Linux Connexion con[Andrián Perales](https://adrianperales.com/)
+ Siguiente curso: [Hugo](https://gohugo.io/) en [Gitlab](https://medium.com/@loadbalancing/building-a-blog-website-1-quick-start-with-hugo-and-gitlab-888b9faa176f)
+ Sigo con [Git](https://alejandrojs.wordpress.com/2017/06/01/como-empezar-a-usar-git-con-gitlab/) y [Vim](https://github.com/AnderRasoVazquez/curso-de-vim)
+ Curso [Maker School](https://t.me/kreitek_anuncios/201) de [Kreitek](https://kreitek.org/) primera parte con [Arduino](https://www.arduino.cc/)
+ [Pine64](https://www.pine64.org/) tiene un proyecto de un [reloj inteligente libre](https://twitter.com/thepine64/status/1172648370550136832)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  	
