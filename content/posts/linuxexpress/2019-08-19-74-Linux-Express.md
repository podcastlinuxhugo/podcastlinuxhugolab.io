---
title: "#74 Linux Express"
date: 2019-08-19
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/74linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/74linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/74linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#84 10 preguntas incómodas a Slimbook](https://avpodcast.net/podcastlinux/preguntasslimbook)
+ Próximo episodio: Linux Connexion con [Paco Estrada](https://compilando.audio)
+ [Blog personal](https://juanfebles.gitlab.io) creado con  [Hugo](https://gohugo.io/) como generador web en [Gitlab](https://medium.com/@loadbalancing/building-a-blog-website-1-quick-start-with-hugo-and-gitlab-888b9faa176f)
+ Utilizando [Git](https://alejandrojs.wordpress.com/2017/06/01/como-empezar-a-usar-git-con-gitlab/) y [Vim](https://github.com/AnderRasoVazquez/curso-de-vim) en la terminal.
+ Curso de [Introducción a la producción y realización radiofónica](https://teatenerife.es/actividad/introduccion-a-la-produccion-y-realizacion-radiofonica/1972)
+ Territorio f-Droid: [SGit](https://f-droid.org/es/packages/me.sheimi.sgit/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
