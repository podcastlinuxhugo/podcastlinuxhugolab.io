---
title: "#41 Linux Express"
date: 2018-05-16
author: Juan Febles
categories: [linuxexpress]
img: 2018/41linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/41linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Aquí tienes otro Linux Express, los audios de Telegram que se alternan con Podcast Linux para que sepas lo que se cuece para próximos programas.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/41linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/41linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es a lo que le he estado dando vueltas en estas 2 semanas:  
+ [Episodio #51 Chromebook](https://avpodcast.net/podcastlinux/chromebook/)
+ Siguiente episodio, Linux Connexion con [MosqueteroWeb](https://twitter.com/mosqueteroweb)
+ Chromebook con aplicaciones GNU/Linux
+ Utilizando de nuevo mi Lenovo Thinkpad X220
+ [SSD chino](https://www.gearbest.com/hdd-ssd/pp_1656577.html) para el YEPO 737A
+ Aplicaciones libres para Android en [F-Droid](https://f-droid.org/)
+ [Curso Shotcut](https://podcastlinux.com/shotcut)


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-1155173/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
