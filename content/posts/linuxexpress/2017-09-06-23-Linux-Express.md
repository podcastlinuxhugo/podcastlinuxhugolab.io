---
title: "#23 Linux Express"
date: 2017-09-06
categories: [linuxexpress]
author: Juan Febles
img: 2017/23LinuxExpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/%2323%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
Llega septiembre y entramos en la rutina, tan necesaria a veces para mantaner los buenos hábitos.  

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2323%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #33 1º Directo Podcast Linux](http://avpodcast.net/podcastlinux/directo) <https://youtu.be/tItznvHFKFo>
+ Próximo episodio [Directo Podcast Linux en el Maratón Linuxero](https://youtu.be/Yv90j2HVg1Q)
+ Siguiente episodio será sobre formatos libres.
+ Toda la info sobre Maratón Linuxero: <https://maratonlinuxero.org> Telegram: <https://t.me/maratonlinuxero>
+ Idea de realizar otro directo mediante Mixxx y Icecast.

Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
