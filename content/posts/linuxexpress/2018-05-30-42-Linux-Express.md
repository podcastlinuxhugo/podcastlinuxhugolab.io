---
title: "#42 Linux Express"
date: 2018-05-30
author: Juan Febles
categories: [linuxexpress]
img: 2018/42linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/42linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Aquí tienes otro Linux Express, los audios de Telegram que se alternan con Podcast Linux para que sepas lo que se cuece para próximos programas.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/42linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/42linuxexpress.mp3" type="audio/mpeg">
</audio>

Te dejo el cacharreo e información al que he estado dándole vueltas en esta quincena:  
+ [Episodio #52 Chromebook](https://avpodcast.net/podcastlinux/mosqueteroweb)
+ Siguiente episodio, Libera tu móvil
+ Aportaciones de dispositivos de los oyentes
+ [Linux Center](https://linuxcenter.es/) y [GNU/Linux Valencia](https://www.meetup.com/es/gnu-linux-valencia/)
+ Proyectos para el verano
+ [Curso Edición de vídeo con Shotcut](https://podcastlinux.com/shotcut)
+ Pŕoximo [Maratón Linuxero](https://maratonlinuxero.org): 21 de julio

La imagen utilizada es una fotografía propia de un [Glinet AR-150](https://www.gl-inet.com/ar150/), dispositivo cedido por [VíctorSnk](https://twitter.com/victorsnk)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
