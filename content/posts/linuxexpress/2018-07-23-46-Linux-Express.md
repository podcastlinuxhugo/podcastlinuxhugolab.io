---
title: "#46 Linux Express"
date: 2018-07-23
author: Juan Febles
categories: [linuxexpress]
img: 2018/46linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/46linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Aquí comparto todo lo vivido esta quincena entre Linux Express y Linux Express.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/46linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/46linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ [Episodio #56 Linux Connexion con Linux Center](https://avpodcast.net/podcastlinux/linuxcenter)
+ Próximo epiosdio: Especial [TLP2018](https://tlp-tenerife.com/)
+ Emisiones en Directo gracias a [Icecast](https://www.icecast.org/) y [Gitlab](http://podcastlinux.com/directo/)
+ Entrevistas en [Muylinux](https://www.muylinux.com/2018/07/16/entrevista-Juan Febles-febles-linux-podcast-paco-estrada-compilando-podcast-yoyo-fernandez-salmorejo-geek/), [Devisionarios](http://devisionarios.com/), [La razón de la voz](https://larazondelavoz.gitlab.io/pod16Juan Febles/) y [Descargas de mi mente](https://www.ivoox.com/1-podcasts-sus-podcasters-audios-mp3_rf_26919664_1.html).
+ Territorio f-Droid: [Audio Recorder](https://f-droid.org/en/packages/de.danoeh.antennapod/)
+ [Maraton Linuxero 1 de septiembre](https://maratonlinuxero.org)
+ [Jpod18](http://jpod.es/) y [Premios Asociación Podcast](http://premios.asociacionpodcast.es/)

La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-1019864/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
