---
title: "#32 Linux Express"
date: 2018-01-10
author: Juan Febles
categories: [linuxexpress]
img: 2018/32linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/32linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Primer Linux Express del año con una cuantas cositas que contar.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/32linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/32linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en estas 2 semanas:  
+ [Episodio #42 Linux Connexion con Álvaro Nova](http://avpodcast.net/podcastlinux/alvaronova)
+ Próximo episodio, Especial [UtraMoove de Vant](http://www.vantpc.es/producto/ultramoove)
+ Varios screencast en [Youtube](ttps://www.youtube.com/PodcastLinux) y [Archive.org] (https://archive.org/details/@podcast_linux)
+ Sorteo Proel DM581USB realizado: <https://twitter.com/podcastlinux/status/949692246663757826>
+ Todo el curso de Podcasting en el blog: <https://podcastlinux.gitlab.io/cursopodcasting>
+ Ya somos 2.000 en [Twitter](https://twitter.com/podcastlinux) y 600 en el [canal de Telegram](https://t.me/podcastlinux)

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
