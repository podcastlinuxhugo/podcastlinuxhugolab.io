---
title: "#61 Linux Express"
date: 2019-02-20
author: Juan Febles
categories: [linuxexpress]
img: 2019/61linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/61linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/61linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/61linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#71 Linux Connexion con Victorhck](https://avpodcast.net/podcastlinux/Victorhck)
+ Próximo episodio: Especial [Vant MiniMOOVE](https://www.vantpc.es/producto/minimoove)
+ Elige los próximos episodios conmigo
+ [#HiloSoftwareLibre](https://twitter.com/hashtag/HiloSoftwareLibre) en [Twitter](https://twitter.com/podcastlinux) 
+ [Veyon](https://twitter.com/podcastlinux/status/1096659518480699392): Gestiona tu aula de informática
+ Colaboración en [@EntrevistaEnDiferido](https://t.me/entrevistaendiferido)
+ Territorio f-Droid: [OpenScale](https://f-droid.org/en/packages/com.health.openscale)
+ Anímate a colaborar en tu [FLISol 19](https://flisol.info/FLISOL2019)



Las imagen utilizada es propiedad de [Vant](https://www.vantpc.es) .


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
