---
title: "#85 Linux Express"
date: 2020-01-22
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/85linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/85linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/85linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #95 [Fotografía y GNU/Linux](https://avpodcast.net/podcastlinux/fotografia)
+ Próximo episodio:Linux Connexion con [Álvaro Nova](https://alvaronova.com/index.es.html)
+ Vídeoconferencia con [Jitsi](https://meet.jit.si/) mediante llamada telefónica
+ Imágenes libres con [Unsplash](https://unsplash.com/)
+ Conociendo más a [Marco Lachmann-Anke](https://pixabay.com/users/peggy_marco-1553824/)
+ Código del Proyecto [ESP8266 Reloj - clima IOT](https://gitlab.com/podcastlinux/reloj-esp8266) 
+ Eventos podcasting: [Jpod20](http://jpod.es/) y [MaratonPod20](https://www.maratonpod.es/)
+ Gracias por los mensajes sobre [Kira](https://avpodcast.net/wp-content/uploads/2020/01/IMG_20190530_100238.jpg)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
