---
title: "#73 Linux Express"
date: 2019-08-05
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/73linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/73linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/73linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:
+ Episodio [#83 Especial #TLP2019](https://avpodcast.net/podcastlinux/tlp2019)
+ Próximo episodio: 10 Preguntas incómodas a [Slimbook](https://slimbook.es)
+ Retomando de nuevo [Wifiteca](https://twitter.com/wifiteca)
+ Probando [Hugo](https://gohugo.io/) como generador web en [Gitlab](https://medium.com/@loadbalancing/building-a-blog-website-1-quick-start-with-hugo-and-gitlab-888b9faa176f)
+ Utilizando [Git](https://alejandrojs.wordpress.com/2017/06/01/como-empezar-a-usar-git-con-gitlab/) en la terminal
+ Probando [Sox](http://sox.sourceforge.net) gracias a [Jorge Lama](https://twitter.com/raivenra)
+ Fondo productivo de [Audacity](https://www.audacityteam.org/) a raíz de los de [Atareao](https://www.atareao.es/podcast/fondos-de-pantalla-productivos/)
+ Perdí mis Mensajes Guardados de [Telegram](https://twitter.com/podcastlinux/status/1157245288882561024)
+ Territorio f-Droid: [Oversec](https://f-droid.org/packages/io.oversec.one/) gracias a [Jorge Moreno](https://twitter.com/2jmoreno)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
