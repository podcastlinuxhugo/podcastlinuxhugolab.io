---
title: "#93 Linux Express"
date: 2020-05-13
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/93linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/93linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/93linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #103 [Directo Especial Confinamiento](https://avpodcast.net/podcastlinux/directoconfinamiento)
+ Siguiente episodio: Linux Connexion con [VoroMV](https://twitter.com/VOROMV)
+ Emitir vídeo con [Simple Screen Recorder](https://www.maartenbaert.be/simplescreenrecorder/live-streaming)
+ Haciendo ojitos a [Ubuntu Studio](https://ubuntustudio.org)
+ Más [LMMS](https://archive.org/details/podcast-linux-intro-sound-theme)
+ 3 años con [Slimbook One](https://slimbook.es/one)
+ Torre AMD.
+ Nuevo portátil [Vant Edge](https://twitter.com/vantpc/status/1256510319268106241)
+ [InkScape 1.0](https://inkscape.org/release/inkscape-1.0)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
