---
title: "#68 Linux Express"
date: 2019-05-29
author: Juan Febles
categories: [linuxexpress]
img: 2019/68linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/68linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/68linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/68linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#78 Especial Componentes AliExpress](https://avpodcast.net/podcastlinux/aliexpress)
+ Próximo episodio: **Especial Tableta Wacom Intuos BT**
+ Próximos ordenadores a probar
+ 3 semanas con [SlimbookBattery](https://twitter.com/podcastlinux)
+ Curso Atareao [Scripts en Bash](https://www.atareao.es/tutorial/scripts-en-bash/)
+ Backup con [Kup](https://store.kde.org/p/1127689/show/page/2) en KDE Neon
+ Cierra [Antergos](https://antergos.com/blog/antergos-linux-project-ends/)
+ Summer is coming: Especial 3º Aniversario
+ Territorio f-Droid: [FediLab](https://f-droid.org/es/packages/fr.gouv.etalab.mastodon/)


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-2364348/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
