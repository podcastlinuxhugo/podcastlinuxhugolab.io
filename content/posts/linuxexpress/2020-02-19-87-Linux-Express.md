---
title: "#87 Linux Express"
date: 2020-02-19
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/87linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/87linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/87linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #97 [Vídeo y GNU/Linux](https://avpodcast.net/podcastlinux/video)
+ Próximo episodio:Volvemos con Álvaro Nova
+ Nuevo [Kde Plasma 5.18](https://kde.org/announcements/plasma-5.18.0.php?site_locale=es)
+ Nueva adquisición: [Amazfit GTS](https://es.amazfit.com/gts.html)
+ Avances de Pine64 en sus dispositivos libres: [PinePhone, PineWatch y PineTab](https://www.pine64.org/2020/02/15/february-update-post-cny-and-fosdem-status-report/)
+ Curso de Arduino para centros: [Kit básico](https://hackmd.io/@podcastlinux/HyaG97oqB) + [Formato del curso](https://hackmd.io/@podcastlinux/Bk9Agfw7U)
+ Taller de [Podcasting](https://twitter.com/podcastlinux/status/1225762537150566401) en mi cole
+ Preguntas y respuestas para el episodio 100

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
