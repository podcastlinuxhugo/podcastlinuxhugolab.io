---
title: "#83 Linux Express"
date: 2019-12-25
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/83linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/83linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/83linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio #93[Productividad en GNU/Linux 2](https://avpodcast.net/podcastlinux/productividad2)
+ Próximo episodio: Linux Connexion con Atareao
+ [PAMDevice](https://www.atareao.es/podcast/desbloquear-ubuntu-con-tu-movil/) de Atareao
+ Buscando [material didáctico](https://www.elcableamarillo.cc/) para Arduino.
+ En mis manos tengo las [ESP8266](https://twitter.com/podcastlinux/status/1208728497734246400)
+ [KDE Plasma](https://www.forbes.com/sites/jasonevangelho/2019/10/23/bold-prediction-kde-will-steal-the-lightweight-linux-desktop-crown-in-2020/) más liviano que XFCE
+ Somos cerca de [5.000](https://twitter.com/podcastlinux) en Twitter

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
