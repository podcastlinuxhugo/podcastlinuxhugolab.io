---
title: "#72 Linux Express"
date: 2019-07-22
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/72linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/72linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/72linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#82 Hardware Slimbook Zero](https://avpodcast.net/podcastlinux/slimbookzero)
+ Próximo episodio: Especial [#TLP2019](https://tlp-tenerife.com/)
+ Vaya lío con los sorteos en Twitter.
+ Compra [Tascam DR-40](https://www.tascam.eu/en/dr-40x.html) y [Boya M1](http://www.boya-mic.com/lavaliermicrophones/BY-M1.html)
+ Síntesis de voz libre con [ESpeak](http://espeak.sourceforge.net/)
+ Probando [VLC](https://www.videolan.org/vlc/) en mi AndroidTV
+ Territorio f-Droid: [AndStatus](https://f-droid.org/en/packages/org.andstatus.app/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
