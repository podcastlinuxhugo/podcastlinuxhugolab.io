---
title: "#95 Linux Express"
date: 2020-06-10
author: Juan Febles
categories: [linuxexpress]
img: linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/95linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/95linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/95linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #105 [Linux Connexion con Gabriel Viso](https://avpodcast.net/podcastlinux/gabrielviso)
+ Siguiente episodio: Hardware [Vant Edge](https://www.vantpc.es/edge)
+ Cierra [Avpodcast](https://avpodcast.net/)
+ Nueva web [Podcast Linux](https://podcastlinux.com/) con [feed híbrido](https://podcastlinux.com/feed) y hospedaje en [Gitlab](https://gitlab.com/podcastlinux/podcastlinux.gitlab.io) y [Archive.org](https://archive.org/details/podcast_linux)
+ Próximamente cambio la web a [Hugo](https://gitlab.com/podcastlinux/hugopodcast)
+ [Linux Connexions](https://podcastlinux.com/tag/Linux%20Connexion/) para verano.


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
