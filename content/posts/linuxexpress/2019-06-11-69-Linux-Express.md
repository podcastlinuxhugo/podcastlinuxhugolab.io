---
title: "#69 Linux Express"
date: 2019-06-11
author: Juan Febles
categories: [linuxexpress]
img: 2019/69linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/69linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/69linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/69linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#79 Especial Tableta Wacom Intuos](https://avpodcast.net/podcastlinux/wacom)
+ Próximo episodio: [Especial Vant Life A](https://www.vantpc.es/life-a)
+ Más dispositivos por llegar
+ Probando crear un podcast sólo desde [Archive.org](https://archive.org/)
+ Slimbook renueva su serie [Pro](https://slimbook.es/prox)
+ [Pinephone](https://www.pine64.org/pinephone/) sigue avanzando
+ Summer is coming: Especial 3º Aniversario
+ Territorio f-Droid: [LabCoat](https://f-droid.org/zh_Hans/packages/com.commit451.gitlab/)


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-1013622/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
