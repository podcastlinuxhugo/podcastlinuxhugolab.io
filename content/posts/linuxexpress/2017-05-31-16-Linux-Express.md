---
title: "#16 Linux Express"
author: Juan Febles
date: 2017-05-31
categories: [linuxexpress]
img: 2017/16LinuxExpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/%2316%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
Otro Linux Express para que estés informado del día a día de Podcast Linux.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2316%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Hoy quiero compartir contigo estos temas:

+ [Episodio #25 GNU/Linux y NAS](http://avpodcast.net/podcastlinux/nas)
+ Próximo episodio con [Ugeek](https://ugeek.github.io/)
+ [Vídeo Slimbook One](https://youtu.be/R5AoSOg84AA)
+ [#MiEscritorioLinux](https://twitter.com/hashtag/MiEscritorioLinux)
+ Podcast Linux cumple un año el 1 de julio

Aquí tienes el vídeo de [Slimbook One](https://slimbook.es/one-minipc-potente):
https://www.youtube.com/watch?v=R5AoSOg84AA


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.gitlab.io/Linux-Express/feed.xml>
