---
title: "#50 Linux Express"
date: 2018-09-19
author: Juan Febles
categories: [linuxexpress]
img: 2018/50linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/50linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Atención, tenemos nueva empresa colaboradora, [Chicles WUG](https://wugum.com/).

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/50linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/50linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#60 Software Libre](https://avpodcast.net/podcastlinux/softwarelibre)
+ Próximo episodio: Linux Connexion con Rubén Rodríguez
+ [Camisetas](https://gitlab.com/podcastlinux/camiseta) y [pegatinas](http://waterdrop.ga) Podcast Linux
+ Presentación [Slimbook Kimera](https://slimbook.es/kymera-aqua-el-ordenador-gnu-linux-de-refrigeracion-liquida-custom) en la [Open Source Madrid](http://hackmadrid.org/actividades.html#)
+ Empresas colaboradoras: [Chicles WUG](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga)
+ Llegamos a los 3.000 seguidores en [Twitter](https://twitter.com/podcastlinux)
+ Territorio f-Droid: [KDE Connect](https://f-droid.org/packages/org.kde.kdeconnect_tp/)
+ Finalista en la categoría Tecnología en los [Premios Asociación Podcast](http://premios.asociacionpodcast.es/listado-finalistas-ix-edicion/)

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [https://pixabay.com/photo-1019949/). El logo y productos WUG son propiedad de esta empresa.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
