---
title: "#27 Linux Express"
date: 2017-11-01
author: Juan Febles
categories: [linuxexpress]
img: 2017/27linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/27linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Otro Linux Express más para los que se les hace larga la espera quincenal de Podcast Linux.  

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/27linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/27linuxexpress.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #37 Cultura Libre](http://avpodcast.net/podcastlinux/culturalibre).
+ Próximo episodio Linux Connexion con Wikimedia España
+ [Jpod17](https://jpod.es/) y [8º Premios Asociación Podcast](http://premios.asociacionpodcast.es/)
+ Curso: Crea tu propio podcast libre: [Archive.org](https://archive.org/details/@podcast_linux) y [Youtube](https://www.youtube.com/playlist?list=PLdt4gHTaH61HOOLsyAc2xYzdbinem9ooZ)
+ Probando un feed híbrido: [Feed Linux Express](https://podcastlinux.gitlab.io/Linux-Express/feed.xml)
+ Nuevo proyecto [Libre Sessions](https://archive.org/details/@libresessions)

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
