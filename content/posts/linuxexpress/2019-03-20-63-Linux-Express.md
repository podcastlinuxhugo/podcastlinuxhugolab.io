---
title: "#63 Linux Express"
date: 2019-03-20
author: Juan Febles
categories: [linuxexpress]
img: 2019/63linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/63linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/63linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/63linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#73 Audio y GNU/Linux](https://avpodcast.net/podcastlinux/audio)
+ Próximo episodio: Linux Connexion con [JoséGDF](https://www.josegdf.net/)
+ Proyecto: Montar PC reciclado
+ En breve, nuevos dispositivos a analizar
+ [Bot Telegram en tu Terminal (Ugeek)](https://ugeek.github.io/blog/post/2019-03-14-crea-un-bot-de-telegram-con-bash-y-una-sola-linea-de-terminal.html)
+ [Curso Emacs y OrgMode (Ugeek)](https://ugeek.github.io/blog/post/2019-03-15-cursos-webs-grupos-tutoriales-todo-sobre-emacs-y-orgmode.html)
+ [Audacity 2.3.1](https://www.audacityteam.org/audacity-2-3-1-released/)
+ Territorio f-Droid: [KODI Android](https://f-droid.org/es/packages/org.xbmc.kodi/)
+ [FLISol 19](https://flisol.info/FLISOL2019) Tenerife


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-1825685/).

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
