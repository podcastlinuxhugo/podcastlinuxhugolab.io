---
title: "#47 Linux Express"
date: 2018-08-06
author: Juan Febles
categories: [linuxexpress]
img: 2018/47linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/47linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
El verano me deja el suficiente tiempo libre para trastear e indagar.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/47linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/47linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ [Episodio #57 Especial TLP2018](https://avpodcast.net/podcastlinux/tlp2018)
+ Próximo episodio: Linux Connexion con [Dabo](https://daboblog.com/)
+ Comparte tu escritorio con [#EscritorioGNULinux](https://twitter.com/hashtag/escritoriognulinux)
+ [Odroid Go](https://www.hardkernel.com/main/products/prdt_info.php?g_code=G152875062626): Consola portátil retrogaming
+ Nuevas [voces americanas y europeas](https://twitter.com/podcastlinux/status/1025021929739169794) para la 3ª Temporada 
+ Territorio f-Droid: [Maps](https://f-droid.org/en/packages/com.github.axet.maps/)
+ [Maraton Linuxero 1 de septiembre](https://maratonlinuxero.org)
+ Inscritos en los Premios [Asociacion Podcast](http://premios.asociacionpodcast.es/2018_inscritos/)

Las imágenes utilizadas están bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-295252/) y [Pixabay](https://pixabay.com/photo-42936/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
