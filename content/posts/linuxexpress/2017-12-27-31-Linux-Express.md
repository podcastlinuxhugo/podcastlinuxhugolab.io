---
title: "#31 Linux Express"
date: 2017-12-27
author: Juan Febles
categories: [linuxexpress]
img: 2017/31linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/31linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Éste es el último Linux Express del año. Valoro el 2017.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/31linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/31linuxexpress.mp3" type="audio/mpeg">
</audio>

Vamos a repasar el 2017:  
+ [Episodio #41 Gaming y GNU/Linux](http://avpodcast.net/podcastlinux/gaming)
+ Próximo episodio Linux Connexion con Álvaro Nova
+ Primeras sensaciones con [UltraMoove de Vant](http://www.vantpc.es/producto/ultramoove) 
+ Un repaso al 2017
    + [Maratón Linuxero](https://maratonlinuxero.org/)
    + [Killall Radio](http://killallradio.com/)
    + [Blog Gitlab Podcast Linux](http://podcastlinux.gitlab.io/)
    + [Curso podcasting](https://archive.org/details/@podcast_linux)
    + Dispositivos probados: [Slimbook Katana](https://slimbook.es/ultrabook-katana), [Slimbook One](https://slimbook.es/one-minipc-potente) y [Vant UltraMoove](http://www.vantpc.es/producto/ultramoove)
+ Ranking personal de Software, Hardware y proyectos linuxeros en el 2017

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
