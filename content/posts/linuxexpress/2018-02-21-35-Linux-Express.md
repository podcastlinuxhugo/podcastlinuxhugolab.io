---
title: "#35 Linux Express"
date: 2018-02-21
author: Juan Febles
categories: [linuxexpress]
img: 2018/35linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/35linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Un nuevo Linux Express, los audios de Telegram que se intercalan con los formales.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/35linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/35linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en esta quincena:  
+ [Episodio #45 Distros Ligeras](http://avpodcast.net/podcastlinux/distrosligeras)
+ Siguiente episodio, Linux Connexion con [GALPon](https://www.galpon.org/).
+ YEPO 737A.
+ Nueva cuenta en [Mastodon](https://mastodon.social/@podcastlinux/)
+ KDE Neon vs. Linux Mint
+ Anímate a compartir tu [#EscritorioGNULinux](https://twitter.com/hashtag/escritoriognulinux)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
