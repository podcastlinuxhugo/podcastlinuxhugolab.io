---
title: "#29 Linux Express"
date: 2017-11-29
author: Juan Febles
categories: [linuxexpress]
img: 2017/29linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/29linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Todos los miércoles, entrega de un podcast, ya sea Linux Express o Podcast Linux. 

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/29linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/29linuxexpress.mp3" type="audio/mpeg">
</audio>

Seguimos dándole al coco y probando cosas para el podcast: 

+ [Episodio #39 GNU/Linux y Móviles](http://avpodcast.net/podcastlinux/moviles).
+ Próximo episodio Linux Connexion con Aleix Pol
+ Utiliza apk libres en tu Android: [Firefox](https://www.mozilla.org/es-ES/firefox/mobile/), [AntennaPod](https://f-droid.org/packages/de.danoeh.antennapod/) y [F-Droid](https://f-droid.org/)
+ [Calendario GNU/Linux](https://pad.disroot.org/p/Calendario_GNU-Linux#)
+ [Linux Mint Cinammon 18.3 Sylvia](https://linuxmint.com/download.php)
+ Próximo [Maratón Linuxero 3D](https://maratonlinuxero.org/)

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
