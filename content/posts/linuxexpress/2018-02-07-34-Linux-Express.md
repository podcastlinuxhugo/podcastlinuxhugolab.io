---
title: "#34 Linux Express"
date: 2018-02-07
author: Juan Febles
categories: [linuxexpress]
img: 2018/34linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/34linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Un nuevo Linux Express, los audios de Telegram que se intercalan con los formales.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/34linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/34linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en esta quincena:  
+ [Episodio #44 Linux Connexion con Vant](http://avpodcast.net/podcastlinux/vant)
+ Ganadores sorteo 5 productos packs [Vant](https://twren.ch/check#953506421555056641)
+ Siguiente episodio, Distros ligeras.
+ Cacharreando con un ChromeBook.
+ Probando [OpenWrt](https://openwrt.org/)
+ Próximo [Maratón Linuxero](https://maratonlinuxero.org/)
+ Ya tenemos [Jpod18: Madrid](http://www.asociacionpodcast.es/2018/02/01/la-jornadas-de-podcasting-de-2018-seran-en-madrid/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
