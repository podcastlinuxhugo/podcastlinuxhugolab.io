---
title: "#65 Linux Express"
date: 2019-04-17
author: Juan Febles
categories: [linuxexpress]
img: 2019/65linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/65linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/65linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/65linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#75 Especial PC Reciclado](https://avpodcast.net/podcastlinux/pcreciclado)
+ Próximo episodio: Especial [FLISOL 2019](https://flisol.info/)
+ Componentes para PC Reciclado
+ Mejorar otros PC que tengo
+ Visor [Nomacs](https://nomacs.org/) para realizar fotomosaicos
+ Territorio f-Droid: [Etar](https://f-droid.org/es/packages/ws.xsoh.etar/)
+ [Maratón Linuxero](https://maratonlinuxero.org/) sábado 20 de abril
+ No queda nada para [FLISol 19 Tenerife](https://flisol.info/FLISOL2019/Espana/Tenerife)


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-3071754/).
El nombre y logotipo FLISoL es propiedad de [FLISol](https://flisol.info)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
