---
title: "#19 Linux Express"
author: Juan Febles
date: 2017-07-12
categories: [linuxexpress]
img: 2017/19LinuxExpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/%2319%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
Otro Linux Express más para te llegue toda la información de lo que se cuece en Podcast Linux.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2319%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido y el verano refrescante que nos espera:

+ [Episodio Aniversario](http://avpodcast.net/podcastlinux/aniversario) y [Linux Connexion con Alejandro López 2](http://avpodcast.net/podcastlinux/one)
+ [Akademy y AkademyES](https://www.kde-espana.org/publicado-el-programa-de-charlas-de-akademy-es-2017)
+ Próximos episodios y organización en verano
+ Charla Educación y software Libre en la [Tenerife Lan Party](https://osl.ull.es/eventos/colabora-en-tlp-tenerife-2017-con-la-ull/)
+ [Maratón Linuxero](https://maratonlinuxero.org/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
