---
title: "#67 Linux Express"
date: 2019-05-15
author: Juan Febles
categories: [linuxexpress]
img: 2019/67linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/67linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/67linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/67linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#77 Especial FLISOL Tenerife](https://avpodcast.net/podcastlinux/flisoltenerife/)
+ Próximo episodio: Especial Componentes AliExpress
+ Nuevos dispositivos a probar
+ Ya somos 4.000 en [Twitter](https://twitter.com/podcastlinux)
+ Servicio envío de Firefox: <https://send.firefox.com/>
+ Utilizando Google Drive con [Kio-GDrive](https://community.kde.org/KIO_GDrive) y [GDrive](https://github.com/gdrive-org/gdrive)
+ Territorio f-Droid: [Diario](https://f-droid.org/en/packages/org.billthefarmer.diary/)


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-2999580/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
