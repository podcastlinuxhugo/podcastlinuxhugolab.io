---
title: "#71 Linux Express"
date: 2019-07-08
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/71linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/71linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/71linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#81 Especial 3º Aniversario](https://avpodcast.net/podcastlinux/3aniversario)
+ Próximo episodio: Hardware [Slimbook Zero](https://slimbook.es/pedidos/slimbook-zero/zero-comprar)
+ Cambios en Linux Express
+ Subiendo archivos a [Archive.org](https://archive.org/services/docs/api/internetarchive/cli.html#) desde la terminal
+ Utiliza [R-Clone](https://ugeek.github.io/blog/post/2019-07-03-dale-almacenamiento-ilimitado-a-tu-raspberry-servidor-o-pc-con-rclone.html)
+ Gestiona tus contraseñas con [Firefox Lockwise](https://lockwise.firefox.com/)
+ Somos 900 en el canal de Telegram: <https://t.me/podcastlinux>
+ Territorio f-Droid: [Termux](https://f-droid.org/en/packages/com.termux/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
