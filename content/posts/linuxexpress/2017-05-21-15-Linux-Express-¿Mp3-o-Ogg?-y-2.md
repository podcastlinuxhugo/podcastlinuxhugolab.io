---
title: "#15 Linux Express ¿Mp3 o Ogg? y 2"
author: Juan Febles
date: 2017-05-21
categories: [linuxexpress]
img: 2017/15LinuxExpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/%2315%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
Mi gozo en un pozo. Fin del experimento. El Mp3 está vivo, muy vivo. Escucha mis conclusiones sobre el uso del ogg en el podcasting.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2315%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Si no pudiste escuchar el anterior episodio por estar codificado en ogg, aquí lo tienes en mp3.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2314%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.gitlab.io/Linux-Express/feed.xml>
