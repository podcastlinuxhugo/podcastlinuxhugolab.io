---
title: "#88 Linux Express"
date: 2020-03-04
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/88linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/88linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/88linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #98 [Linux Connexion con Álvaro Nova](https://avpodcast.net/podcastlinux/alvaronova3)
+ Sigiente episodio: Linux Connexion con [EsLibre](https://eslib.re/2020/)
+ Nueva versión de [Shotcut](https://www.shotcut.org/blog/new-release-200217/)
+ [Twitter](https://twitter.com/podcastlinux) o [Mastodon](https://mastodon.social/@podcastlinux)
+ La [FSF](https://www.fsf.org/blogs/sysadmin/coming-soon-a-new-site-for-fully-free-collaboration) quiere tener su propio servicio [Git](https://www.muylinux.com/2020/02/25/free-software-foundation-forja-publica/)
+ Yoyo se ha pillado el [Slimbook One v.3](https://youtu.be/pfKuaqJ1buU)
+ Preguntas y respuestas para el episodio 100

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
