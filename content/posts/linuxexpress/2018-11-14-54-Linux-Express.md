---
title: "#54 Linux Express"
date: 2018-11-14
author: Juan Febles
categories: [linuxexpress]
img: 2018/54linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/54linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Agradecer una vez más a las empresas colaboradoras del programa: [Chicles GUW](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga).

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/54linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/54linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#64 Pásate a GNU/Linux](https://avpodcast.net/podcastlinux/gnulinux)
+ Próximo episodio: Especial [Pinebook](https://www.pine64.org/?product=11-6-pinebook)
+ Gracias otra vez a las Empresas colaboradoras: [Chicles WUG](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga)
+ Grupo Telegram [Pásate a GNU/Linux](https://t.me/pasategnulinux)
+ Capturadora [Slimbook](https://slimbook.es/pedidos/imagen-y-sonido/capturadora-compatible-con-ubuntu-linux-comprar) y [review](https://youtu.be/0NdXYqn_fjg)
+ OffTopic: [Móvil roto](https://twitter.com/podcastlinux/status/1061621197962657792)
+ Territorio f-Droid: [Fennec](https://f-droid.org/es/packages/org.mozilla.fennec_fdroid)
+ Visita a Valencia en diciembre.
+ [Maratón Linuxero](https://maratonlinuxero.org/) el 15 de diciembre

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [https://pixabay.com/photo-2071299). Los logos WUG y Waterdrop Hydroprint son propiedad de sus respectivas empresas.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
