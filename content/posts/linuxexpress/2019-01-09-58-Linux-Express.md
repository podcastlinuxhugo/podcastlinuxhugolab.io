---
title: "#58 Linux Express"
date: 2019-01-09
author: Juan Febles
categories: [linuxexpress]
img: 2019/58linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/58linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/58linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/58linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#68 Especial Slimbook Eclipse](https://avpodcast.net/podcastlinux/slimbookeclipse/)
+ Próximo episodio: Linux Connexion con [Aleix Pol](https://twitter.com/aleixpol)
+ Instalando [KDE Neon](https://neon.kde.org/) desde cero en mi [Slimbook One](https://slimbook.es/one-minipc-potente)
+ Instalando [Retropie](https://retropie.org.uk/) en PC [GNU/Linux](https://twitter.com/podcastlinux/status/1080741326289862656)
+ Territorio F-Droid: [Neoid](https://f-droid.org/es/packages/com.androidemu.nes/)
+ Me he pillado la [Playastation Mini](https://www.playstation.com/es-es/explore/playstation-classic/)
+ Ya está aquí el [Xiaomi Mi8 Lite](https://www.mi.com/es/mi-8-lite)
+ [FLISol Tenerife](https://flisol.info/FLISOL2019/Espana/Tenerife) en marcha
+ Feliz 2019


Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-1889081).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
