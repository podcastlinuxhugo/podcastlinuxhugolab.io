---
title: "#39 Linux Express"
date: 2018-04-18
author: Juan Febles
categories: [linuxexpress]
img: 2018/39linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/39linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Aquí tienes otro Linux Express, los audios de Telegram que se alternan con Podcast Linux para que sepas en qué estoy trabajando.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/39linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/39linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en esta quincena:  
+ [Episodio #49 Libera tu router](https://avpodcast.net/podcastlinux/liberaturouter/)
+ Siguiente episodio, Linux Connexion con [Hefistion](https://elblogdelazaro.gitlab.io/)
+ Ya tengo el Nexus 5 con [UBPorts](https://ubports.com/). Gracias [Jorge Moreno](https://territoriofotografico.com/)
+ [LineageOS](https://lineageos.org/) en Galaxy Mini 3
+ Próximos eventos Software Libre: [UbuconEU18](http://ubucon.org/en/events/ubucon-europe/), [Flisol18](https://flisol.info/) y [AkademyEs18](https://www.kde-espana.org/akademy-es-2018)
+ [Jpod18 Madrid](http://jpod.es/)

Las imágenes utilizadas están bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forman parte de [Pixabay](https://pixabay.com/photo-1294370/) y [Pixabay](https://pixabay.com/photo-1001858/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
