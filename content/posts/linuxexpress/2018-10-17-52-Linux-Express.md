---
title: "#52 Linux Express"
date: 2018-10-17
author: Juan Febles
categories: [linuxexpress]
img: 2018/52linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/52linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Agradecer a las empresas colaboradoras del programa: [Chicles GUW](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga).

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/52linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/52linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#62 Especial Jpod18](https://avpodcast.net/podcastlinux/jpod18)
+ Próximo episodio: Linux Connexion con Rubén Rodríguez
+ Gracias de nuevo a las Empresas colaboradoras: [Chicles WUG](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga)
+ Pronto Review de nuevo dispositivo
+ Samsung Galaxy Mini 3
+ Territorio f-Droid: [ForRunners](https://f-droid.org/en/packages/net.khertan.forrunners/)
+ ¿Hacemos un [Flisol](https://flisol.info/)?
+ Visita a Valencia en diciembre.

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-1871405/). Los logos WUG y Waterdrop Hydroprint son propiedad de sus respectivas empresas.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
