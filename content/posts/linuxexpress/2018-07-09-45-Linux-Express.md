---
title: "#45 Linux Express"
date: 2018-07-09
author: Juan Febles
categories: [linuxexpress]
img: 2018/45linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/45linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Inmerso en el verano, comento los ganadores del 2º Aniversario, próximos programas, eventos y noticias importantes.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/45linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/45linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ [Episodio #55 Especial 2º Aniversario](https://avpodcast.net/podcastlinux/2aniversario)
+ Ganadores [Sorteo Vant](https://twren.ch/check#5gqDkf0b7Yd6ZPF7).
+ Ganadores [Sorteo Slimbook](https://twren.ch/check#fv2IPY3oAEdwlM41).
+ Ganadores [Sorteo Neodigit](https://twren.ch/check#LrDU1ouA8OkddYwo).
+ [Linux Mint 19](https://linuxmint.com/) ya está aquí.
+ [GUADEC 18](https://2018.guadec.org/) en Almería
+ Territorio f-Droid: [AntennaPod](https://f-droid.org/en/packages/de.danoeh.antennapod/)
+ [Maraton Linuxero 21 de julio](https://maratonlinuxero.org)
+ Apúntate a los [Premios Asociación Podcast](http://premios.asociacionpodcast.es/inscribete_podcast/ )

La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-1904655/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
