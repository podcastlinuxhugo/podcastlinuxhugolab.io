---
title: "#96 Linux Express"
date: 2020-06-24
author: Juan Febles
category: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/96linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/96linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/96linuxexpress.mp3" type="audio/mpeg">
</audio>

Temas tratados en este episodio:  
+ Episodio #106 [Hardware Vant Edge](https://podcastlinux.com/106-Podcast-Linux)
+ Siguiente episodio: Especial 4º Aniversario
+ 8 días sin [Twitter](https://twitter.com/podcastlinux)
+ Realizando [Podcastlinux.com](https://podcastlinuxhugo.gitlab.io/) en [Hugo](https://gohugo.io/) gracias a David Marzal
+ Alternativa libre a [Disqus](https://disqus.com/) para comentar los episodios en la web
+ Nueva [Intro](https://archive.org/details/PodcastLinux-wakeup) y [Música](https://archive.org/details/PodcastLinux-7AM) propia realizada en [LMMS](https://lmms.io/)
+ Buscando un pc de 2ª mano por piezas.
+ Actividad extraescolar de [Arduino](https://www.arduino.cc/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@disroot.org>  
Web: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  

Este podcast está producido por Juan Febles y tiene licencia [Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).  

<img src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" alt="CC"
	title="CC SA-BY" width="88" height="31" />
