---
title: "#18 Linux Express"
author: Juan Febles
date: 2017-06-28
categories: [linuxexpress]
img: 2017/18LinuxExpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/%2318%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
Otro Linux Express para que estés informado del día a día de Podcast Linux.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2318%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #27 Especial Slimbook One](http://avpodcast.net/podcastlinux/slimbookone)
+ Especial 1º Aniversario Podcast Linux 1 de julio
+ Próximo episodio Linux Connexion con Alejandro López [Slimbook](https://slimbook.es)
+ [Premios Asociación Podcast](http://asociacionpodcast.us1.list-manage2.com/subscribe?u=976ab30aaa02edad08f32ed62&id=62ad6d3953) y [Maratón Jpod](http://jpod.es/maraton)
+ Aterrizan los Chromebooks en Educación


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>
