---
title: "#53 Linux Express"
date: 2018-10-31
author: Juan Febles
categories: [linuxexpress]
img: 2018/53linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/53linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Agradecer una vez más a las empresas colaboradoras del programa: [Chicles GUW](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga).

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/53linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/53linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Episodio [#63 Linux Connexion con Rubén Rodríguez](https://avpodcast.net/podcastlinux/rubenrodriguez)
+ Próximo episodio: Pásate a GNU/Linux
+ Gracias de nuevo a las Empresas colaboradoras: [Chicles WUG](https://wugum.com/) y [Waterdrop Hydroprint](http://waterdrop.ga)
+ ya viene mi [Pinebook](https://www.pine64.org/?product=11-6-pinebook)
+ [Pinephone y Pinetab](https://itsfoss.com/pinebook-kde-smartphone/) con KDE Plasma para 2019
+ Territorio f-Droid: [StreetComplete](https://f-droid.org/es/packages/de.westnordost.streetcomplete/)
+ En marcha FLISoL Tenerife ¿Y el tuyo?
+ Visita a Valencia en diciembre.
+ [Maratón Linuxero](https://maratonlinuxero.org/) el 15 de diciembre

Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [https://pixabay.com/photo-2071296). Los logos WUG y Waterdrop Hydroprint son propiedad de sus respectivas empresas.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
