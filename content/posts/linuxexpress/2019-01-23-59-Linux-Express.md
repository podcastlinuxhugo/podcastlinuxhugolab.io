---
title: "#59 Linux Express"
date: 2019-01-23
author: Juan Febles
categories: [linuxexpress]
img: 2019/59linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/59linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/59linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/59linuxexpress.mp3" type="audio/mpeg">
</audio>

Estas semanas han estado cargadas de muchas cosas que quiero compartir:  
+ Episodio [#69 Linux Connexion con Aleix Pol](https://avpodcast.net/podcastlinux/aleixpol2/)
+ Próximo episodio: [Alternativas Libres a Google](https://twitter.com/podcastlinux/status/1083636444609040386)
+ Instalando [Dietpi](https://www.dietpi.com/) en [Raspberry Pi](https://twitter.com/podcastlinux/status/1086555741207646208)
+ Actualizando [Odroid-Go](https://wiki.odroid.com/odroid_go/odroid_go)
+ Probando [MX Linux](https://twitter.com/podcastlinux/status/1084825507164839938)
+ Busco Ipod y Tablet BQ M10 FHD
+ Ya tengo mi [Peluche Tux](https://twitter.com/podcastlinux/status/1086241725792833536)
+ Territorio f-Droid: [NextCloud Android App](https://f-droid.org/es/packages/com.nextcloud.client/)
+ Monta tu [FLISol](https://flisol.info) en tu localidad



Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-2364349). La foto de peluche Tux es propia.


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
