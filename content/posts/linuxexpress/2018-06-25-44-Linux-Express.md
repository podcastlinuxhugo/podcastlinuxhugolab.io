---
title: "#44 Linux Express"
date: 2018-06-25
author: Juan Febles
categories: [linuxexpress]
img: 2018/44linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/44linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Primer Linux Express del verano. Se acercan fechas estivales y te cuento todo lo que se va a realizar en ellas.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/44linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/44linuxexpress.mp3" type="audio/mpeg">
</audio>

Te dejo el cacharreo e información al que he estado dándole vueltas en esta quincena:  
+ [Episodio #54 Linux Connexion con Marcos Costales](https://avpodcast.net/podcastlinux/marcoscostales)
+ Siguiente episodio, Especial 2º Aniversario con sorteos.
+ Linux Connexions y especiales para el verano.
+ KDE Neon en un [netbook ligero](https://youtu.be/jxnhpeddnoM)
+ Podcast de Google vs. AntennaPod
+ Territorio F-Droid: [Screencam](https://f-droid.org/en/packages/com.orpheusdroid.screenrecorder/)
+ [Maraton Linuxero 21 de julio](https://maratonlinuxero.org)

La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-1907156/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
