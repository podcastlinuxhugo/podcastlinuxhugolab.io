---
title: "#17 Linux Express"
author: Juan Febles
date: 2017-06-14
categories: [linuxexpress]
img: 2017/17LinuxExpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/%2317%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
Otro Linux Express para que estés informado del día a día de Podcast Linux.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2317%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

Repasamos lo acontecido en estas semanas:

+ [Episodio #26 Linux Connexion con Ugeek](http://avpodcast.net/podcastlinux/ugeek)
+ Nuevos podcast linuxeros: [Dj Informático](http://eldjinformatico.blogspot.com.es) y [Ubuntu y otras hierbas](https://www.ivoox.com/ubuntu-otras-hierbas_sq_f1412582_1.html)
+ [Premios Asociación Podcast](http://premios.asociacionpodcast.es/)
+ Próximo episodio Especial [Slimbook One](https://slimbook.es/one-minipc-potente)
+ Especial 1º Aniversario Podcast Linux 1 de julio
+ Reto: Maratón Linuxero


Recuerda que puedes **contactar** conmigo de las siguientes formas:

+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <http://avpodcast.net/podcastlinux/>
+ Blog: <https://podcastlinux.gitlab.io/>
+ Telegram: <https://t.me/podcastlinux>
+ Youtube: <https://www.youtube.com/PodcastLinux>
+ Feed Podcast Linux: <https://podcastlinux.com/feed>
+ Feed Linux Express (Audios Telegram): <https://podcastlinux.gitlab.io/Linux-Express/feed.xml>
