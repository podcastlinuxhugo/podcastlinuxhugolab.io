---
title: "#40 Linux Express"
date: 2018-05-02
author: Juan Febles
categories: [linuxexpress]
img: 2018/40linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/40linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Aquí tienes otro Linux Express, los audios de Telegram que se alternan con Podcast Linux para que sepas en qué estoy trabajando.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/40linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/40linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es a lo que le he estado dando vueltas en estas 2 semanas:  
+ [Episodio #50 Linux Connexion con Hefistion](https://avpodcast.net/podcastlinux/hefistion/)
+ Siguiente episodio, Chromebooks
+ Chromebooks en mi colegio el próximo año
+ Cambio de batería del Nexus 5
+ Off Topic: [#EnvíosCanariasCeutaMelillaYa](https://twitter.com/hashtag/Env%C3%ADosCanariasCeutaMelillaYa)
+ Editor de vídeo [ShotCut](https://www.shotcut.org/)
+ Maratón [Jpod18 Madrid](http://jpod.es/)

Las imagen utilizada está bajo [licencia Creative Commons Atribución](https://creativecommons.org/licenses/by/4.0/) del autor [Jimmy Burns](https://www.flickr.com/photos/chipsxp/) y forma parte de [Flickr](https://flic.kr/p/FxMbPj).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
