---
title: "#38 Linux Express"
date: 2018-04-04
author: Juan Febles
categories: [linuxexpress]
img: 2018/38linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/38linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
Esto es Linux Express, los audios de Telegram que se alternan con Podcast Linux para que semanalmente estés informado de todo lo que se cuece.

<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/38linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/38linuxexpress.mp3" type="audio/mpeg">
</audio>

Esto es lo que ha sucedido en esta quincena:  
+ [Episodio #48 Linux Express Ubucon EU 18](https://avpodcast.net/podcastlinux/ubuconeu18/)
+ Siguiente episodio, Libera tu router
+ [PirateBox](https://piratebox.cc/android) en Android
+ Nexus 5 y [UBPorts](https://ubports.com/)
+ [Intro](https://twitter.com/podcastlinux/status/979655744856436736) para Screencast en [KDENLive](https://kdenlive.org/)
+ Éxito de [Maratón Linuxero](https://maratonlinuxero.org/).

Las imágenes utilizadas están bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forman parte de [Pixabay](https://pixabay.com/photo-1684590/) y [Pixabay]( https://pixabay.com/photo-1294370/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
