---
title: "#84 Linux Express"
date: 2020-01-08
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/84linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/84linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/84linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio #94[Linux Connexion con Atareao](https://avpodcast.net/podcastlinux/atareao2)
+ Próximo episodio: Fotografía y GNU/Linux
+ [Sorteo Slimbook One](https://twitter.com/podcastlinux/status/1211948003650686976)
+ [Componentes Aliexpress](https://es.aliexpress.com/w/wholesale-arduino.html) vs. [Componentes tienda local](http://k-electronica.es/) de [Arduino](https://www.arduino.cc)
+ Proyecto [ESP8266 de letrero de texto por wifi](https://twitter.com/podcastlinux/status/1210509465516163073)
+ Proyecto [ESP8266 Reloj - clima IOT](https://twitter.com/podcastlinux/status/1212994942693445633) 
+ Ya somos [5.000](https://twitter.com/podcastlinux) en Twitter
+ Hasta siempre mi querida [Kira](https://twitter.com/podcastlinux/status/1212046476068708352)

Recuerda que puedes **contactar** conmigo de las siguientes formas:  

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
