---
title: "#80 Linux Express"
date: 2019-11-13
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/80linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/80linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/80linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio #90 [Arduino](https://avpodcast.net/podcastlinux/arduino)
+ Próximo episodio: Linux Connexion con [Luis del Valle](https://www.programarfacil.com/)
+ Conectando  [Arduino](https://www.arduino.cc/) en una [Raspberry Pi](https://www.raspberrypi.org/)
+ II Premios [Tecnoedu](https://tecnoedu.webs.ull.es/premios-2019-2/) el 26 de noviembre
+ Colaboración con [Applelianos](http://www.applelianos.com/2019/11/01/5x32-especial-la-historia-de-linux) sobre la historia de GNU/Linux
+ Traducción de [infografía](https://archive.org/details/choose-es) para elegir licencias [Creative Commons](https://creativecommons.org/) 
+ [Emacs](https://www.gnu.org/software/emacs/) o [Vim](https://www.vim.org/)
+ Somos 1.000 suscriptores en [Telegram](https://t.me/podcastlinux) y 4.700 en [Twitter](https://twitter.com/podcastlinux)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  	
