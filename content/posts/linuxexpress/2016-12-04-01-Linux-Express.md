---
title: "#01 Linux Express"
date: 2016-12-04
author: Juan Febles
categories: [linuxexpress]
img: 2017/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/%2301%20Podcast%20Linux%20Express
  video:
tags: [audio, telegram, Linux Express,]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/%2301%20Podcast%20Linux%20Express.mp3" type="audio/mpeg">
</audio>

El primer linux Express, los audios que comparto cada 2 semanas en [Telegram](https://t.me/podcastlinux) para ir alternando
los podcasts quincenales con éstos.
Aquí encontrarás información de lo que estoy proyectando y realizando mientras esperas a un nuevo podcast.
