---
title: "#75 Linux Express"
date: 2019-09-04
author: Juan Febles
categories: [linuxexpress]
img: 2019/linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/75linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/75linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/75linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#85 Linux Connexion con Paco Estrada](https://avpodcast.net/podcastlinux/pacoestrada)
+ Próximo episodio: Redes Sociales Libres
+ Terminando el curso[Audacity](https://www.audacityteam.org/) Para la [Asociación Podcast](https://www.asociacionpodcast.es/curso-audacity-por-Juan Febles-febles/)
+ Posible curso de [Hugo](https://gohugo.io/) como generador web en [Gitlab](https://medium.com/@loadbalancing/building-a-blog-website-1-quick-start-with-hugo-and-gitlab-888b9faa176f)
+ Enganchado a [Git](https://alejandrojs.wordpress.com/2017/06/01/como-empezar-a-usar-git-con-gitlab/) y [Vim](https://github.com/AnderRasoVazquez/curso-de-vim) para trabajar con las webs.
+ Aprender comandos de [Vim Jugando](https://vim-adventures.com/) gracias a [Adrián Benavente](https://twitter.com/fenavente)
+ Curso [Maker School](https://t.me/kreitek_anuncios/201) de [Kreitek](https://kreitek.org/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  	
