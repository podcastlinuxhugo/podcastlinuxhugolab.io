---
title: "#57 Linux Express"
date: 2018-12-26
author: Juan Febles
categories: [linuxexpress]
img: 2018/57linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/57linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/57linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/57linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los puntos tratados en este programa:  
+ Pedir perdón por el [Maratón Linuxero](https://maratonlinuxero.org/) del 15 de diciembre
+ Episodio [#67 Especial Viaje a Valencia](https://avpodcast.net/podcastlinux/valencia)
+ Próximo episodio: Especial [Slimbook Eclipse](https://slimbook.es/eclipse-linux-profesional-workstation-and-gaming-laptop). [Review Youtube](https://youtu.be/XVCHaCG2jxc)
+ Episodio de [KDE España Podcast](https://www.ivoox.com/05x02-infraestructura-kde-entenderla-usarla-audios-mp3_rf_30984630_1.html)
+ Actualizaciones de [Shotcut](https://www.shotcut.org/blog/new-release-181223/)
+ Territorio f-Droid: [Mastalab](https://f-droid.org/es/packages/fr.gouv.etalab.mastodon/)
+ Xiaomi elegido: [Mi8 Lite](https://www.mi.com/es/mi-8-lite)
+ Felices Fiestas, Feliz Navidad.


Las imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/photo-1697937/).


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
