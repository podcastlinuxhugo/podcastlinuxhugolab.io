---
title: "#62 Linux Express"
date: 2019-03-06
author: Juan Febles
categories: [linuxexpress]
img: 2019/62linuxexpress.png
podcast:
  audio: https://podcastlinux.gitlab.io/Linux-Express/62linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
<audio controls>
  <source src="https://podcastlinux.gitlab.io/Linux-Express/62linuxexpress.ogg" type="audio/ogg">
  <source src="https://podcastlinux.gitlab.io/Linux-Express/62linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#72 Especial Vant MiniMOOVE](https://avpodcast.net/podcastlinux/minimoove)
+ Próximo episodio: Audio y GNU/Linux
+ Nuevos dispositivos a analizar
+ Idea de proyecto: Montar PC reciclado
+ Sobre SGAE, Ivoox, Itunes
+ Taller Audacity en mi colegio.
+ Territorio f-Droid: [Activity Diary](https://f-droid.org/es/packages/de.rampro.activitydiary/)
+ [FLISol 19](https://flisol.info/FLISOL2019) Tenerife


La imagen utilizada está bajo [licencia Creative Commons 0](https://creativecommons.org/choose/zero/) y forma parte de [Pixabay](https://pixabay.com/images/id-2320131).

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://podcastlinux.com/feed>  
Feed Linux Express (Audios Telegram): <https://podcastlinux.com/Linux-Express/feed>  
